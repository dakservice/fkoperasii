/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.resetpass;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MOtp;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.Function;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Random;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.jpos.transaction.Context;

import static org.jpos.transaction.TransactionConstants.PREPARED;

import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 * @author 1star
 */
public class ResetPassword implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    static ChannelSftp channelSftp = null;
    static Session session = null;
    static Channel channel = null;
    static String PATHSEPARATOR = "/";

    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd");
        Boolean forgot_password = false;
        if (jsonReq.has("forgot_password")) {
            forgot_password = true;
        }
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {

            Random rand = new Random();
            int rand_int1 = rand.nextInt(999999);
            String otp =  String.format("%06d", rand_int1);
            String notelp;
            MNasabah nsbh;
            if (forgot_password) {
                nsbh = SpringInit.getmNasabahDao().userByEmail(jsonReq.getString("email"));
            } else {
                notelp = jsonReq.getString("no_telp");
                nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            }

            if (nsbh == null) {
                rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.DATA_NOT_FOUND);
                if (forgot_password) {
                    log.info("====================== USER NOT FOUND");
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(null);
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    dr = new ResponseWebServiceContainer(Constant.WA.RC.USER_NOT_FOUND, "Email tidak ditemukan, pastikan anda memasukan email yang terdaftar dengan akun anda !");
                } else {
                    dr = new ResponseWebServiceContainer("02", "No Telepon Tidak Ditemukan");
                }
            } else {
                notelp = nsbh.getNotelp();
                String email;
                if(nsbh.getEmail()==null || nsbh.getEmail().isEmpty()){
                    email = jsonReq.getString("email");
                }else{
                    email = nsbh.getEmail();
                }
                MOtp mOtp = new MOtp();
                mOtp.setOtp(otp);
                mOtp.setType("reset_password");
                mOtp.setStatus("aktif");
                mOtp.setNoTelp(notelp);


//                ClientOptions options = ClientOptions.builder()
//                        .apiKey("ee5cb04f1c66e70ad4d265f9bf2d2992")
//                        .apiSecretKey("e32af3572e6b5c0a8d24ef579a0d978f")
//                        .build();
//
//                MailjetClient client = new MailjetClient(options);
//                TransactionalEmail message1 = TransactionalEmail
//                        .builder()
//                        .to(new SendContact(email, nsbh.getNama()))
//                        .from(new SendContact("diykoperasi@gmail.com", "KOPERASI DIY"))
//                        .htmlPart(emailHtml)
//                        .subject("FORGOT PASSWORD KOPERASI APP")
//                        .trackOpens(TrackOpens.ENABLED)
//                        //.attachment(Attachment.fromFile(attachmentPath))
//                        .header("test-header-key", "test-value")
//                        .customID("custom-id-value")
//                        .build();
//
//                SendEmailsRequest request = SendEmailsRequest
//                        .builder()
//                        .message(message1) // you can add up to 50 messages per request
//                        .build();
//
//                // act
//                SendEmailsResponse response = request.sendWith(client);
//                log.info("asd response " + response);
                log.info(email);
                JSONObject responseSendOTP = new JSONObject();
                String bodyresp;
                if(jsonReq.has("type")) {
                    if (jsonReq.getString("type").equalsIgnoreCase("wa")) {
                        final String message = SpringInit.getmSettingDao().getSettingByParam("otp_wa_template").getValue().replace("#otp",otp);
                        Function.sendWA(jsonReq.getString("no_telp"), message);
                        responseSendOTP.put("message","Success");
                    }else{
                        String emailHtml = SpringInit.getmSettingDao().getSettingByParam("format_email_cp").getValue().replace("#otp", otp);
                        responseSendOTP= Function.sendEmailLib(email,nsbh.getNama(),emailHtml);
                        log.info("respon email" + responseSendOTP);
                    }
                }else if(!jsonReq.has("type")){
                    String emailHtml = SpringInit.getmSettingDao().getSettingByParam("format_email_cp").getValue().replace("#otp", otp);
                    responseSendOTP= Function.sendEmailLib(email,nsbh.getNama(),emailHtml);
                    log.info("respon email" + responseSendOTP);
                }

                if (responseSendOTP.getString("message").equals("Bad Request")) {
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
                    dr = new ResponseWebServiceContainer("", "internal server error");

                } else {

                    SpringInit.getmOtpDao().saveOrUpdate(mOtp);
                    JSONObject resp = new JSONObject();
                    JSONObject data = new JSONObject();
                    data.put("no_telp", notelp);
                    resp.put("data", data);
                    if(jsonReq.has("type") && jsonReq.getString("type").equalsIgnoreCase("wa"))
                    resp.put("rm", "OTP Sukses Dikirim, Silahkan Cek WhatsApp Anda");
                    else resp.put("rm", "OTP Sukses Dikirim, Silahkan Cek Email Anda");

                    resp.put("rc", "00");
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(resp);
                }
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable context) {
    }

}
