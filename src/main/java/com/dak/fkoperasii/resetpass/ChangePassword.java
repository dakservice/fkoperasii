/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.resetpass;

import com.dak.fkoperasii.entity.MDeviceAnggota;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MOtp;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jpos.transaction.Context;

import static org.jpos.transaction.TransactionConstants.PREPARED;

import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 * @author 1star
 */
public class ChangePassword implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    static ChannelSftp channelSftp = null;
    static Session session = null;
    static Channel channel = null;
    static String PATHSEPARATOR = "/";

    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd");
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            String device_id = jsonReq.getString("device_id");
            String device_name = jsonReq.getString("device_name");
            String notelp = jsonReq.getString("no_telp");
            String password = "nsi" + jsonReq.getString("password");
            String otp = jsonReq.getString("otp");
            Boolean newUser = jsonReq.getBoolean("new_user");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);

            if (nsbh == null) {
                dr = new ResponseWebServiceContainer("02", "No Telepon Tidak Ditemukan");

            } else {

                MOtp motp = SpringInit.getmOtpDao().getOtpByDate(otp);
                if (motp == null) {
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    dr = new ResponseWebServiceContainer("02", "OTP Tidak Ditemukan");
                } else {
                    Date date = new Date();
                    Date expired = motp.getCreateAt();
                    long diffInMillies = date.getTime() - expired.getTime();
                    log.info(expired.toString());
                    log.info(date.toString());

                    long difference_In_Minutes = (diffInMillies
                            / (1000 * 60));
                    if (difference_In_Minutes > 30) {
                        //                mlogmobile id user
                        ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                        mACtivity.setIdUser(nsbh.getId());
                        ctx.put(Constant.WS.REQUEST, mACtivity);
                        log.info("OTP Kadaluarsa");
                        dr = new ResponseWebServiceContainer("02", "OTP Anda Kadalaursa, Silahkan Request OTP Lagi");

                    } else {
                        if (newUser) {
                            log.info("save new device");
                            MDeviceAnggota newDevice = new MDeviceAnggota();
                            newDevice.setDevice_id(device_id);
                            newDevice.setDevices_name(device_name);
                            newDevice.setIdAnggota(nsbh.getId());
                            newDevice.setStatus("true");
                            SpringInit.getMDeviceAnggotaDao().saveOrUpdate(newDevice);

                        }
                        log.info("otp Benar");
                        String encrip = encryptThisString(password);
                        String encrip2 = encryptThisString(jsonReq.getString("password"));
                        log.info(encrip);
                        log.info(encrip2);
                        nsbh.setPass_word(encrip);
                        SpringInit.getmNasabahDao().saveOrUpdate(nsbh);
                        String email = nsbh.getEmail();
                        JSONObject resp = new JSONObject();
                        resp.put("email", email);
                        log.info(resp.toString());
                        //                mlogmobile id user
                        ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                        mACtivity.setIdUser(nsbh.getId());
                        ctx.put(Constant.WS.REQUEST, mACtivity);
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                        dr = new ResponseWebServiceContainer(rc.getRc(), "Password Berhasil Dirubah");
                    }

                }

            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable context) {
    }

    public static String encryptThisString(String input) throws NoSuchAlgorithmException {

        // getInstance() method is called with algorithm SHA-1 
        MessageDigest md = MessageDigest.getInstance("SHA-1");

        // digest() method is called 
        // to calculate message digest of the input string 
        // returned as array of byte 
        byte[] messageDigest = md.digest(input.getBytes());

        // Convert byte array into signum representation 
        BigInteger no = new BigInteger(1, messageDigest);

        // Convert message digest into hex value 
        String hashtext = no.toString(16);

        // Add preceding 0s to make it 32 bit 
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }

        // return the HashText 
        return hashtext;
    }
}
