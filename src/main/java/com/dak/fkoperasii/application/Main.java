/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.application;

import com.dak.fkoperasii.spring.SpringInit;

import javax.annotation.PostConstruct;
import javax.naming.ConfigurationException;
import org.jpos.q2.Q2;
import org.jpos.util.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author Morten Jonathan
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.dak.fkoperasii.controller"})
@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
public class Main {
    
    Log log = Log.getLog("Q2", getClass().getName());
//    @PostConstruct
//    void started() {
//        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
//    }
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        try {
            Q2 q2 = new Q2("src/main/resources/deploy");
//            Q2 q2 = new Q2("deploy");
            q2.start();
            SpringInit SpringInit = new SpringInit();
            SpringInit.initService();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }
}
