/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.application;

import java.security.KeyFactory;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import org.joda.time.DateTime;

/**
 *
 * @author 1star
 */
public class Test {
    
    public static void main(String[] args) throws Exception {
//        String input = "sample input";
//         
//        // Not a real private key! Replace with your private key!
//        String strPk = "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9"
//                + "w0BAQEFAASCBKkwggSlAgEAAoIBAQDJUGqaRB11KjxQ\nKHDeG"
//                + "........................................................"
//                + "Ldt0hAPNl4QKYWCfJm\nNf7Afqaa/RZq0+y/36v83NGENQ==\n"
//                + "-----END PRIVATE KEY-----\n";
//         
//        String base64Signature = signSHA256RSA(input,strPk);
//        System.out.println("Signature="+base64Signature);

              DateTime jdate = new DateTime();
        String tmpe = jdate.toString("yyyy-MM-dd HH:mm:ss");
         Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(tmpe);  
    System.out.println(tmpe+"\t"+date1);
    }
 
    // Create base64 encoded signature using SHA256/RSA.
    private static String signSHA256RSA(String input, String strPk) throws Exception {
        // Remove markers and new line characters in private key
        String realPK = strPk.replaceAll("-----END PRIVATE KEY-----", "")
                             .replaceAll("-----BEGIN PRIVATE KEY-----", "")
                             .replaceAll("\n", "");
 
        byte[] b1 = Base64.getDecoder().decode(realPK);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(b1);
        KeyFactory kf = KeyFactory.getInstance("RSA");
 
        Signature privateSignature = Signature.getInstance("SHA256withRSA");
        privateSignature.initSign(kf.generatePrivate(spec));
        privateSignature.update(input.getBytes("UTF-8"));
        byte[] s = privateSignature.sign();
        return Base64.getEncoder().encodeToString(s);
    }
     
}
