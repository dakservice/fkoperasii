/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MSukuBunga")
@Table(name = "suku_bunga")
public class MSukuBunga {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "m_jenis_angsuran_id_seq")
    @SequenceGenerator(name = "m_jenis_angsuran_id_seq", sequenceName = "m_jenis_angsuran_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "opsi_key", nullable = false)
    private String opsi_key;
    
    @Column(name = "opsi_val", nullable = false)
    private String opsi_val;
    
    @Column(name = "id_coa", nullable = false)
    private Integer id_coa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpsi_key() {
        return opsi_key;
    }

    public void setOpsi_key(String opsi_key) {
        this.opsi_key = opsi_key;
    }

    public String getOpsi_val() {
        return opsi_val;
    }

    public void setOpsi_val(String opsi_val) {
        this.opsi_val = opsi_val;
    }

    public Integer getId_coa() {
        return id_coa;
    }

    public void setId_coa(Integer id_coa) {
        this.id_coa = id_coa;
    }
    
    
    
}
