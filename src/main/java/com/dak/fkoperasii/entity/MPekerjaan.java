/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.*;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MPekerjaan")
@Table(name = "pekerjaan")
public class MPekerjaan {
    public enum Aktif {
        Y, N;
    }

    @Id
    @Column(name = "id_kerja", nullable = false)
    private String id_kerja;
    
    @Column(name = "jenis_kerja", nullable = false)
    private String jenis_kerja;

    @Enumerated(EnumType.ORDINAL)
    private Aktif aktif;


    public String getId_kerja() {
        return id_kerja;
    }

    public void setId_kerja(String id_kerja) {
        this.id_kerja = id_kerja;
    }

    public String getJenis_kerja() {
        return jenis_kerja;
    }

    public void setJenis_kerja(String jenis_kerja) {
        this.jenis_kerja = jenis_kerja;
    }
    
}

