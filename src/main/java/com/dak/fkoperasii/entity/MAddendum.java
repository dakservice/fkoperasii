/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;


import com.dak.fkoperasii.utility.Constant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MAddendum")
@Table(name = "tbl_pengajuan_addendum")
public class MAddendum {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "maddendum_id_seq")
    @SequenceGenerator(name = "maddendum_id_seq", sequenceName = "maddendum_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "kode_addendum", nullable = false)
    private String kode_addendum;
    
    @Column(name = "pinjam_id", nullable = false)
    private Integer pinjam_id;
    
    @Column(name = "angsuran_ke", nullable = false)
    private String angsuran_ke;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;
    
    @Column(name = "alasan", nullable = false)
    private String alasan;
    
    @Column(name = "status", nullable = false)
    private Integer status;
    
    @Column(name = "tgl_input", nullable = false)
    private String tgl_input;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKode_addendum() {
        return kode_addendum;
    }

    public void setKode_addendum(String kode_addendum) {
        this.kode_addendum = kode_addendum;
    }

    public Integer getPinjam_id() {
        return pinjam_id;
    }

    public void setPinjam_id(Integer pinjam_id) {
        this.pinjam_id = pinjam_id;
    }

    public String getAngsuran_ke() {
        return angsuran_ke;
    }

    public void setAngsuran_ke(String angsuran_ke) {
        this.angsuran_ke = angsuran_ke;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTgl_input() {
        return tgl_input;
    }

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }
    
    
    
    
    
}
