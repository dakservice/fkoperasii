/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;


import com.dak.fkoperasii.utility.Constant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MHeadPinjaman")
@Table(name = "tbl_pinjaman_h")
public class MHeadPinjaman {

    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "tgl_pinjam", nullable = false)
    private String tgl_pinjam;
    
    @Column(name = "anggota_id", nullable = false)
    private Integer anggota_id;
    
    @Column(name = "barang_id", nullable = false)
    private Integer barang_id;
    
    @Column(name = "lama_angsuran", nullable = false)
    private Integer lama_angsuran;
    
    @Column(name = "jumlah", nullable = false)
    private Integer jumlah;
    
    @Column(name = "bunga", nullable = false)
    private Float bunga;
    
    @Column(name = "biaya_adm", nullable = false)
    private Integer biaya_adm;
    
    @Column(name = "biaya_provisi", nullable = false)
    private Integer biaya_provisi;
    
    @Column(name = "lunas", nullable = false)
    private String lunas;
    
    @Column(name = "dk", nullable = false)
    private String dk;
    
    @Column(name = "kas_id", nullable = false)
    private Integer kas_id;
    
    @Column(name = "jns_trans", nullable = false)
    private Integer jns_trans;
    
    @Column(name = "update_data", nullable = false)
    private String update_data;
    
    @Column(name = "user_name", nullable = false)
    private String user_name;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;
    
    @Column(name = "contoh", nullable = false)
    private Integer contoh;
    
    @Column(name = "id_nilai_jaminan", nullable = false)
    private Integer id_nilai_jaminan;

    @Column(name = "jenis_pinjaman", nullable = false)
    private Integer jenis_pinjaman;

    @Column(name = "jenis_jaminan", nullable = false)
    private Integer jenis_jaminan;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTgl_pinjam() {
        return tgl_pinjam;
    }

    public void setTgl_pinjam(String tgl_pinjam) {
        this.tgl_pinjam = tgl_pinjam;
    }

    public Integer getAnggota_id() {
        return anggota_id;
    }

    public void setAnggota_id(Integer anggota_id) {
        this.anggota_id = anggota_id;
    }

    public Integer getBarang_id() {
        return barang_id;
    }

    public void setBarang_id(Integer barang_id) {
        this.barang_id = barang_id;
    }

    public Integer getLama_angsuran() {
        return lama_angsuran;
    }

    public void setLama_angsuran(Integer lama_angsuran) {
        this.lama_angsuran = lama_angsuran;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public Float getBunga() {
        return bunga;
    }

    public void setBunga(Float bunga) {
        this.bunga = bunga;
    }

    public Integer getBiaya_adm() {
        return biaya_adm;
    }

    public void setBiaya_adm(Integer biaya_adm) {
        this.biaya_adm = biaya_adm;
    }

    public Integer getBiaya_provisi() {
        return biaya_provisi;
    }

    public void setBiaya_provisi(Integer biaya_provisi) {
        this.biaya_provisi = biaya_provisi;
    }

    public String getLunas() {
        return lunas;
    }

    public void setLunas(String lunas) {
        this.lunas = lunas;
    }

    public String getDk() {
        return dk;
    }

    public void setDk(String dk) {
        this.dk = dk;
    }

    public Integer getKas_id() {
        return kas_id;
    }

    public void setKas_id(Integer kas_id) {
        this.kas_id = kas_id;
    }

    public Integer getJns_trans() {
        return jns_trans;
    }

    public void setJns_trans(Integer jns_trans) {
        this.jns_trans = jns_trans;
    }

    public String getUpdate_data() {
        return update_data;
    }

    public void setUpdate_data(String update_data) {
        this.update_data = update_data;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Integer getContoh() {
        return contoh;
    }

    public void setContoh(Integer contoh) {
        this.contoh = contoh;
    }

    public Integer getId_nilai_jaminan() {
        return id_nilai_jaminan;
    }

    public void setId_nilai_jaminan(Integer id_nilai_jaminan) {
        this.id_nilai_jaminan = id_nilai_jaminan;
    }

    public Integer getJenis_pinjaman() {
        return jenis_pinjaman;
    }

    public void setJenis_pinjaman(Integer jenis_jaminan) {
        this.jenis_pinjaman = jenis_jaminan;
    }

    public Integer getJenis_jaminan() {
        return jenis_jaminan;
    }

    public void setJenis_jaminan(Integer jenis_jaminan) {
        this.jenis_jaminan = jenis_jaminan;
    }
    
    
    
}
