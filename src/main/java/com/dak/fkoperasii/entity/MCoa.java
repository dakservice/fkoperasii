/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MCoa")
@Table(name = "coa")
public class MCoa {
    
    @Id
    @Column(name = "id_coa", nullable = false)
    private Integer id_coa;

    @Column(name = "kd_akun", nullable = false)
    private String kd_akun;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;
    
    @Column(name = "akun", nullable = false)
    private String akun;
    
    @Column(name = "laba_rugi", nullable = false)
    private String laba_rugi;
    
    @Column(name = "pemasukan", nullable = false)
    private Double pemasukan;
    
    @Column(name = "pengeluaran", nullable = false)
    private String pengeluaran;
    
    @Column(name = "tab", nullable = false)
    private Integer tab;
    
    @Column(name = "aktif", nullable = false)
    private String aktif;
    
    @Column(name = "tipe", nullable = false)
    private String tipe;

    public Integer getId_coa() {
        return id_coa;
    }

    public void setId_coa(Integer id_coa) {
        this.id_coa = id_coa;
    }

    public String getKd_akun() {
        return kd_akun;
    }

    public void setKd_akun(String kd_akun) {
        this.kd_akun = kd_akun;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getAkun() {
        return akun;
    }

    public void setAkun(String akun) {
        this.akun = akun;
    }

    public String getLaba_rugi() {
        return laba_rugi;
    }

    public void setLaba_rugi(String laba_rugi) {
        this.laba_rugi = laba_rugi;
    }

    public Double getPemasukan() {
        return pemasukan;
    }

    public void setPemasukan(Double pemasukan) {
        this.pemasukan = pemasukan;
    }

    public String getPengeluaran() {
        return pengeluaran;
    }

    public void setPengeluaran(String pengeluaran) {
        this.pengeluaran = pengeluaran;
    }

    public Integer getTab() {
        return tab;
    }

    public void setTab(Integer tab) {
        this.tab = tab;
    }

    public String getAktif() {
        return aktif;
    }

    public void setAktif(String aktif) {
        this.aktif = aktif;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }
 
    
    
}
