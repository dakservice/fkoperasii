/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MLogMobile")
//@Table(name = "m_log_mob")
public class MLogMobile {
     @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "log_mobile_activity_id_seq")
    @SequenceGenerator(name = "log_mobile_activity_id_seq", sequenceName = "log_mobile_activity_id_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "remote_addr", nullable = false)
    private String remoteAddr;

    @Column(name = "request_url", nullable = false)
    private String requestUrl;

    @Column(name = "router_path", nullable = false)
    private String routerPath;

    @Column(name = "request_body", nullable = false)
    @Type(type = "text")
    private String requestBody;

    @Column(name = "response")
    @Type(type = "text")
    private String response;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "request_time")
    private Date requestTime;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "response_time")
    private Date responseTime;

    @Column(name = "token")
    private String token;

    @Column(name = "response_code")
    private String responseCode;

    @Column(name = "response_message")
    private String responseMessage;

    @Column(name = "status")
    private String status;

    @Column(name = "id_user")
    private Long idUser;

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdUser() {
        return idUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRouterPath() {
        return routerPath;
    }

    public void setRouterPath(String routerPath) {
        this.routerPath = routerPath;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
