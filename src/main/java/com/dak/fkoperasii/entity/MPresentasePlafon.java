/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MPresentasePlafon")
@Table(name = "presentase_plafon")
public class MPresentasePlafon {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mpengajuan_id_seq")
    @SequenceGenerator(name = "mpengajuan_id_seq", sequenceName = "mpengajuan_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "presentase_plafon", nullable = false)
    private Integer presentase_plafon;
    
    @Column(name = "status", nullable = false)
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPresentase_plafon() {
        return presentase_plafon;
    }

    public void setPresentase_plafon(Integer presentase_plafon) {
        this.presentase_plafon = presentase_plafon;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
