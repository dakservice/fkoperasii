/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MTanggunganPlafon")
@Table(name = "tbl_tanggungan_plafon")
public class MTanggunganPlafon {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "m_idplafon_seq")
    @SequenceGenerator(name = "m_idplafon_seq", sequenceName = "m_idplafon_seq")
    @Column(name = "id_plafon", nullable = false)
    private Integer id_plafon;

    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "nominal_tanggungan", nullable = false)
    private Integer nominal_tanggungan;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;
    
    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "foto_bukti", nullable = false)
    private String foto_bukti;

    public Integer getId_plafon() {
        return id_plafon;
    }

    public void setId_plafon(Integer id_plafon) {
        this.id_plafon = id_plafon;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNominal_tanggungan() {
        return nominal_tanggungan;
    }

    public void setNominal_tanggungan(Integer nominal_tanggungan) {
        this.nominal_tanggungan = nominal_tanggungan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFoto_bukti() {
        return foto_bukti;
    }

    public void setFoto_bukti(String foto_bukti) {
        this.foto_bukti = foto_bukti;
    }
    
    
}
