/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import com.dak.fkoperasii.utility.Constant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MPartnerCore")
//@Table(name = "m_partner_core")
public class MPartnerCore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "m_partner_core_core_partner_id_seq")
    @SequenceGenerator(name = "m_partner_core_core_partner_id_seq", sequenceName = "m_partner_core_core_partner_id_seq")
    @Column(name = "core_partner_id", nullable = false)
    private Long corePartnerId;

    @Column(name = "partner_code", unique = true, nullable = false)
    private String partnerCode;

    @Column(name = "partner_name", nullable = false)
    private String partnerName;

    @Column(name = "partner_password", nullable = false)
    private String partnerPassword;

    public Long getCorePartnerId() {
        return corePartnerId;
    }

    public void setCorePartnerId(Long corePartnerId) {
        this.corePartnerId = corePartnerId;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public void setPartnerCode(String partnerCode) {
        this.partnerCode = partnerCode;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getPartnerPassword() {
        return partnerPassword;
    }

    public void setPartnerPassword(String partnerPassword) {
        this.partnerPassword = partnerPassword;
    }
}
