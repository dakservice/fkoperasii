/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MDepartement")
@Table(name = "tbl_departement")
public class MDepartement {

    @Id
    @Column(name = "id_departement", nullable = false)
    private Integer id_departement;
    
    @Column(name = "nm_departement", nullable = false)
    private String nm_departement;

    public Integer getId_departement() {
        return id_departement;
    }

    public void setId_departement(Integer id_departement) {
        this.id_departement = id_departement;
    }

    public String getNm_departement() {
        return nm_departement;
    }

    public void setNm_departement(String nm_departement) {
        this.nm_departement = nm_departement;
    }

    

    
}
