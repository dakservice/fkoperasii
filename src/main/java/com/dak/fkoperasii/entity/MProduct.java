/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MProduct")
@Table(name = "tbl_barang")
public class MProduct {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "nm_barang", nullable = false)
    private String nm_barang;
    
    @Column(name = "type", nullable = false)
    private String type;
    
    @Column(name = "merk", nullable = false)
    private String merk;
    
    @Column(name = "warna", nullable = false)
    private String warna;
    
    @Column(name = "harga", nullable = false)
    private Integer harga;
    
    @Column(name = "jml_brg", nullable = false)
    private Integer jml_brg;
    
    @Column(name = "ket", nullable = false)
    private String ket;
    
    @Column(name = "kategori", nullable = false)
    private String kategori;
    
    @Column(name = "foto_produk", nullable = false)
    private String foto_produk;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNm_barang() {
        return nm_barang;
    }

    public void setNm_barang(String nm_barang) {
        this.nm_barang = nm_barang;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }
    
    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    public Integer getJml_brg() {
        return jml_brg;
    }

    public void setJml_brg(Integer jml_brg) {
        this.jml_brg = jml_brg;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getFoto_produk() {
        return foto_produk;
    }

    public void setFoto_produk(String foto_produk) {
        this.foto_produk = foto_produk;
    }
 
    
    
}
