/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;


import com.dak.fkoperasii.utility.Constant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MNasabah")
@Table(name = "tbl_anggota")
public class MNasabah {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "nama", nullable = false)
    private String nama;
    
    @Column(name = "identitas", nullable = false)
    private String identitas;
    
    @Column(name = "jk", nullable = false)
    private String jk;
    
    @Column(name = "tmp_lahir", nullable = false)
    private String tmp_lahir;
    
    @Column(name = "tgl_lahir", nullable = false)
    private String tgl_lahir;
    
    @Column(name = "status", nullable = false)
    private String status;
    
    @Column(name = "id_agama", nullable = false)
    private Integer id_agama;
    
    @Column(name = "departement", nullable = false)
    private String departement;
    
    @Column(name = "pekerjaan", nullable = false)
    private String pekerjaan;
    
    @Column(name = "alamat", nullable = false)
    private String alamat;
    
    @Column(name = "kota", nullable = false)
    private String kota;
    
    @Column(name = "notelp", nullable = false)
    private String notelp;
    
    @Column(name = "tgl_daftar", nullable = false)
    private String tgl_daftar;
    
    @Column(name = "jabatan_id", nullable = false)
    private String jabatan_id;
    
    @Column(name = "aktif", nullable = false)
    private String aktif;
    
    @Column(name = "pass_word", nullable = false)
    private String pass_word;
    
    @Column(name = "file_pic", nullable = false)
    private String file_pic;
    
    @Column(name = "status_pegawai", nullable = false)
    private String status_pegawai;
    
    @Column(name = "gaji_pokok", nullable = false)
    private Integer gaji_pokok;
    
    @Column(name = "saldo_plafon")
    private Integer saldo_plafon;

    @Column(name = "id_cabang")
    private Integer id_cabang;

    @Column(name = "nik")
    private String no_ktp;

    @Column(name = "nip")
    private String nip;

    @Column(name = "email")
    private String email;

    @Column(name = "tgl_berhenti")
    private String tgl_berhenti;

    @Column(name = "cif")
    private String cif;

    @Column(name = "id_status_anggota")
    private Integer id_status_anggota;

    @Column(name = "rek_simpeda")
    private String rek_simpeda;
    
    @Column(name = "kode_anggota")
    private String kode_anggota;
    
    @Column(name = "lengkap")
    private String lengkap;


    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getIdentitas() {
        return identitas;
    }

    public void setIdentitas(String identitas) {
        this.identitas = identitas;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getTmp_lahir() {
        return tmp_lahir;
    }

    public void setTmp_lahir(String tmp_lahir) {
        this.tmp_lahir = tmp_lahir;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getId_agama() {
        return id_agama;
    }

    public void setId_agama(Integer id_agama) {
        this.id_agama = id_agama;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getTgl_daftar() {
        return tgl_daftar;
    }

    public void setTgl_daftar(String tgl_daftar) {
        this.tgl_daftar = tgl_daftar;
    }

    public String getJabatan_id() {
        return jabatan_id;
    }

    public void setJabatan_id(String jabatan_id) {
        this.jabatan_id = jabatan_id;
    }

    public String getAktif() {
        return aktif;
    }

    public void setAktif(String aktif) {
        this.aktif = aktif;
    }

    public String getPass_word() {
        return pass_word;
    }

    public void setPass_word(String pass_word) {
        this.pass_word = pass_word;
    }

    public String getFile_pic() {
        return file_pic;
    }

    public void setFile_pic(String file_pic) {
        this.file_pic = file_pic;
    }

    public String getStatus_pegawai() {
        return status_pegawai;
    }

    public void setStatus_pegawai(String status_pegawai) {
        this.status_pegawai = status_pegawai;
    }

    public Integer getGaji_pokok() {
        return gaji_pokok;
    }

    public void setGaji_pokok(Integer gaji_pokok) {
        this.gaji_pokok = gaji_pokok;
    }

    public Integer getSaldo_plafon() {
        return saldo_plafon;
    }

    public void setSaldo_plafon(Integer saldo_plafon) {
        this.saldo_plafon = saldo_plafon;
    }

    public Integer getId_cabang() {
        return id_cabang;
    }

    public void setId_cabang(Integer id_cabang) {
        this.id_cabang = id_cabang;
    }

    public String getNo_ktp() {
        return no_ktp;
    }

    public void setNo_ktp(String no_ktp) {
        this.no_ktp = no_ktp;
    }

    public String getNik() {
        return nip;
    }

    public void setNik(String nip) {
        this.nip = nip;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTgl_berhenti() {
        return tgl_berhenti;
    }

    public void setTgl_berhenti(String tgl_berhenti) {
        this.tgl_berhenti = tgl_berhenti;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public Integer getId_status_anggota() {
        return id_status_anggota;
    }

    public void setId_status_anggota(Integer id_status_anggota) {
        this.id_status_anggota = id_status_anggota;
    }

    public String getRek_simpeda() {
        return rek_simpeda;
    }

    public void setRek_simpeda(String rek_simpeda) {
        this.rek_simpeda = rek_simpeda;
    }

    public String getKode_anggota() {
        return kode_anggota;
    }

    public void setKode_anggota(String kode_anggota) {
        this.kode_anggota = kode_anggota;
    }

    public String getLengkap() {
        return lengkap;
    }

    public void setLengkap(String lengkap) {
        this.lengkap = lengkap;
    }

    
}
