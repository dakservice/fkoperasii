/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MJenisPinjaman")
@Table(name = "jenis_pinjaman")
public class MJenisPinjaman {
    
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "nama_pinjaman", nullable = false)
    private String nama_pinjaman;
    
    @Column(name = "status_pegawai", nullable = false)
    private String status_pegawai;
    
    @Column(name = "suku_bunga", nullable = false)
    private Integer suku_bunga;
    
    @Column(name = "plafon", nullable = false)
    private Integer plafon;
    
    @Column(name = "lama_angsuran", nullable = false)
    private Integer lama_angsuran;
    
    @Column(name = "jenis_angsuran", nullable = false)
    private String jenis_angsuran;

    @Column(name = "status", nullable = false)
    private String status;
    
    @Column(name = "biaya_polis", nullable = false)
    private Integer biaya_polis;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama_pinjaman() {
        return nama_pinjaman;
    }

    public void setNama_pinjaman(String nama_pinjaman) {
        this.nama_pinjaman = nama_pinjaman;
    }

    public String getStatus_pegawai() {
        return status_pegawai;
    }

    public void setStatus_pegawai(String status_pegawai) {
        this.status_pegawai = status_pegawai;
    }

    public Integer getSuku_bunga() {
        return suku_bunga;
    }

    public void setSuku_bunga(Integer suku_bunga) {
        this.suku_bunga = suku_bunga;
    }

    public Integer getPlafon() {
        return plafon;
    }

    public void setPlafon(Integer plafon) {
        this.plafon = plafon;
    }

    public Integer getLama_angsuran() {
        return lama_angsuran;
    }

    public void setLama_angsuran(Integer lama_angsuran) {
        this.lama_angsuran = lama_angsuran;
    }

    public String getJenis_angsuran() {
        return jenis_angsuran;
    }

    public void setJenis_angsuran(String jenis_angsuran) {
        this.jenis_angsuran = jenis_angsuran;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getBiaya_polis() {
        return biaya_polis;
    }

    public void setBiaya_polis(Integer biaya_polis) {
        this.biaya_polis = biaya_polis;
    }
    
    
    
}
