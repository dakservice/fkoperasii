/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MSaldoAnggota")
@Table(name = "tbl_saldo_anggota")
public class MSaldoAnggota {

    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "id_anggota", nullable = false)
    private Integer id_anggota;

    @Column(name = "id_jenis", nullable = false)
    private Integer id_jenis;

    @Column(name = "saldo", nullable = false)
    private Integer saldo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_anggota() {
        return id_anggota;
    }

    public void setId_anggota(Integer id_anggota) {
        this.id_anggota = id_anggota;
    }

    public Integer getId_jenis() {
        return id_jenis;
    }

    public void setId_jenis(Integer id_jenis) {
        this.id_jenis = id_jenis;
    }

    public Integer getSaldo() {
        return saldo;
    }

    public void setSaldo(Integer saldo) {
        this.saldo = saldo;
    }

    
    
}
