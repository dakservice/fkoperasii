/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author arfandiusemahu
 */
@Entity(name = "MPinjamanBarangPromo")
@Table(name = "m_conf_pinjaman_barang_promo")
public class MPinjamanBarangPromo {
    
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "periode_dari", nullable = false)
    private String periode_dari;
    
    @Column(name = "periode_sampai", nullable = false)
    private String periode_sampai;
    
    @Column(name = "tahun", nullable = false)
    private Integer tahun;
    
    @Column(name = "jasa", nullable = false)
    private String jasa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPeriode_dari() {
        return periode_dari;
    }

    public void setPeriode_dari(String periode_dari) {
        this.periode_dari = periode_dari;
    }

    public String getPeriode_sampai() {
        return periode_sampai;
    }

    public void setPeriode_sampai(String periode_sampai) {
        this.periode_sampai = periode_sampai;
    }

    public Integer getTahun() {
        return tahun;
    }

    public void setTahun(Integer tahun) {
        this.tahun = tahun;
    }

    public String getJasa() {
        return jasa;
    }

    public void setJasa(String jasa) {
        this.jasa = jasa;
    }
    
}
