/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MNilaiJaminan")
@Table(name = "tbl_nilai_jamin")
public class MNilaiJaminan {

    @Id
    @Column(name = "id_nilai_jaminan", nullable = false)
    private Integer id_nilai_jaminan;
    
    @Column(name = "nm_jaminan", nullable = false)
    private String nm_jaminan;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;
    
    @Column(name = "persentasi_nj", nullable = false)
    private Float persentasi_nj;
    
    @Column(name = "tmpl_penarikan", nullable = false)
    private Float tmpl_penarikan;

    public Integer getId_nilai_jaminan() {
        return id_nilai_jaminan;
    }

    public void setId_nilai_jaminan(Integer id_nilai_jaminan) {
        this.id_nilai_jaminan = id_nilai_jaminan;
    }

    public String getNm_jaminan() {
        return nm_jaminan;
    }

    public void setNm_jaminan(String nm_jaminan) {
        this.nm_jaminan = nm_jaminan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Float getPresentasi_nj() {
        return persentasi_nj;
    }

    public void setPresentasi_nj(Float presentasi_nj) {
        this.persentasi_nj = presentasi_nj;
    }

    public Float getTmpl_penarikan() {
        return tmpl_penarikan;
    }

    public void setTmpl_penarikan(Float tmpl_penarikan) {
        this.tmpl_penarikan = tmpl_penarikan;
    }
    

    
}
