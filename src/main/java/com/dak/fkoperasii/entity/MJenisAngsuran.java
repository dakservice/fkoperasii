/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MJenisAngsuran")
@Table(name = "jns_angsuran")
public class MJenisAngsuran {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "m_jenis_angsuran_id_seq")
    @SequenceGenerator(name = "m_jenis_angsuran_id_seq", sequenceName = "m_jenis_angsuran_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "ket", nullable = false)
    private Integer ket;
    
    @Column(name = "aktif", nullable = false)
    private String aktif;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKet() {
        return ket;
    }

    public void setKet(Integer ket) {
        this.ket = ket;
    }

    public String getAktif() {
        return aktif;
    }

    public void setAktif(String aktif) {
        this.aktif = aktif;
    }


    
}
