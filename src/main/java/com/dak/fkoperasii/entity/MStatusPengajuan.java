/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import com.dak.fkoperasii.utility.Constant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MStatusPengajuan")
@Table(name = "MStatusPengajuan")
public class MStatusPengajuan {
    
    @Id
    @Column(name = "id_status", nullable = false)
    private Integer id_status;

    @Column(name = "status", nullable = false)
    private String status;
    
    @Column(name = "aktif", nullable = false)
    private String aktif;

    public Integer getId_status() {
        return id_status;
    }

    public void setId_status(Integer id_status) {
        this.id_status = id_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAktif() {
        return aktif;
    }

    public void setAktif(String aktif) {
        this.aktif = aktif;
    }
 
    
    
}
