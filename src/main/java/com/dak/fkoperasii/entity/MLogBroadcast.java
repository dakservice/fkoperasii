package com.dak.fkoperasii.entity;


import javax.persistence.*;

@Entity(name = "MLogBroadcast")
@Table(name = "MLogBroadcast")
public class MLogBroadcast {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "log_broadcast_id_seq")
    @SequenceGenerator(name = "log_broadcast_id_seq", sequenceName = "log_broadcast_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "broadcast_type", nullable = false)
    private String BroadcastType;

    @Column(name = "type", nullable = false)
    private String Type;

    @Column(name = "date", nullable = false)
    private String Date;

    @Column(name = "sender", nullable = false)
    private String Sender;

    @Column(name = "receiver", nullable = false)
    private String Receiver;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "response", nullable = false)
    private String Response;

    @Column(name = "system_response", nullable = false,columnDefinition = "TEXT")
    private String SystemResponse;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBroadcastType() {
        return BroadcastType;
    }

    public void setBroadcastType(String broadcastType) {
        BroadcastType = broadcastType;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String sender) {
        Sender = sender;
    }

    public String getReceiver() {
        return Receiver;
    }

    public void setReceiver(String receiver) {
        Receiver = receiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getSystemResponse() {
        return SystemResponse;
    }

    public void setSystemResponse(String systemResponse) {
        SystemResponse = systemResponse;
    }
}
