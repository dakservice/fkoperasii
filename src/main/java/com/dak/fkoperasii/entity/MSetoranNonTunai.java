/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MSetoranNonTunai")
@Table(name = "tbl_setoran_non_tunai")
public class MSetoranNonTunai {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "m_idsetor_seq")
    @SequenceGenerator(name = "m_idsetor_seq", sequenceName = "m_idsetor_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "user_id", nullable = false)
    private Integer user_id;
    
    @Column(name = "tanggal_trx", nullable = false)
    private String tanggal_trx;
    
    @Column(name = "id_simpanan", nullable = false)
    private Integer id_simpanan;
    
    @Column(name = "nominal", nullable = false)
    private Integer nominal;
    
    @Column(name = "bukti_transfer", nullable = false)
    private String bukti_transfer;
    
    @Column(name = "status", nullable = false)
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getTanggal_trx() {
        return tanggal_trx;
    }

    public void setTanggal_trx(String tanggal_trx) {
        this.tanggal_trx = tanggal_trx;
    }

    public Integer getId_simpanan() {
        return id_simpanan;
    }

    public void setId_simpanan(Integer id_simpanan) {
        this.id_simpanan = id_simpanan;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public String getBukti_transfer() {
        return bukti_transfer;
    }

    public void setBukti_transfer(String bukti_transfer) {
        this.bukti_transfer = bukti_transfer;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    
    
}
