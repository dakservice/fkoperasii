/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MPengajuan")
@Table(name = "tbl_pengajuan")
public class MPengajuan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mpengajuan_id_seq")
    @SequenceGenerator(name = "mpengajuan_id_seq", sequenceName = "mpengajuan_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mpengajuan_no_ajuan_seq")
    @SequenceGenerator(name = "mpengajuan_no_ajuan_seq", sequenceName = "mpengajuan_no_ajuan_seq")
    @Column(name = "no_ajuan", nullable = false)
    private String no_ajuan;
    
    @Column(name = "ajuan_id", nullable = false)
    private String ajuan_id;

    @JoinColumn(name = "anggota_id", nullable = false)
    private Integer anggota_id;
    
    @Column(name = "tgl_input", nullable = false)
    private String tgl_input;
    
    @Column(name = "jenis", nullable = false)
    private String jenis;
    
    @Column(name = "nominal", nullable = false)
    private String nominal;
    
    @Column(name = "lama_ags", nullable = false)
    private String lama_ags;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;
    
    @Column(name = "status", nullable = false)
    private String status;
    
    @Column(name = "alasan", nullable = false)
    private String alasan;
    
    @Column(name = "tgl_cair", nullable = false)
    private String tgl_cair;
    
    @Column(name = "tgl_update", nullable = false)
    private String tgl_update;
    
    @Column(name = "id_barang", nullable = false)
    private Integer id_barang;
    
    @Column(name = "id_nilai_jaminan", nullable = false)
    private Integer id_nilai_jaminan;
    
    @Column(name = "biaya_tambahan", nullable = false)
    private Integer biaya_tambahan;
    
    @Column(name = "biaya_admin", nullable = false)
    private Integer biaya_admin;

    @Column(name = "skema_angsuran_bulan", nullable = true)
    private String skema_angsuran_bulan;

    @Column(name = "skema_angsuran_nominal", nullable = true)
    private String skema_angsuran_nominal;

    public String getSkema_angsuran_bulan() {
        return skema_angsuran_bulan;
    }

    public void setSkema_angsuran_bulan(String skema_angsuran_bulan) {
        this.skema_angsuran_bulan = skema_angsuran_bulan;
    }

    public String getSkema_angsuran_nominal() {
        return skema_angsuran_nominal;
    }

    public void setSkema_angsuran_nominal(String skema_angsuran_nominal) {
        this.skema_angsuran_nominal = skema_angsuran_nominal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNo_ajuan() {
        return no_ajuan;
    }

    public void setNo_ajuan(String no_ajuan) {
        this.no_ajuan = no_ajuan;
    }

    public String getAjuan_id() {
        return ajuan_id;
    }

    public void setAjuan_id(String ajuan_id) {
        this.ajuan_id = ajuan_id;
    }

    public Integer getAnggota_id() {
        return anggota_id;
    }

    public void setAnggota_id(Integer anggota_id) {
        this.anggota_id = anggota_id;
    }

        public String getTgl_input() {
        return tgl_input;
    }

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getLama_ags() {
        return lama_ags;
    }

    public void setLama_ags(String lama_ags) {
        this.lama_ags = lama_ags;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAlasan() {
        return alasan;
    }

    public void setAlasan(String alasan) {
        this.alasan = alasan;
    }

    public String getTgl_cair() {
        return tgl_cair;
    }

    public void setTgl_cair(String tgl_cair) {
        this.tgl_cair = tgl_cair;
    }

    public String getTgl_update() {
        return tgl_update;
    }

    public void setTgl_update(String tgl_update) {
        this.tgl_update = tgl_update;
    }

    public Integer getId_barang() {
        return id_barang;
    }

    public void setId_barang(Integer id_barang) {
        this.id_barang = id_barang;
    }

    public Integer getId_nilai_jaminan() {
        return id_nilai_jaminan;
    }

    public void setId_nilai_jaminan(Integer id_nilai_jaminan) {
        this.id_nilai_jaminan = id_nilai_jaminan;
    }

    public Integer getBiaya_tambahan() {
        return biaya_tambahan;
    }

    public void setBiaya_tambahan(Integer biaya_tambahan) {
        this.biaya_tambahan = biaya_tambahan;
    }

    public Integer getBiaya_admin() {
        return biaya_admin;
    }

    public void setBiaya_admin(Integer biaya_admin) {
        this.biaya_admin = biaya_admin;
    }
    
    
    
}
