/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Morten Jonathan
 */
@Entity
@Table(name = "MTokenSetorTarikDetail")
public class MTokenSetorTarikDetail {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "token_tariksetordetail_id_seq")
    @SequenceGenerator(name = "token_tariksetordetail_id_seq", sequenceName = "token_tariksetordetail_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "token_id")
    private Integer token_id;
    
    @Column(name = "id_simpanan")
    private Integer id_simpanan;
    
    @Column(name = "nominal")
    private Integer nominal;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getToken_id() {
        return token_id;
    }

    public void setToken_id(Integer token_id) {
        this.token_id = token_id;
    }

    public Integer getId_simpanan() {
        return id_simpanan;
    }

    public void setId_simpanan(Integer id_simpanan) {
        this.id_simpanan = id_simpanan;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    
    
    
}
