/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;


import com.dak.fkoperasii.utility.Constant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MDetailPinjaman")
@Table(name = "tbl_pinjaman_d")
public class MDetailPinjaman {

    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "tgl_bayar", nullable = false)
    private String tgl_bayar;
    
    @Column(name = "pinjam_id", nullable = false)
    private Integer pinjam_id;
    
    @Column(name = "angsuran_ke", nullable = false)
    private Integer angsuran_ke;
    
    @Column(name = "jumlah_bayar", nullable = false)
    private Integer jumlah_bayar;
    
    @Column(name = "denda_rp", nullable = false)
    private Integer denda_rp;
    
    @Column(name = "terlambat", nullable = false)
    private Integer terlambat;
    
    @Column(name = "ket_bayar", nullable = false)
    private String ket_bayar;
    
    @Column(name = "dk", nullable = false)
    private String dk;
    
    @Column(name = "kas_id", nullable = false)
    private Integer kas_id;
    
    @Column(name = "jns_trans", nullable = false)
    private Integer jns_trans;
    
    @Column(name = "update_data", nullable = false)
    private String update_data;
       
    @Column(name = "user_name", nullable = false)
    private String user_name;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;
    
    @Column(name = "biaya_administrasi", nullable = false)
    private Integer biaya_administrasi;
    
    @Column(name = "biaya_provisi", nullable = false)
    private Integer biaya_provisi;
    
    @Column(name = "id_kolektibilitas", nullable = false)
    private Integer id_kolektibilitas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTgl_bayar() {
        return tgl_bayar;
    }

    public void setTgl_bayar(String tgl_bayar) {
        this.tgl_bayar = tgl_bayar;
    }

    public Integer getPinjam_id() {
        return pinjam_id;
    }

    public void setPinjam_id(Integer pinjam_id) {
        this.pinjam_id = pinjam_id;
    }

    public Integer getAngsuran_ke() {
        return angsuran_ke;
    }

    public void setAngsuran_ke(Integer angsuran_ke) {
        this.angsuran_ke = angsuran_ke;
    }

    public Integer getJumlah_bayar() {
        return jumlah_bayar;
    }

    public void setJumlah_bayar(Integer jumlah_bayar) {
        this.jumlah_bayar = jumlah_bayar;
    }

    public Integer getDenda_rp() {
        return denda_rp;
    }

    public void setDenda_rp(Integer denda_rp) {
        this.denda_rp = denda_rp;
    }

    public Integer getTerlambat() {
        return terlambat;
    }

    public void setTerlambat(Integer terlambat) {
        this.terlambat = terlambat;
    }

    public String getKet_bayar() {
        return ket_bayar;
    }

    public void setKet_bayar(String ket_bayar) {
        this.ket_bayar = ket_bayar;
    }

    public String getDk() {
        return dk;
    }

    public void setDk(String dk) {
        this.dk = dk;
    }

    public Integer getKas_id() {
        return kas_id;
    }

    public void setKas_id(Integer kas_id) {
        this.kas_id = kas_id;
    }

    public Integer getJns_trans() {
        return jns_trans;
    }

    public void setJns_trans(Integer jns_trans) {
        this.jns_trans = jns_trans;
    }

    public String getUpdate_data() {
        return update_data;
    }

    public void setUpdate_data(String update_data) {
        this.update_data = update_data;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Integer getBiaya_administrasi() {
        return biaya_administrasi;
    }

    public void setBiaya_administrasi(Integer biaya_administrasi) {
        this.biaya_administrasi = biaya_administrasi;
    }

    public Integer getBiaya_provisi() {
        return biaya_provisi;
    }

    public void setBiaya_provisi(Integer biaya_provisi) {
        this.biaya_provisi = biaya_provisi;
    }
    
    

    public Integer getId_kolektibilitas() {
        return id_kolektibilitas;
    }

    public void setId_kolektibilitas(Integer id_kolektibilitas) {
        this.id_kolektibilitas = id_kolektibilitas;
    }
    
    
    
    
}
