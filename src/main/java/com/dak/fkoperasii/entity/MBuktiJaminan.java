/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MBuktiJaminan")
@Table(name = "tbl_bukti_jaminan")
public class MBuktiJaminan {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mbukti_id_seq")
    @SequenceGenerator(name = "mbukti_id_seq", sequenceName = "mbukti_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "id_bukti_jaminan", nullable = false)
    private Integer id_bukti_jaminan;
    
    @Column(name = "foto_bukti", nullable = false)
    private String foto_bukti;
    
    @Column(name = "nilai_agunan", nullable = false)
    private Integer nilai_agunan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_bukti_jaminan() {
        return id_bukti_jaminan;
    }

    public void setId_bukti_jaminan(Integer id_bukti_jaminan) {
        this.id_bukti_jaminan = id_bukti_jaminan;
    }

    public String getFoto_bukti() {
        return foto_bukti;
    }

    public void setFoto_bukti(String foto_bukti) {
        this.foto_bukti = foto_bukti;
    }

    public Integer getNilai_agunan() {
        return nilai_agunan;
    }

    public void setNilai_agunan(Integer nilai_agunan) {
        this.nilai_agunan = nilai_agunan;
    }
    
    
    
}
