/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MKas")
@Table(name = "nama_kas_tbl")
public class MKas {

    @Id
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "id_coa", nullable = false)
    private Integer id_coa;
    
    @Column(name = "nama", nullable = false)
    private String nama;
    
    @Column(name = "tmpl_simpan", nullable = false)
    private String tmpl_simpan;
    
    @Column(name = "tmpl_penarikan", nullable = false)
    private String tmpl_penarikan;
    
    @Column(name = "tmpl_pinjaman", nullable = false)
    private String tmpl_pinjaman;
    
    @Column(name = "tmpl_bayar", nullable = false)
    private String tmpl_bayar;
    
    @Column(name = "tmpl_pemasukan", nullable = false)
    private String tmpl_pemasukan;
    
    @Column(name = "tmpl_pengeluaran", nullable = false)
    private String tmpl_pengeluaran;
    
    @Column(name = "tmpl_transfer", nullable = false)
    private String tmpl_transfer;
    
    @Column(name = "trans_pulsa", nullable = false)
    private String trans_pulsa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_coa() {
        return id_coa;
    }

    public void setId_coa(Integer id_coa) {
        this.id_coa = id_coa;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTmpl_simpan() {
        return tmpl_simpan;
    }

    public void setTmpl_simpan(String tmpl_simpan) {
        this.tmpl_simpan = tmpl_simpan;
    }

    public String getTmpl_penarikan() {
        return tmpl_penarikan;
    }

    public void setTmpl_penarikan(String tmpl_penarikan) {
        this.tmpl_penarikan = tmpl_penarikan;
    }

    public String getTmpl_pinjaman() {
        return tmpl_pinjaman;
    }

    public void setTmpl_pinjaman(String tmpl_pinjaman) {
        this.tmpl_pinjaman = tmpl_pinjaman;
    }

    public String getTmpl_bayar() {
        return tmpl_bayar;
    }

    public void setTmpl_bayar(String tmpl_bayar) {
        this.tmpl_bayar = tmpl_bayar;
    }

    public String getTmpl_pemasukan() {
        return tmpl_pemasukan;
    }

    public void setTmpl_pemasukan(String tmpl_pemasukan) {
        this.tmpl_pemasukan = tmpl_pemasukan;
    }

    public String getTmpl_pengeluaran() {
        return tmpl_pengeluaran;
    }

    public void setTmpl_pengeluaran(String tmpl_pengeluaran) {
        this.tmpl_pengeluaran = tmpl_pengeluaran;
    }

    public String getTmpl_transfer() {
        return tmpl_transfer;
    }

    public void setTmpl_transfer(String tmpl_transfer) {
        this.tmpl_transfer = tmpl_transfer;
    }

    public String getTrans_pulsa() {
        return trans_pulsa;
    }

    public void setTrans_pulsa(String trans_pulsa) {
        this.trans_pulsa = trans_pulsa;
    }

    
}
