/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MPersyaratanPengajuan")
@Table(name = "tbl_persyaratan_pengajuan")
public class MPersyaratanPengajuan {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "msyarat_id_seq")
    @SequenceGenerator(name = "msyarat_id_seq", sequenceName = "msyarat_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "id_persyaratan", nullable = false)
    private Integer id_persyaratan;
    
    @Column(name = "file_persyaratan", nullable = false)
    private String file_persyaratan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_persyaratan() {
        return id_persyaratan;
    }

    public void setId_persyaratan(Integer id_persyaratan) {
        this.id_persyaratan = id_persyaratan;
    }

    public String getFile_persyaratan() {
        return file_persyaratan;
    }

    public void setFile_persyaratan(String file_persyaratan) {
        this.file_persyaratan = file_persyaratan;
    }
    
    
    
}
