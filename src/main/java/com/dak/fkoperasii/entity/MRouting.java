/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MRouting")
@Table(name = "MRouting")
public class MRouting {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "m_routing_id_seq")
    @SequenceGenerator(name = "m_routing_id_seq", sequenceName = "m_routing_id_seq")
    @Column(name = "id", nullable = false)
    private int id;
    
    @Column(name = "client")
    private String client;
    
    @Column(name = "url")
    private String url;
    
    @Column(name = "param")
    @Type(type = "text")
    private String param;
    
    @Column(name = "status")
    private Boolean status;
    
    @Column(name = "txn_mgr")
    private String txn_mgr;
    
    @Column(name = "trx_type")
    private String trxType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getTxn_mgr() {
        return txn_mgr;
    }

    public void setTxn_mgr(String txn_mgr) {
        this.txn_mgr = txn_mgr;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }
    
    
    
}
