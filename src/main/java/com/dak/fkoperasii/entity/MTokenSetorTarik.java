/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Morten Jonathan
 */
@Entity
@Table(name = "MTokenSetorTarik")
public class MTokenSetorTarik {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "token_tariksetor_id_seq")
    @SequenceGenerator(name = "token_tariksetor_id_seq", sequenceName = "token_tariksetor_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "token_number")
    private String token_number;
    
    @Column(name = "expired_date")
    private String expired_date;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "action")
    private String action;
    
    @Column(name = "id_user")
    private String id_user;

    @Column(name = "tanggal_trx")
    private String tanggal_trx;

    @Column(name = "date_created")
    private String date_created;

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_created() {
        return date_created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken_number() {
        return token_number;
    }

    public void setToken_number(String token_number) {
        this.token_number = token_number;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getTanggal_trx() {
        return tanggal_trx;
    }

    public void setTanggal_trx(String tanggal_trx) {
        this.tanggal_trx = tanggal_trx;
    }

    
}
