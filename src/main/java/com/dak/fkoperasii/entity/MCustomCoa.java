/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MCustomCoa")
@Table(name = "custom_coa")
public class MCustomCoa {
    
    @Id
    @Column(name = "id_custom", nullable = false)
    private Integer id_custom;

    @Column(name = "nm_label", nullable = false)
    private String nm_label;
    
    @Column(name = "name_db", nullable = false)
    private String name_db;
    
    @Column(name = "name_field", nullable = false)
    private String name_field;
    
    @Column(name = "id_link_menu", nullable = false)
    private Integer id_link_menu;
    
    @Column(name = "id_coa", nullable = false)
    private Integer id_coa;
    
    @Column(name = "status_aktif", nullable = false)
    private String status_aktif;
    
    @Column(name = "status_jurnal", nullable = false)
    private String status_jurnal;
    
    @Column(name = "value_awal", nullable = false)
    private Integer value_awal;

    public Integer getId_custom() {
        return id_custom;
    }

    public void setId_custom(Integer id_custom) {
        this.id_custom = id_custom;
    }

    public String getNm_label() {
        return nm_label;
    }

    public void setNm_label(String nm_label) {
        this.nm_label = nm_label;
    }

    public String getName_db() {
        return name_db;
    }

    public void setName_db(String name_db) {
        this.name_db = name_db;
    }

    public String getName_field() {
        return name_field;
    }

    public void setName_field(String name_field) {
        this.name_field = name_field;
    }

    public Integer getId_link_menu() {
        return id_link_menu;
    }

    public void setId_link_menu(Integer id_link_menu) {
        this.id_link_menu = id_link_menu;
    }

    public Integer getId_coa() {
        return id_coa;
    }

    public void setId_coa(Integer id_coa) {
        this.id_coa = id_coa;
    }

    public String getStatus_aktif() {
        return status_aktif;
    }

    public void setStatus_aktif(String status_aktif) {
        this.status_aktif = status_aktif;
    }

    public String getStatus_jurnal() {
        return status_jurnal;
    }

    public void setStatus_jurnal(String status_jurnal) {
        this.status_jurnal = status_jurnal;
    }

    public Integer getValue_awal() {
        return value_awal;
    }

    public void setValue_awal(Integer value_awal) {
        this.value_awal = value_awal;
    }
    
 
    
    
}
