package com.dak.fkoperasii.entity;

import com.dak.fkoperasii.utility.Constant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author risyamaulana
 */
@Entity(name = "MRc")
//@Table(name = "m_rc")
public class MRc {

    @Id
    @Column(name = "response_code", nullable = false)
    private String rc;

    @Column(name = "response_message", nullable = false)
    private String rm;

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public String getRm() {
        return rm;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

}
