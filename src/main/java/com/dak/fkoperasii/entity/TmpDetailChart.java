/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "TmpDetailChart")
@Table(name = "tbl_tmp_dtl_chart")
public class TmpDetailChart {
   
    @Id
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "id_barang")
    private Long id_barang;
    
    @Column(name = "jml")
    private Integer jml;
    
    @Column(name = "status_chart")
    private String status_chart;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Long getId_barang() {
        return id_barang;
    }

    public void setId_barang(Long id_barang) {
        this.id_barang = id_barang;
    }

    public Integer getJml() {
        return jml;
    }

    public void setJml(Integer jml) {
        this.jml = jml;
    }

    public String getStatus_chart() {
        return status_chart;
    }

    public void setStatus_chart(String status_chart) {
        this.status_chart = status_chart;
    }
    
    
}
