/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MAgama")
@Table(name = "tbl_agama")
public class MAgama {

    @Id
    @Column(name = "id_agama", nullable = false)
    private Integer id_agama;
    
    @Column(name = "nm_agama", nullable = false)
    private String nm_agama;

    public Integer getId_agama() {
        return id_agama;
    }

    public void setId_agama(Integer id_agama) {
        this.id_agama = id_agama;
    }

    public String getNm_agama() {
        return nm_agama;
    }

    public void setNm_agama(String nm_agama) {
        this.nm_agama = nm_agama;
    }
    
    

    
}
