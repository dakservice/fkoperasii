/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MLogTrx")
@Table(name = "MLogTrx")
public class MLogTrx {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "m_log_idtrx_seq")
    @SequenceGenerator(name = "m_log_idtrx_seq", sequenceName = "m_log_idtrx_seq")
    @Column(name = "id_logtrx")
    private Long id_logtrx;
    
    @Column(name = "created_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date created_time;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "info")
    private String info;
    
    @Column(name = "status", length = 20)
    private String status;
    
    @Column(name = "transaction_type")
    private String transaction_type;
    
    @Column(name = "user_created")
    private String user_created;

    @Column(name = "user_updated")
    private String user_updated;
    
    @Column(name = "id_order")
    private String id_order;
    
//    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MRequest.class)
//    @JoinColumn(name = "id_request")
//    private MRequest id_request;
    
//    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MNasabah.class)
//    @JoinColumn(name = "anggota_id")
    @Column(name = "anggota_id")
    private String anggota_id;
    
    @Column(name = "total_fee")
    private double total_fee;
    
    @Column(name = "nominal")
    private double nominal;
    
    @Column(name = "reff")
    private String reff;

    @Column(name = "rrn")
    private String rrn;
    
    @Column(name = "stan")
    private String stan;
    
    @Column(name = "type")
    private String type;
    
    @Column(name = "level_setby")
    private String level_setby;
    
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MProduct.class)
    @JoinColumn(name = "id_product")
    private MProduct id_product;
    
    @Column(name = "amount")
    private double amount = 0;

    @Column(name = "mod_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date mod_time;

    @Column(name = "response_msg")
    private String responseMsg = "";

    @Column(name = "rc")
    private String rc = "";

    public Long getId_logtrx() {
        return id_logtrx;
    }

    public void setId_logtrx(Long id_logtrx) {
        this.id_logtrx = id_logtrx;
    }

    public Date getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Date created_time) {
        this.created_time = created_time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getUser_created() {
        return user_created;
    }

    public void setUser_created(String user_created) {
        this.user_created = user_created;
    }

    public String getUser_updated() {
        return user_updated;
    }

    public void setUser_updated(String user_updated) {
        this.user_updated = user_updated;
    }

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getAnggota_id() {
        return anggota_id;
    }

    public void setAnggota_id(String anggota_id) {
        this.anggota_id = anggota_id;
    }
    
    public double getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(double total_fee) {
        this.total_fee = total_fee;
    }

    public double getNominal() {
        return nominal;
    }

    public void setNominal(double nominal) {
        this.nominal = nominal;
    }

    public String getReff() {
        return reff;
    }

    public void setReff(String reff) {
        this.reff = reff;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLevel_setby() {
        return level_setby;
    }

    public void setLevel_setby(String level_setby) {
        this.level_setby = level_setby;
    }

    public MProduct getId_product() {
        return id_product;
    }

    public void setId_product(MProduct id_product) {
        this.id_product = id_product;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    public Date getMod_time() {
        return mod_time;
    }

    public void setMod_time(Date mod_time) {
        this.mod_time = mod_time;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }


    
    
}
