/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MJurnal")
@Table(name = "tbl_jurnal")
public class MJurnal {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mjurnal_id_jurnal_seq")
    @SequenceGenerator(name = "mjurnal_id_jurnal_seq", sequenceName = "mjurnal_id_jurnal_seq")
    @Column(name = "id_jurnal", nullable = false)
    private Integer id_jurnal;

    @Column(name = "id_transaksi", nullable = false)
    private String id_transaksi;
    
    @Column(name = "outlet_id", nullable = false)
    private Integer outlet_id;
    
    @Column(name = "tgl_jurnal", nullable = false)
    private String tgl_jurnal;
    
    @Column(name = "kd_akun", nullable = false)
    private String kd_akun;
    
    @Column(name = "posisi", nullable = false)
    private String posisi;
    
    @Column(name = "nominal", nullable = false)
    private Double nominal;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;

    public Integer getId_jurnal() {
        return id_jurnal;
    }

    public void setId_jurnal(Integer id_jurnal) {
        this.id_jurnal = id_jurnal;
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public Integer getOutlet_id() {
        return outlet_id;
    }

    public void setOutlet_id(Integer outlet_id) {
        this.outlet_id = outlet_id;
    }
    
    public String getTgl_jurnal() {
        return tgl_jurnal;
    }

    public void setTgl_jurnal(String tgl_jurnal) {
        this.tgl_jurnal = tgl_jurnal;
    }

    public String getKd_akun() {
        return kd_akun;
    }

    public void setKd_akun(String kd_akun) {
        this.kd_akun = kd_akun;
    }

    public String getPosisi() {
        return posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }

    public Double getNominal() {
        return nominal;
    }

    public void setNominal(Double nominal) {
        this.nominal = nominal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
  
    
    
}
