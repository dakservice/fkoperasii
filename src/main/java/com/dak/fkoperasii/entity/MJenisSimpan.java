/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MJenisSimpan")
@Table(name = "jns_simpan")
public class MJenisSimpan {
    
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "id_coa", nullable = false)
    private Integer id_coa;
    
    @Column(name = "jns_simpan", nullable = false)
    private String jns_simpan;
    
    @Column(name = "jumlah", nullable = false)
    private Double jumlah;
    
    @Column(name = "tampil", nullable = false)
    private String tampil;

    @Column(name = "tarik_tunai", nullable = false)
    private String tarik_tunai;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_coa() {
        return id_coa;
    }

    public void setId_coa(Integer id_coa) {
        this.id_coa = id_coa;
    }

    public String getJns_simpan() {
        return jns_simpan;
    }

    public void setJns_simpan(String jns_simpan) {
        this.jns_simpan = jns_simpan;
    }

    public Double getJumlah() {
        return jumlah;
    }

    public void setJumlah(Double jumlah) {
        this.jumlah = jumlah;
    }

    public String getTampil() {
        return tampil;
    }

    public void setTampil(String tampil) {
        this.tampil = tampil;
    }

    public String getTarik_tunai() {
        return tarik_tunai;
    }

    public void setTarik_tunai(String tarik_tunai) {
        this.tarik_tunai = tarik_tunai;
    }


    
}
