/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MTransSP")
@Table(name = "tbl_trans_sp")
public class MTransSP {
    
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "tgl_transaksi", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date tgl_transaksi;
//    private String tgl_transaksi;
    
    @Column(name = "anggota_id", nullable = false)
    private Integer anggota_id;
    
    @Column(name = "jenis_id", nullable = false)
    private Integer jenis_id;
    
    @Column(name = "jumlah", nullable = false)
    private Double jumlah;
    
    @Column(name = "keterangan", nullable = false)
    private String keterangan;
    
    @Column(name = "akun", nullable = false)
    private String akun;
    
    @Column(name = "dk", nullable = false)
    private String dk;
    
    @Column(name = "kas_id", nullable = false)
    private Integer kas_id;
    
    @Column(name = "update_data", nullable = false)
    private String update_data;
    
    @Column(name = "user_name", nullable = false)
    private String user_name;
    
    @Column(name = "nama_penyetor", nullable = false)
    private String nama_penyetor;
    
    @Column(name = "no_identitas", nullable = false)
    private String no_identitas;
    
    @Column(name = "alamat", nullable = false)
    private String alamat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    public String getTgl_transaksi() {
//        return tgl_transaksi;
//    }
//
//    public void setTgl_transaksi(String tgl_transaksi) {
//        this.tgl_transaksi = tgl_transaksi;
//    }

    public Date getTgl_transaksi() {
        return tgl_transaksi;
    }

    public void setTgl_transaksi(Date tgl_transaksi) {
        this.tgl_transaksi = tgl_transaksi;
    }

    
    
    public Integer getAnggota_id() {
        return anggota_id;
    }

    public void setAnggota_id(Integer anggota_id) {
        this.anggota_id = anggota_id;
    }

    public Integer getJenis_id() {
        return jenis_id;
    }

    public void setJenis_id(Integer jenis_id) {
        this.jenis_id = jenis_id;
    }

    public Double getJumlah() {
        return jumlah;
    }

    public void setJumlah(Double jumlah) {
        this.jumlah = jumlah;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getAkun() {
        return akun;
    }

    public void setAkun(String akun) {
        this.akun = akun;
    }

    public String getDk() {
        return dk;
    }

    public void setDk(String dk) {
        this.dk = dk;
    }

    public Integer getKas_id() {
        return kas_id;
    }

    public void setKas_id(Integer kas_id) {
        this.kas_id = kas_id;
    }

    public String getUpdate_data() {
        return update_data;
    }

    public void setUpdate_data(String update_data) {
        this.update_data = update_data;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getNama_penyetor() {
        return nama_penyetor;
    }

    public void setNama_penyetor(String nama_penyetor) {
        this.nama_penyetor = nama_penyetor;
    }

    public String getNo_identitas() {
        return no_identitas;
    }

    public void setNo_identitas(String no_identitas) {
        this.no_identitas = no_identitas;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    
    
    
}
