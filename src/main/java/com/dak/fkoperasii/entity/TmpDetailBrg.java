/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "TmpDetailBrg")
@Table(name = "tbl_tmp_dtl_brg")
public class TmpDetailBrg {
   
    @Id
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "id_barang")
    private Integer id_barang;
    
    @Column(name = "jml_brg")
    private Integer jml_brg;
    
    @Column(name = "status_pengajuan")
    private String status_pengajuan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_barang() {
        return id_barang;
    }

    public void setId_barang(Integer id_barang) {
        this.id_barang = id_barang;
    }
  
    public Integer getJml_brg() {
        return jml_brg;
    }

    public void setJml_brg(Integer jml_brg) {
        this.jml_brg = jml_brg;
    }

    public String getStatus_pengajuan() {
        return status_pengajuan;
    }

    public void setStatus_pengajuan(String status_pengajuan) {
        this.status_pengajuan = status_pengajuan;
    }

    
    
}
