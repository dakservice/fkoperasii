/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.joda.time.DateTime;

/**
 *
 * @author 1star
 */
@Entity(name = "MOtp")
@Table(name = "otp")
public class MOtp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "otp_id_seq")
    @SequenceGenerator(name = "otp_id_seq", sequenceName = "otp_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "otp", nullable = false)
    private String otp;

    @Column(name = "no_telp", nullable = false)
    private String noTelp;

    @Column(name = "create_at", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createAt;

    @Column(name = "mod_at")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date modAt;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "type", nullable = false)
    private String type;

    @PrePersist
    public void prePersist() throws ParseException {

//     SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
//     LocalDateTime myDateObj = LocalDateTime.now();
//     DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
//     Date firstDate = new Date();
//     firstDate.
        DateTime jdate = new DateTime();
        DateTimeFormatter time = DateTimeFormat.forPattern("yyyy-MM-dd  HH:mm:ss");
        String tmpe = jdate.toString(time); 
        
        Date date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(tmpe);

        setCreateAt(date1);
    }

    @PreUpdate
    public void preUpdate() {
        setModAt(new Date());
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the otp
     */
    public String getOtp() {
        return otp;
    }

    /**
     * @param otp the otp to set
     */
    public void setOtp(String otp) {
        this.otp = otp;
    }

    /**
     * @return the createAt
     */
    public Date getCreateAt() {
        return createAt;
    }

    /**
     * @param createAt the createAt to set
     */
    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    /**
     * @return the modAt
     */
    public Date getModAt() {
        return modAt;
    }

    /**
     * @param modAt the modAt to set
     */
    public void setModAt(Date modAt) {
        this.modAt = modAt;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the noTelp
     */
    public String getNoTelp() {
        return noTelp;
    }

    /**
     * @param noTelp the noTelp to set
     */
    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

}
