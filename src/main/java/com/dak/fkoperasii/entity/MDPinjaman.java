/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MDPinjaman")
@Table(name = "MDPinjaman")
public class MDPinjaman {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "mdpinjaman_id_seq")
    @SequenceGenerator(name = "mdpinjaman_id_seq", sequenceName = "mdpinjaman_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "pinjam_id", nullable = false)
    private Integer pinjam_id;
    
    @Column(name = "angsuran", nullable = false)
    private Integer angsuran;
    
    @Column(name = "jlh_angsuran", nullable = false)
    private Integer jlh_angsuran;
    
    @Column(name = "angsuran_jasa", nullable = false)
    private Integer angsuran_jasa;
    
    @Column(name = "biaya_adm", nullable = false)
    private Integer biaya_adm;
    
    @Column(name = "biaya_provisi", nullable = false)
    private Integer biaya_provisi;
    
    @Column(name = "jatuh_tempo", nullable = false)
    private String jatuh_tempo;
    
    @Column(name = "angsuran_pokok", nullable = false)
    private Integer angsuran_pokok;
    
    @Column(name = "status_bayar", nullable = false)
    private String status_bayar;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPinjam_id() {
        return pinjam_id;
    }

    public void setPinjam_id(Integer pinjam_id) {
        this.pinjam_id = pinjam_id;
    }

       
    public Integer getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(Integer angsuran) {
        this.angsuran = angsuran;
    }

    public Integer getJlh_angsuran() {
        return jlh_angsuran;
    }

    public void setJlh_angsuran(Integer jlh_angsuran) {
        this.jlh_angsuran = jlh_angsuran;
    }

    public Integer getAngsuran_jasa() {
        return angsuran_jasa;
    }

    public void setAngsuran_jasa(Integer angsuran_jasa) {
        this.angsuran_jasa = angsuran_jasa;
    }

    public Integer getBiaya_adm() {
        return biaya_adm;
    }

    public void setBiaya_adm(Integer biaya_adm) {
        this.biaya_adm = biaya_adm;
    }

    public Integer getBiaya_provisi() {
        return biaya_provisi;
    }

    public void setBiaya_provisi(Integer biaya_provisi) {
        this.biaya_provisi = biaya_provisi;
    }

    public String getJatuh_tempo() {
        return jatuh_tempo;
    }

    public void setJatuh_tempo(String jatuh_tempo) {
        this.jatuh_tempo = jatuh_tempo;
    }

    public Integer getAngsuran_pokok() {
        return angsuran_pokok;
    }

    public void setAngsuran_pokok(Integer angsuran_pokok) {
        this.angsuran_pokok = angsuran_pokok;
    }

    public String getStatus_bayar() {
        return status_bayar;
    }

    public void setStatus_bayar(String status_bayar) {
        this.status_bayar = status_bayar;
    }
    
    
    
}
