/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MRateAsuransi")
@Table(name = "rate_asuransi")
public class MRateAsuransi {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "m_rate_asuransi_id_seq")
    @SequenceGenerator(name = "m_rate_asuransi_id_seq", sequenceName = "m_rate_asuransi_id_seq")
    @Column(name = "id_rate", nullable = false)
    private Integer id_rate;

    @Column(name = "jangka_waktu", nullable = false)
    private Integer jangka_waktu;
    
    @Column(name = "persentase_rate", nullable = false)
    private Double persentase_rate;

    @Column(name = "status_aktif", nullable = false)
    private String status_aktif;

    public Integer getId_rate() {
        return id_rate;
    }

    public void setId_rate(Integer id_rate) {
        this.id_rate = id_rate;
    }

    public Integer getJangka_waktu() {
        return jangka_waktu;
    }

    public void setJangka_waktu(Integer jangka_waktu) {
        this.jangka_waktu = jangka_waktu;
    }

    public Double getPersentase_rate() {
        return persentase_rate;
    }

    public void setPersentase_rate(Double persentase_rate) {
        this.persentase_rate = persentase_rate;
    }

    public String getStatus_aktif() {
        return status_aktif;
    }

    public void setStatus_aktif(String status_aktif) {
        this.status_aktif = status_aktif;
    }



    
}
