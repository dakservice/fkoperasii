/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.util.Date;
/**
 *
 * @author 1star
 */
@Entity(name = "MDeviceAnggota")
@Table(name = "device_anggota")
public class MDeviceAnggota {
    
    
    @Id
    @Column(name = "device_id", nullable = false)
    private String device_id;
    
    @Column(name = "id_anggota", nullable = false)
    private Long idAnggota;

    @Column(name = "devices_name")
    private String devices_name;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "updated_n")
    private Date updatedOn;
    
    @Column(name = "created_on")
    private Date createdOn;
    
      @PrePersist
    public void prePersist() {
        setCreatedOn(new Date());
//        createdBy = LoggedUser.get();
    }
 
    @PreUpdate
    public void preUpdate() {
        setUpdatedOn(new Date());
//        updatedBy = LoggedUser.get();
    }

    /**
     * @return the device_id
     */
    public String getDevice_id() {
        return device_id;
    }

    /**
     * @param device_id the device_id to set
     */
    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    /**
     * @return the devices_name
     */
    public String getDevices_name() {
        return devices_name;
    }

    /**
     * @param devices_name the devices_name to set
     */
    public void setDevices_name(String devices_name) {
        this.devices_name = devices_name;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the updatedOn
     */
    public Date getUpdatedOn() {
        return updatedOn;
    }

    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the idAnggota
     */
    public Long getIdAnggota() {
        return idAnggota;
    }

    /**
     * @param idAnggota the idAnggota to set
     */
    public void setIdAnggota(Long idAnggota) {
        this.idAnggota = idAnggota;
    }
}
