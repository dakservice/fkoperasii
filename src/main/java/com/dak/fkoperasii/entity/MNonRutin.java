/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Morten Jonathan
 */
@Entity(name = "MNonRutin")
@Table(name = "tbl_non_rutin")
public class MNonRutin {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "nonrutin_id_seq")
    @SequenceGenerator(name = "nonrutin_id_seq", sequenceName = "nonrutin_id_seq")
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "id_non_rutin", nullable = false)
    private Integer id_non_rutin;

    @Column(name = "id_pengajuan", nullable = false)
    private Integer id_pengajuan;

    @Column(name = "jatuh_tempo", nullable = false)
    private String jatuh_tempo;
    
    @Column(name = "angsuran_ke", nullable = false)
    private Integer angsuran_ke;
    
    @Column(name = "pokok", nullable = false)
    private Integer pokok;

    @Column(name = "bunga", nullable = false)
    private Integer bunga;
    
    @Column(name = "angsuran", nullable = false)
    private Integer angsuran;
    
    @Column(name = "denda", nullable = false)
    private Integer denda;

    @Column(name = "total_bayar", nullable = false)
    private Integer total_bayar;
    
    @Column(name = "status", nullable = false)
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_non_rutin() {
        return id_non_rutin;
    }

    public void setId_non_rutin(Integer id_non_rutin) {
        this.id_non_rutin = id_non_rutin;
    }

    public Integer getId_pengajuan() {
        return id_pengajuan;
    }

    public void setId_pengajuan(Integer id_pengajuan) {
        this.id_pengajuan = id_pengajuan;
    }
    

    public String getJatuh_tempo() {
        return jatuh_tempo;
    }

    public void setJatuh_tempo(String jatuh_tempo) {
        this.jatuh_tempo = jatuh_tempo;
    }

    public Integer getAngsuran_ke() {
        return angsuran_ke;
    }

    public void setAngsuran_ke(Integer angsuran_ke) {
        this.angsuran_ke = angsuran_ke;
    }

    public Integer getPokok() {
        return pokok;
    }

    public void setPokok(Integer pokok) {
        this.pokok = pokok;
    }

    public Integer getBunga() {
        return bunga;
    }

    public void setBunga(Integer bunga) {
        this.bunga = bunga;
    }

    public Integer getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(Integer angsuran) {
        this.angsuran = angsuran;
    }

    public Integer getDenda() {
        return denda;
    }

    public void setDenda(Integer denda) {
        this.denda = denda;
    }

    public Integer getTotal_bayar() {
        return total_bayar;
    }

    public void setTotal_bayar(Integer total_bayar) {
        this.total_bayar = total_bayar;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
