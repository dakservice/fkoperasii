/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.product;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MProduct;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class ViewAllProduct implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            log.info("================= MASUK TRY ");
            String notelp = jsonReq.getString("notelp");
            String kategori = jsonReq.getString("kategori");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            log.info("================= MASUK NASABAH " + nsbh.getAktif());

            if (nsbh.getAktif().equals("Y")) {

                log.info("================= MASUK IF ");
                JSONArray data = new JSONArray();
                log.info("================= SEBELUM LIST.");
                List<MProduct> mp = SpringInit.getMProductDao().productByKategori(kategori);
                log.info("================= SEBELUM LIST. DATA: " + mp.size());
                log.info("================= MASUK LIST ");
                for (MProduct datamp : mp) {
                    JSONObject logData = new JSONObject();
                    logData.put("id_produk", datamp.getId());
                    logData.put("nama_produk", datamp.getNm_barang());
                    logData.put("merk", datamp.getMerk());
                    logData.put("warna", datamp.getWarna());
                    logData.put("harga", datamp.getHarga().toString());
                    logData.put("jumlah_produk", datamp.getJml_brg().toString());

                    String foto_produk = datamp.getFoto_produk();
                    log.info("================= MASUK FOTO ");
                    if (foto_produk != null) {
                        if (!"".equals(foto_produk)) {
                            String[] data_image = foto_produk.split(";", -1);
                            logData.put("foto_produk", SpringInit.getMRoutingDao().getConstantDB("img_url").getValue() + data_image[0]);
                        } else {
                            logData.put("foto_produk", "");
                        }
                    } else {
                        logData.put("foto_produk", "");
                    }

                    data.put(logData);

                    jsonReq.put("data", data);
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
                }
            } else {
                //                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_AUTH_FAILED);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }

        } catch (Exception e) {
//             log.info(ExceptionUtils.getStackTrace(e));
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }

        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
