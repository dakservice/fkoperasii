/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.product;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.TmpDetailBrg;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class addToKeranjangBrg implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {

            log.info("================= MASUK TRY ");
            String notelp = jsonReq.getString("notelp");
            String id_produk = jsonReq.getString("id_produk");
            String qty = jsonReq.getString("qty");

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            TmpDetailBrg tdb = SpringInit.getTmpDetailBrgDao().getDetailByIdBarang(nsbh.getId().intValue(), Integer.parseInt(id_produk));
            log.info("================= MASUK NASABAH " + nsbh.getAktif());
            log.info("================= MASUK TMP DETAIL BRG " + tdb);

            if (nsbh != null) {

                if (nsbh.getAktif().equals("Y")) {

                    if (tdb == null) {

                        log.info("================= MASUK IF ");
                        TmpDetailBrg tmp = new TmpDetailBrg();
                        tmp.setId(Integer.parseInt(nsbh.getId().toString()));
                        tmp.setId_barang(Integer.parseInt(id_produk));
                        tmp.setJml_brg(Integer.parseInt(qty));
                        tmp.setStatus_pengajuan("Pengajuan Barang");

                        log.info("================= MASUK MAU SAVE ");
                        tmp = SpringInit.getTmpDetailBrgDao().saveOrUpdate(tmp);
                        //                mlogmobile id user
                        ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                        mACtivity.setIdUser(nsbh.getId());
                        ctx.put(Constant.WS.REQUEST, mACtivity);
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                    } else {
                        //                mlogmobile id user
                        ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                        mACtivity.setIdUser(nsbh.getId());
                        ctx.put(Constant.WS.REQUEST, mACtivity);
                        rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.ALREADY_IN_THE_CART);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                    }
                } else {
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_AUTH_FAILED);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            } else {
                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }

        } catch (Exception e) {

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }

}
