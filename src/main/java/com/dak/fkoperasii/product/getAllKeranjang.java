/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.product;

import com.dak.fkoperasii.entity.MCustomCoa;
import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MNilaiJaminan;
import com.dak.fkoperasii.entity.MPresentasePlafon;
import com.dak.fkoperasii.entity.MProduct;
import com.dak.fkoperasii.entity.MRateAsuransi;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MTanggunganPlafon;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class getAllKeranjang implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    DecimalFormat df = new DecimalFormat("#");

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);

        JSONArray array = new JSONArray();
        int harga = 0;
        int jumlah = 0;
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            String notelp = jsonReq.getString("notelp");
            log.info("========================= NO TELEPON " + notelp);

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            if (nsbh != null) {
                String jenis_pinjaman = "Pinjaman Motor";
                MJenisPinjaman mjp = SpringInit.getMJenisPinjamanDao().jenisPinjamanByNama(jenis_pinjaman, nsbh.getStatus_pegawai());
                Long anggota_id = Long.parseLong(nsbh.getId().toString());
                log.info("====================== ANGGOTA ID " + anggota_id);
                JSONArray data = new JSONArray();
                JSONArray arrays2 = new JSONArray();

                List<Object[]> tmp = SpringInit.getTmpDetailChartDao().getAllChartNative(anggota_id);
                log.info("====================== MASUK LIST " + tmp);
                log.info("====================== SIZE KERANJANG " + tmp.size());
                int sz = tmp.size();
                int sub = 0;
                Integer total = 0;

                log.info("====================== MASUK IF");
                if (sz <= 0) {
                    log.info("====================== SIZE KERANJANG " + sz);
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                } else if (sz > 0) {
                    log.info("====================== SIZE KERANJANG " + sz);
                    for (int i = 0; i < sz; i++) {
                        JSONObject logData = new JSONObject();
                        MProduct mp = SpringInit.getMProductDao().barangById(Long.valueOf(tmp.get(i)[1].toString()));

                        logData.put("idproduk", mp.getId());
                        logData.put("namaproduk", mp.getNm_barang());
                        logData.put("type", mp.getType());
                        logData.put("harga", mp.getHarga().toString());
                        logData.put("jumlah", tmp.get(i)[2].toString());
                        logData.put("fotoproduk", SpringInit.getMRoutingDao().getConstantDB("img_url").getValue() + mp.getFoto_produk());

                        harga = mp.getHarga();
                        jumlah = Integer.parseInt(tmp.get(i)[2].toString());
                        data.put(logData);

                        sub = harga * jumlah;
                        total += sub;
                        logData.put("subtotal", "" + sub);
                    }

                    JSONObject jsonproduk = new JSONObject();
                    jsonproduk.put("produk", data);
                    jsonproduk.put("total", "" + total);

                    JSONObject json = new JSONObject();
                    json.put("produk", jsonproduk);

                    List<MNilaiJaminan> mnj = SpringInit.getMNilaiJaminanDao().viewNilaiJaminan();
                    if (mnj.size() > 0) {
                        log.info("====================== SIZE LIST " + mnj.size());

                        for (MNilaiJaminan datamnj : mnj) {
                            JSONObject jsons = new JSONObject();
                            jsons.put("id", datamnj.getId_nilai_jaminan());
                            jsons.put("value", datamnj.getNm_jaminan());

                            array.put(jsons);
                        }
                        json.put("jenisjaminan", array);
                    }

                    log.info("==============================================================================================");
                    MJenisPinjaman mjpinjam = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(mjp.getId());
                    JSONObject logDetSimulasi = new JSONObject();
                    logDetSimulasi.put("Plafon", kursIndonesia.format(total).toString());
                    logDetSimulasi.put("Jangka Waktu", mjp.getLama_angsuran());

                    Integer lamaangsuran = mjp.getLama_angsuran();
                    Double waktu = Double.valueOf(lamaangsuran) / 12;
                    Double wkt = Math.ceil(waktu);
                    log.info("====== HASIL WKT " + wkt);
                    Integer k = Integer.parseInt(df.format(wkt.intValue()));

                    MRateAsuransi mra = SpringInit.getMPresentasePlafonDao().getRateAsuransi(k);
                    logDetSimulasi.put("Rate Asuransi", mra.getPersentase_rate().toString() + "%");

                    Double premi = total * mra.getPersentase_rate() / 100;
                    logDetSimulasi.put("Premi Asuransi", kursIndonesia.format(Integer.parseInt(df.format(premi))));
                    log.info("====== PREMI ASURANSI " + premi);
                    logDetSimulasi.put("Biaya Polis", kursIndonesia.format(mjpinjam.getBiaya_polis()).toString());

                    MCustomCoa b_materai = SpringInit.getMPresentasePlafonDao().getBiayaMaterai();
                    logDetSimulasi.put("Biaya Materai", kursIndonesia.format(b_materai.getValue_awal()).toString());

                    Integer jlh_biaya = Integer.parseInt(df.format(premi)) + mjpinjam.getBiaya_polis() + b_materai.getValue_awal();
                    logDetSimulasi.put("Jumlah Biaya", kursIndonesia.format(jlh_biaya).toString());
                    log.info("===== JUMLAH BIAYA " + jlh_biaya);

                    Integer b_plafon = mjpinjam.getPlafon() * 45 / 100;
                    Integer b_adm = total * 5 / 1000;
                    log.info("===== BIAYA ADM " + b_adm);
                    Integer biaya_adm = 0;
                    if (b_adm < 25000) {
                        biaya_adm = 25000;
                    } else {
                        biaya_adm = b_adm;
                    }

                    logDetSimulasi.put("Biaya Administrasi", kursIndonesia.format(biaya_adm).toString());
                    logDetSimulasi.put("Pinjaman Diterima", kursIndonesia.format(total - (jlh_biaya + biaya_adm)).toString());
                    log.info("===== PINJAMAN DITERIMA " + (total - (jlh_biaya + biaya_adm)));

                    log.info("==============================================================================================");

                    JSONObject logAnggota = new JSONObject();
                    logAnggota.put("nama", nsbh.getNama());
                    logAnggota.put("status", nsbh.getStatus_pegawai());

                    if (nsbh.getGaji_pokok() == null) {
                        logAnggota.put("penghasilan", "0");
                    } else {
                        logAnggota.put("penghasilan", nsbh.getGaji_pokok().toString());
                    }

                    MPresentasePlafon mpp = SpringInit.getMPresentasePlafonDao().getPresentasePlafon();
                    logAnggota.put("plafon", mpp.getPresentase_plafon() + "%");

                    int pc;
                    if (nsbh.getGaji_pokok() == null) {
                        pc = 0 * mpp.getPresentase_plafon() / 100;
                    } else {
                        pc = nsbh.getGaji_pokok() * mpp.getPresentase_plafon() / 100;
                    }
                    logAnggota.put("plafon_cicilan", "" + pc);
                    logAnggota.put("jenis", mjp.getId().toString());

                    json.put("anggota", logAnggota);

                    log.info("==============================================================================================");

                    List<MTanggunganPlafon> mtp = SpringInit.getmNasabahDao().tanggunganByIdAnggota(Integer.parseInt(nsbh.getId().toString()));
                    if (mtp.size() > 0) {
                        log.info("====================== SIZE LIST " + mtp.size());

                        for (MTanggunganPlafon datamtp : mtp) {
                            JSONObject jsons = new JSONObject();
                            jsons.put("id_plafon", datamtp.getId_plafon());
                            jsons.put("nominal_tanggungan", datamtp.getNominal_tanggungan().toString());
                            jsons.put("keterangan", datamtp.getKeterangan());
                            jsons.put("status", datamtp.getStatus());

                            arrays2.put(jsons);
                        }
                    } else {
                        JSONObject jsons = new JSONObject();
                        jsons.put("message", "Data Tanggungan Tidak Ada");
                    }

                    log.info("==============================================================================================");

                    String status = nsbh.getStatus_pegawai();
                    Double lama_angsuran = lamaangsuran.doubleValue();
//                if(status.equals("Pegawai Tetap")){
//                    lama_angsuran = lamaangsuran.doubleValue();
//                }else{
//                    lama_angsuran = 36;
//                }

                    double pokok_awal = total;
                    double jasa = 0.15;
                    double month = 12;
                    double hasil = 0;
                    double a, b, c, d, e, angsuran_jasa, pokok;

                    DecimalFormat df = new DecimalFormat("#");

                    a = pokok_awal * (jasa / month);
                    b = 1 + jasa / month;
                    c = Math.pow(b, lama_angsuran);
                    d = 1 / c;
                    e = 1 - d;

                    JSONArray logSimulasis = new JSONArray();
                    for (int i = 0; i < lama_angsuran; i++) {
                        hasil = a / e;
                        angsuran_jasa = pokok_awal * (jasa / month);
                        pokok = hasil - angsuran_jasa;
                        pokok_awal = pokok_awal - pokok;
                        int x = i + 1;

                        JSONObject logSimulasi = new JSONObject();
                        logSimulasi.put("angsuran", x);
                        logSimulasi.put("total", df.format(hasil));
                        logSimulasi.put("pokok", df.format(pokok));
                        logSimulasi.put("jasa", df.format(angsuran_jasa));

                        if (pokok_awal > 0) {
                            logSimulasi.put("sisa_angsuran", df.format(Math.round(pokok_awal)));
                        } else {
                            logSimulasi.put("sisa_angsuran", "0");
                        }
                        logSimulasis.put(logSimulasi);
                    }

                    JSONArray array_ = new JSONArray();
                    JSONArray arrays = new JSONArray();

                    array_.put(logDetSimulasi);
                    for (int i = 0; i < array_.length(); i++) {
                        JSONObject jsons = array_.getJSONObject(i);
                        JSONArray array2 = jsons.names();
                        for (int j = i; j < array2.length(); j++) {
                            JSONObject logs = new JSONObject();
                            logs.put("item", array2.getString(j));
                            logs.put("value", jsons.get(array2.getString(j)));
                            arrays.put(logs);

                        }
                    }

                    JSONObject json2 = new JSONObject();
                    json.put("simulasi", logSimulasis);
                    json.put("detail_simulasi", arrays);
                    json.put("data_tanggungan", arrays2);
                    json2.put("data", json);
                    log.info("=============================== LOG DATA :" + json2);
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
                }
            } else {
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }

}
