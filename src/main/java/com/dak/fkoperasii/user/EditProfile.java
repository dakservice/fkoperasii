/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.user;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class EditProfile implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();

    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {

            String id_anggota = jsonReq.getString("id_anggota");
            String nama = jsonReq.getString("nama");
            String jk = jsonReq.getString("jenis_kelamin");
            String alamat = jsonReq.getString("alamat");
            String telp = jsonReq.getString("telp");
            String rek_simpeda = jsonReq.getString("rekening_simpeda");
            String tempat_lahir = jsonReq.getString("tempat_lahir");
            String id_agama = jsonReq.getString("agama");
            String pekerjaan = jsonReq.getString("pekerjaan");
            String nik = jsonReq.getString("nik");
            String nip = jsonReq.getString("nip");
            String email = jsonReq.getString("email");
            String tgl_lahir = jsonReq.getString("tgl_lahir");
            String status = jsonReq.getString("status");
            String kota = jsonReq.getString("kota");
            String departement = jsonReq.getString("departement");

            MNasabah mnsbh = SpringInit.getmNasabahDao().userById(Long.parseLong(id_anggota));
            MNasabah nsbh = new MNasabah();
            nsbh.setId(Long.valueOf(id_anggota));
            nsbh.setNama(nama);
            nsbh.setIdentitas(mnsbh.getIdentitas());
            nsbh.setJk(jk);
            nsbh.setTmp_lahir(tempat_lahir);
            nsbh.setTgl_lahir(tgl_lahir);
            nsbh.setStatus(status);
            nsbh.setId_agama(Integer.parseInt(id_agama));
            nsbh.setDepartement(departement);
            nsbh.setPekerjaan(pekerjaan);
            nsbh.setKota(kota);
            nsbh.setJabatan_id(mnsbh.getJabatan_id());
            nsbh.setAktif(mnsbh.getAktif());
            nsbh.setAlamat(alamat);
            nsbh.setNotelp(telp);
            nsbh.setStatus_pegawai(mnsbh.getStatus_pegawai());

            if (mnsbh.getGaji_pokok() == null) {
                nsbh.setGaji_pokok(0);
            } else {
                nsbh.setGaji_pokok(mnsbh.getGaji_pokok());
            }

            nsbh.setEmail(email);
            if (mnsbh.getFile_pic() == null) {
                nsbh.setFile_pic("");
            } else {
                nsbh.setFile_pic(mnsbh.getFile_pic());
            }

            nsbh.setId_cabang(mnsbh.getId_cabang());
            nsbh.setId_status_anggota(mnsbh.getId_status_anggota());
            nsbh.setNip(nip);
            nsbh.setNik(nik);
            nsbh.setPass_word(mnsbh.getPass_word());
            nsbh.setTgl_daftar(mnsbh.getTgl_daftar());
            nsbh.setTgl_berhenti(nsbh.getTgl_berhenti());
            nsbh.setRek_simpeda(rek_simpeda);
            nsbh.setLengkap("1");

            SpringInit.getmNasabahDao().updateNasabah(nsbh);
            //                mlogmobile id user
            mACtivity.setIdUser(nsbh.getId());
            ctx.put(Constant.WS.REQUEST, mACtivity);

            rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable context) {
    }

}
