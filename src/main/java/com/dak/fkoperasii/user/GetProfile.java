/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.user;

import com.dak.fkoperasii.entity.MAgama;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class GetProfile implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            String notelp = jsonReq.getString("notelp");
            log.info("========================= NO TELEPON " + notelp);

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            if (nsbh != null) {
                if (nsbh.getAktif().equals("Y")) {

                    JSONObject logData = new JSONObject();
                    logData.put("id_anggota", "AG00" + nsbh.getId());
                    logData.put("nama", nsbh.getNama());
                    String jk = nsbh.getJk();
                    String jenis_kelamin = "";
                    if (jk != null) {
                        if (jk.toLowerCase().equals("l")) {
                            jenis_kelamin = "Laki-Laki";
                        } else {
                            jenis_kelamin = "Perempuan";
                        }
                    }
                    logData.put("jenis_kelamin", jenis_kelamin);
                    logData.put("alamat", nsbh.getAlamat());
                    logData.put("telp", nsbh.getNotelp());
                    logData.put("departement", nsbh.getDepartement());

                    MAgama a = SpringInit.getMProfileDao().agamaById(nsbh.getId_agama());
                    if (nsbh.getId_agama() != null) {
                        if (nsbh.getId_agama() == 0) {
                            logData.put("agama", "Tidak Diketahui");
                        } else {
                            logData.put("agama", a.getNm_agama());
                        }
                    }
                    logData.put("rekening_simpeda", nsbh.getRek_simpeda());
                    logData.put("kota", nsbh.getKota());
                    logData.put("nik", nsbh.getNik());
                    logData.put("nip", nsbh.getNip());
                    logData.put("tempat_lahir", nsbh.getTmp_lahir());
                    logData.put("tgl_lahir", nsbh.getTgl_lahir());
                    logData.put("pekerjaan", nsbh.getPekerjaan());
                    logData.put("email", nsbh.getEmail());
                    logData.put("status", nsbh.getStatus());

                    if (nsbh.getFile_pic() == null || nsbh.getFile_pic().equals("")) {
                        logData.put("file_pic", "");
                    } else {
                        logData.put("file_pic", "https://koperasi.dak.co.id/uploads/anggota/" + nsbh.getFile_pic());
                    }

                    JSONObject data = new JSONObject();
                    data.put("data", logData);
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), data);
                } else {
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }

            } else {
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

}
