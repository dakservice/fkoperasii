/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.user;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.Date;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class ChangePasswordConf implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY); 
        
        
        try {
            String send_to = jsonReq.getString("send_to");
            
            JSONObject json = new JSONObject();
            json.put("send_to", send_to);
            json.put("username", "dakfkoperasi@gmail.com");
            json.put("password", "fkoperasi123");
            json.put("subject", "KOPERASI RESET PASSWORD");
            json.put("body_text", "KOPERASI RESET PASSWORD");
            
            String UrltoEmail = "192.168.100.19:9990/emailsender/sendemail";
            
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(UrltoEmail);

            StringEntity strPostData = new StringEntity(json.toString(), "UTF-8");
            post.setHeader("Content-Type", "application/json");
            post.setHeader("incoming", "koperasi");
            post.setHeader("apikey", "K0P3R451xD1G1T4L4M0R3KR1Y4N3S14");
            post.setEntity(strPostData);

            HttpResponse response = client.execute(post);
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        } catch (Exception e) {
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
