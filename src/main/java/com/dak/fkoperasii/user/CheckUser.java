/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.user;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class CheckUser implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
    JSONObject data = new JSONObject();
    MRc rc = new MRc();

    @Override
    public int prepare(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject bodyData = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            String notelp = bodyData.getString("notelp");
//            MPartnerCore partnerCore = (MPartnerCore) ctx.get(Constant.WS.PARTNER_CORE);
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            if (nsbh != null) {
                nsbh.getId();

                if (nsbh.getStatus().equals(Constant.LOGIN.STATUS_USER.ACTIVE)) {
                    ctx.put(Constant.TRX.USER_DETAIL, nsbh);

                    return PREPARED | NO_JOIN;
                } else {

                    switch (nsbh.getStatus()) {
                        case Constant.LOGIN.STATUS_USER.INACTIVE:
                            //                mlogmobile id user
                            mACtivity.setIdUser(nsbh.getId());
                            ctx.put(Constant.WS.REQUEST, mACtivity);

                            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.USER_AUTH_FAILED);
                            data.put("nama", nsbh.getNama());
                            data.put("status_user", Constant.LOGIN.USER_DESC_STATUS.INACTIVE);
                            bodyData.put("data", data);
                            wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);
                            break;
                        case Constant.LOGIN.STATUS_USER.BLOCKED:
                            //                mlogmobile id user
                            mACtivity.setIdUser(nsbh.getId());
                            ctx.put(Constant.WS.REQUEST, mACtivity);

                            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.USER_AUTH_FAILED);

                            data.put("nama", nsbh.getNama());
                            data.put("status_user", Constant.LOGIN.USER_DESC_STATUS.BLOCKED);
                            bodyData.put("data", data);
                            wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);
                            break;
                        case Constant.LOGIN.STATUS_USER.DELETED:
                            //                mlogmobile id user
                            mACtivity.setIdUser(nsbh.getId());
                            ctx.put(Constant.WS.REQUEST, mACtivity);

                            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.USER_AUTH_FAILED);

                            data.put("nama", nsbh.getNama());
                            data.put("status_user", Constant.LOGIN.USER_DESC_STATUS.DELETED);
                            bodyData.put("data", data);
                            wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);
                            break;
                        default:
                            //                mlogmobile id user
                            mACtivity.setIdUser(nsbh.getId());
                            ctx.put(Constant.WS.REQUEST, mACtivity);

                            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.USER_AUTH_FAILED);
                            data.put("nama", nsbh.getNama());
                            data.put("status_user", Constant.LOGIN.USER_DESC_STATUS.INACTIVE);
                            bodyData.put("data", data);
                            wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);
                    }

                    ctx.put(Constant.WS.RESPONSE, wsResponse);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
                    return ABORTED | NO_JOIN;
                }
            } else {
                rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.USER_NOT_FOUND);
                wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);

                ctx.put(Constant.WS.RESPONSE, wsResponse);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
                return ABORTED | NO_JOIN;
            }
        } catch (Exception e) {
            wsResponse = new ResponseWebServiceContainer();

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);

            ctx.put(Constant.WS.RESPONSE, wsResponse);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
            return ABORTED | NO_JOIN;

        }

    }

    @Override
    public void commit(long id, Serializable context) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long id, Serializable context) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
