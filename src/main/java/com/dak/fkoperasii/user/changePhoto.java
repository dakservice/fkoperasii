/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.user;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Morten Jonathan
 */
public class changePhoto implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    static ChannelSftp channelSftp = null;
    static Session session = null;
    static Channel channel = null;
    static String PATHSEPARATOR = "/";

    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {

            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            MultipartFile[] file_images = (MultipartFile[]) ctx.get(Constant.IMAGE);

            //==== UPLOAD FOTO MULTIPART BEDA SERVER ==================================================================================                    
            if (file_images.length > 0) {

                int i = 1;
                String path = "/opt/fkoperasii/webapps/cms/anggota/";
                String SFTPHOST = "139.99.33.151";
                int SFTPPORT = 7879;
                String SFTPUSER = "koperasidakco";
                String SFTPPASS = "Qawsed#1477";
                String SFTPWORKINGDIR = "/home/koperasidakco/public_html/uploads/anggota";

                JSch jsch = new JSch();
                for (MultipartFile filed : file_images) {
                    String filename = filed.getOriginalFilename();
                    String LOCALDIRECTORY = path + filename;
                    File convFile = new File(LOCALDIRECTORY);
                    convFile.createNewFile();
                    FileOutputStream stream = new FileOutputStream(convFile);
                    stream.write(filed.getBytes());
                    stream.flush();
                    stream.close();

                    try {

                        session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
                        log.info("LEWATTTTTTTTTTTTTTTTT1");

                        session.setPassword(SFTPPASS);
                        log.info("LEWATTTTTTTTTTTTTTTTT2");

                        java.util.Properties config = new java.util.Properties();
                        log.info("LEWATTTTTTTTTTTTTTTTT3");

                        config.put("StrictHostKeyChecking", "no");
                        log.info("LEWATTTTTTTTTTTTTTTTT4");

                        session.setConfig(config);
                        log.info("LEWATTTTTTTTTTTTTTTTT5");

                        session.connect(); // Create SFTP Session
                        log.info("LEWATTTTTTTTTTTTTTTTT6");

                        channel = session.openChannel("sftp"); // Open SFTP Channel
                        log.info("LEWATTTTTTTTTTTTTTTTT7");

                        channel.connect();
                        log.info("LEWATTTTTTTTTTTTTTTTT8");

                        channelSftp = (ChannelSftp) channel;
                        log.info("LEWATTTTTTTTTTTTTTTTT9");

                        channelSftp.cd(SFTPWORKINGDIR); // Change Directory on SFTP Server
                        log.info("LEWATTTTTTTTTTTTTTTTT10");

                        recursiveUpload(LOCALDIRECTORY, SFTPWORKINGDIR);
                        log.info("LEWATTTTTTTTTTTTTTTTT11");

                        channelSftp.chown(1053, SFTPWORKINGDIR + filename);
                        log.info("LEWATTTTTTTTTTTTTTTTT12");

                    } catch (Exception ex) {
                        log.info("MASUK CATCH GAGAL");
                        ex.printStackTrace();
                    } finally {
                        if (channelSftp != null) {
                            channelSftp.disconnect();
                        }
                        if (channel != null) {
                            channel.disconnect();
                        }
                        if (session != null) {
                            session.disconnect();
                        }

                    }

                    log.info("===== FILENAME " + filename);

                    SpringInit.getmNasabahDao().updateFoto(nsbh.getId().intValue(), filename);
                }
            }
//==== UPLOAD FOTO MULTIPART BEDA SERVER ==================================================================================

            //                mlogmobile id user
            mACtivity.setIdUser(nsbh.getId());
            ctx.put(Constant.WS.REQUEST, mACtivity);

            rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());

        } catch (Exception e) {

            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);

        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

    }

    @Override
    public void abort(long id, Serializable context) {
    }

    private static void recursiveUpload(String sourcePath, String destinationPath) throws SftpException, FileNotFoundException {

        File sourceFile = new File(sourcePath);
        if (sourceFile.isFile()) {

            channelSftp.cd(destinationPath);
            if (!sourceFile.getName().startsWith(".")) {
                channelSftp.put(new FileInputStream(sourceFile), sourceFile.getName(), ChannelSftp.OVERWRITE);
            }

        } else {

            System.out.println("inside else " + sourceFile.getName());
            File[] files = sourceFile.listFiles();

            if (files != null && !sourceFile.getName().startsWith(".")) {

                channelSftp.cd(destinationPath);
                SftpATTRS attrs = null;

                try {
                    attrs = channelSftp.stat(destinationPath + "/" + sourceFile.getName());
                } catch (Exception e) {
                    System.out.println(destinationPath + "/" + sourceFile.getName() + " not found");
                }

                // else create a directory
                if (attrs != null) {
                    System.out.println("Directory exists IsDir=" + attrs.isDir());
                } else {
                    System.out.println("Creating dir " + sourceFile.getName());
                    channelSftp.mkdir(sourceFile.getName());
                }

                for (File f : files) {
                    recursiveUpload(f.getAbsolutePath(), destinationPath + "/" + sourceFile.getName());
                }

            }
        }

    }
}
