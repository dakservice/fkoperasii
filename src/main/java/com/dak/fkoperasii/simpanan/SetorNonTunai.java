/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.simpanan;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MSetoranNonTunai;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.tx.participant.UploadManager;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Morten Jonathan
 */
public class SetorNonTunai implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    static ChannelSftp channelSftp = null;
    static Session session = null;
    static Channel channel = null;
    static String PATHSEPARATOR = "/";

    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd");

        String notelp = jsonReq.getString("notelp");
        MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
        String jenis_simpanan = jsonReq.getString("jenis_simpanan");
        String jumlah_setoran = jsonReq.getString("jumlah_setoran");
        MultipartFile[] file_images = (MultipartFile[]) ctx.get(Constant.IMAGE);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            final JSONObject responseJsonUpload = UploadManager.uploadToProduction("setoran", file_images);
            if (!responseJsonUpload.getString("message").equals("00")) {
                log.error("+++++++++++++++++++++++" + responseJsonUpload.toString());
                //                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);
                rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
            } else {
                MSetoranNonTunai msn = new MSetoranNonTunai();
                msn.setUser_id(Integer.parseInt(nsbh.getId().toString()));
                msn.setTanggal_trx(datetime.format(new Date()));
                log.info("======== TANGGAL " + datetime.format(new Date()));
                msn.setId_simpanan(Integer.parseInt(jenis_simpanan));
                msn.setNominal(Integer.parseInt(jumlah_setoran));
                msn.setBukti_transfer(responseJsonUpload.getJSONArray("data").get(0).toString());
                msn.setStatus(0);
                SpringInit.getMSetorTarikDao().saveOrUpdate(msn);
                //                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);
                rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace();
            log.error("+++++++++++++++++++++++" + stringWriter.toString());
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable context) {
    }

}
