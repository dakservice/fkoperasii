/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.simpanan;

import com.dak.fkoperasii.entity.*;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.Function;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.dak.fkoperasii.utility.Utility;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class TokenSetoran implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    Calendar cal = Calendar.getInstance();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        DateTimeFormatter datetime = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter time = DateTimeFormat.forPattern("yyyy-MM-dd  HH:mm:ss");
        DateTime date = new DateTime();
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            final String token = Utility.generateToken(6);
            log.info("=========== EXPIRE  :" + date.plusMinutes(30).toString(time));
            Integer values = 0;
            final String curDate = date.toString(datetime);
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            JSONArray logSetoran = jsonReq.getJSONArray("setoran");
            int lengthS = logSetoran.length();
            if (lengthS > 0) {

                JSONArray jarr = new JSONArray();

                MTokenSetorTarik mtarik = new MTokenSetorTarik();
                mtarik.setToken_number(token);
                mtarik.setExpired_date(date.plusMinutes(30).toString(time));
                mtarik.setAction("SETORAN");
                mtarik.setStatus("0");
                mtarik.setId_user(nsbh.getId().toString());
                mtarik.setTanggal_trx(curDate);
                mtarik.setDate_created(Function.yyyyMMddHHmmssToDate(new Date()).toString());

                SpringInit.getMSetorTarikDao().saveOrUpdate(mtarik);

                for (int a = 0; a < lengthS; a++) {
                    JSONObject jso = new JSONObject();
                    JSONObject setorans = logSetoran.getJSONObject(a);
                    String id_simpanan = setorans.getString("id_simpanan");
                    String value = setorans.getString("value");

                    MTokenSetorTarikDetail mtarikdetail = new MTokenSetorTarikDetail();
                    mtarikdetail.setToken_id(mtarik.getId());
                    mtarikdetail.setId_simpanan(Integer.parseInt(id_simpanan));
                    mtarikdetail.setNominal(Integer.parseInt(value));

                    SpringInit.getMSetorTarikDao().saveOrUpdate(mtarikdetail);

                    values += Integer.parseInt(value);
                }
                JSONObject js = new JSONObject();
                js.put("total_pembayaran", values.toString());
                js.put("token", token);
                js.put("expire_date", date.plusMinutes(30).toString(time));

                JSONObject json = new JSONObject();
                json.put("data", js);
                //                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);
                final MSetting setting = SpringInit.getmSettingDao().getSettingByParam("token_wa");
                final String waTemplate = setting.getValue().replace("#type","Setoran").replace("#token",token);
                Function.sendWA(nsbh.getNotelp(),waTemplate);

                rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json);

            }
        } catch (Exception e) {
            e.printStackTrace();
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
