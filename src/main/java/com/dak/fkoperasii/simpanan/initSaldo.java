/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.simpanan;

import com.dak.fkoperasii.entity.MJenisSimpan;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Morten Jonathan
 */
public class initSaldo implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        DecimalFormat df = new DecimalFormat("#");
        JSONArray arrays = new JSONArray();
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            String notelp = jsonReq.getString("notelp");

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);

            if (nsbh == null) {
                log.info("====================== DATA" + nsbh);
                //                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(null);
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            } else {

                List<MJenisSimpan> mjss = SpringInit.getMJenisSimpanDao().getJenisSimpanan();
//                Double saldo = SpringInit.getmNasabahDao().getSaldoByIdSimpanan("1365","32");

                for (MJenisSimpan datams : mjss) {
                    JSONObject logData = new JSONObject();
                    log.info(datams.getId());
                    logData.put("id", datams.getId());
                    logData.put("jenis_tabungan", datams.getJns_simpan());
                    logData.put("min_ngendap",0);
                    if(datams.getJns_simpan().equalsIgnoreCase("simpanan sukarela")){
                        final String year = String.valueOf(DateTime.now( DateTimeZone.forID( "Asia/Jakarta" ) ).getYear());
                        final int month = DateTime.now( DateTimeZone.forID( "Asia/Jakarta" ) ).getMonthOfYear();
                        System.out.println("asd year : "+year+"| month : "+month+"");
                        final int minSaldo = Integer.parseInt(SpringInit.getMJenisSimpanDao().getMinSaldoSukarela(year,month).toString());
                        logData.put("min_ngendap",minSaldo);
                    }
                    System.out.println("param-1-----------"+nsbh.getId().toString());
                    System.out.println("param-2------------"+datams.getId().toString());
                    Integer saldo = SpringInit.getmNasabahDao().getSaldoByIdSimpanan(nsbh.getId().toString(),datams.getId().toString());
//                    Double saldo = SpringInit.getmNasabahDao().getSaldoByIdSimpanan(nsbh.getId().toString(), datams.getId().toString());
                    if (saldo > 0) {
                        logData.put("saldo", df.format(saldo).toString());
                    } else {
                        logData.put("saldo", "0");
                    }


                    arrays.put(logData);
                }
//                String id_simpanan = jsonReq.getString("id_simpanan");
//                Double saldo = SpringInit.getmNasabahDao().getSaldoByIdSimpanan(nsbh.getId().toString(), id_simpanan);
                JSONObject json = new JSONObject();
                json.put("jenis_simpanan", arrays);
//                json.put("saldo", df.format(saldo).toString());

                JSONObject json2 = new JSONObject();
                json2.put("data", json);
                //                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);

            }

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

}
