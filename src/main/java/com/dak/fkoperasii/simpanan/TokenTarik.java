/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.simpanan;

import com.dak.fkoperasii.entity.*;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.Function;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.dak.fkoperasii.utility.Utility;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class TokenTarik implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    //Calendar cal = Calendar.getInstance();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        DateTimeFormatter datetime = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter time = DateTimeFormat.forPattern("yyyy-MM-dd  HH:mm:ss");
        DateTime date = new DateTime();
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);
        try {
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            Integer id_simpanan = jsonReq.getInt("id_simpanan");
            Integer nominal = jsonReq.getInt("nominal");
            System.out.println("asd masuk 1");
            String jenisSimpanan = SpringInit.getMJenisSimpanDao().getJenisSimpnanById(id_simpanan);
            System.out.println("asd masuk 2");
            if(jenisSimpanan.equalsIgnoreCase("simpanan sukarela")) {
                System.out.println("asd masuk 3");
                final String year = String.valueOf(DateTime.now(DateTimeZone.forID("Asia/Jakarta")).getYear());
                final int month = DateTime.now(DateTimeZone.forID("Asia/Jakarta")).getMonthOfYear();
                final int minSaldo = Integer.parseInt(SpringInit.getMJenisSimpanDao().getMinSaldoSukarela(year, month).toString());
                Integer saldo = SpringInit.getmNasabahDao().getSaldoByIdSimpanan(nsbh.getId().toString(),id_simpanan.toString());
                System.out.println("min saldo "+minSaldo+" | saldo "+saldo);
                if(saldo-nominal<minSaldo){
                    dr = new ResponseWebServiceContainer("02", "Minimal saldo ngendap "+ Function.toRupiah(minSaldo));
                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
                    return PREPARED|NO_JOIN;
                }
//
            }




            String token = Utility.generateToken(6);
            log.info("=========curenTime : " + date);
            final String curDate = date.toString(datetime);
            log.info("========= ExpiredTime : " + date.plusMinutes(30));
            if (nsbh != null) {
                if (nsbh.getAktif().equals("Y")) {
                    MTokenSetorTarik mtarik = new MTokenSetorTarik();
                    mtarik.setToken_number(token);
                    mtarik.setExpired_date(date.plusMinutes(30).toString(time));
                    mtarik.setAction("PENARIKAN");
                    mtarik.setStatus("0");
                    mtarik.setId_user(nsbh.getId().toString());
                    mtarik.setTanggal_trx(curDate);

                    SpringInit.getMSetorTarikDao().saveOrUpdate(mtarik);

                    MTokenSetorTarikDetail mtarikdetail = new MTokenSetorTarikDetail();
                    mtarikdetail.setToken_id(mtarik.getId());
                    mtarikdetail.setId_simpanan(id_simpanan);
                    mtarikdetail.setNominal(nominal);

                    SpringInit.getMSetorTarikDao().saveOrUpdate(mtarikdetail);

                    JSONObject js = new JSONObject();
                    js.put("total_pembayaran", nominal.toString());
                    js.put("token", token);
                    js.put("expire_date", date.plusMinutes(30).toString(time));

                    JSONObject json = new JSONObject();
                    json.put("data", js);
                    //                mlogmobile id user


                    //send token to WA
                    final MSetting setting = SpringInit.getmSettingDao().getSettingByParam("token_wa");
                    final String waTemplate = setting.getValue().replace("#type","Penarikan").replace("#token",token);
                    Function.sendWA(nsbh.getNotelp(),waTemplate);

                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json);

                } else {
                    //                mlogmobile id user
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            } else {
                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

}
