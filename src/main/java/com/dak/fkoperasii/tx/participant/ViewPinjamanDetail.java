/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MAddendum;
import com.dak.fkoperasii.entity.MDPinjaman;
import com.dak.fkoperasii.entity.MDetailPinjaman;
import com.dak.fkoperasii.entity.MHeadPinjaman;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class ViewPinjamanDetail implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    DecimalFormat df = new DecimalFormat("#");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        JSONArray array = new JSONArray();
        Calendar cal = Calendar.getInstance();
        Date tgl_tempo;
        JSONObject json = null;
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            String notelp = jsonReq.getString("notelp");
            log.info("========================= NO TELEPON " + notelp);

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);

            if (nsbh != null) {
                Long anggota_id = Long.parseLong(nsbh.getId().toString());
                log.info("====================== ANGGOTA ID " + anggota_id);
                String id_pinjam = jsonReq.getString("idpinjam");

                MHeadPinjaman mhp = SpringInit.getMHeadPinjamanDao().pinjamanByAjuanId(Integer.parseInt(id_pinjam));
//                JSONArray data = new JSONArray();
                int total = mhp.getJumlah();

                int lama_angsuran = mhp.getLama_angsuran();

                List<MDPinjaman> mdp_sz = SpringInit.getMDPinjamanDao().getDetailPinjamanById(Integer.parseInt(id_pinjam));
                log.info("================= JUMLAH DETAIL PINJAMAN " + mdp_sz.size());
                log.info("================= JUMLAH LAMA ANGSURAN " + lama_angsuran);

                if (mdp_sz.size() != lama_angsuran) {

                    double pokok_awal = total;
                    double jasas = mhp.getBunga();
                    log.info("==================== BUNGAS " + df.format(jasas));
                    double jasa = jasas / 100;
                    log.info("==================== BUNGA " + df.format(jasa));
                    double month = 12;
                    double hasil = 0;
                    double a, b, c, d, e, angsuran_jasa, pokok;
                    int h = 0;

                    DecimalFormat df = new DecimalFormat("#");

                    a = pokok_awal * (jasa / month);
                    b = 1 + jasa / month;
                    c = Math.pow(b, lama_angsuran);
                    d = 1 / c;
                    e = 1 - d;

                    JSONArray logSimulasis = new JSONArray();
                    int lama_ags = lama_angsuran + 1;
                    for (int i = 1; i <= lama_angsuran; i++) {
                        hasil = a / e;
                        angsuran_jasa = pokok_awal * (jasa / month);
                        pokok = hasil - angsuran_jasa;
                        pokok_awal = pokok_awal - pokok;
//                    int x = i-1;
                        if (i >= 1) {
                            JSONObject logSimulasi = new JSONObject();
                            MDPinjaman mdp = new MDPinjaman();
                            mdp.setAngsuran(i);
                            logSimulasi.put("angsuran", i);
                            mdp.setPinjam_id(Integer.parseInt(id_pinjam));
                            mdp.setAngsuran_jasa(Integer.parseInt(df.format(angsuran_jasa)));
                            logSimulasi.put("angsuranjasa", df.format(angsuran_jasa));
                            log.info("==================== BUNGA " + df.format(angsuran_jasa));
                            mdp.setAngsuran_pokok(Integer.parseInt(df.format(pokok)));
                            logSimulasi.put("angsuranpokok", df.format(pokok));
                            log.info("==================== POKOK " + df.format(pokok));
                            mdp.setBiaya_adm(Integer.parseInt(df.format(mhp.getBiaya_adm())));
                            logSimulasi.put("biayaadm", df.format(mhp.getBiaya_adm()));
                            log.info("==================== ADM " + df.format(mhp.getBiaya_adm()));
                            mdp.setBiaya_provisi(Integer.parseInt(df.format(mhp.getBiaya_provisi())));
                            logSimulasi.put("biayaprovisi", df.format(mhp.getBiaya_provisi()));
                            log.info("==================== PROVISI " + df.format(mhp.getBiaya_provisi()));
                            cal.add(Calendar.DAY_OF_MONTH, 30);
                            tgl_tempo = cal.getTime();
                            String tgl_pinjam = mhp.getTgl_pinjam().substring(8, 10);
                            mdp.setJatuh_tempo(sdf.format(tgl_tempo).toString().substring(0, 8) + tgl_pinjam);
                            logSimulasi.put("jatuhtempo", sdf.format(tgl_tempo).toString().substring(0, 8) + tgl_pinjam);
                            mdp.setJlh_angsuran(Integer.parseInt(df.format(hasil)));
                            logSimulasi.put("jlhangsuran", df.format(hasil));
                            log.info("==================== TOTAL " + df.format(hasil));
                            mdp.setStatus_bayar("Belum Bayar");
                            logSimulasi.put("statusbayar", "Belum Bayar");

//                    String status = null;
//                    List<MDetailPinjaman> detPinjaman = SpringInit.getMDetailPinjamanDao().getAllById(Integer.parseInt(id_pinjam));
//                    List<MAddendum> ma = SpringInit.getMAddendumDao().getAllById(Integer.parseInt(id_pinjam));
//                    
//                    
//                    
//                    log.info("============ SIZE "+mdp.size());
//                    log.info("============ SIZE ADENDUM"+ma.size());
//                        for(int z = i; z <= mdp.size(); z++){
//                    if(mdp.size() <= x){
//                        h = 1;
//                        if(ma.size() <= x){
//                           int k= ma.size()-h;
//                        log.info("=================== ANGSURAN KE "+Integer.parseInt(ma.get(k).getAngsuran_ke()));
//                        if(Integer.parseInt(ma.get(k).getAngsuran_ke()) == x){
//                            logSimulasi.put("sudahbayar", "addendum");
//                        }else if(mdp.size() == x){
//                            logSimulasi.put("sudahbayar", "true");
//                        }else{
//                            logSimulasi.put("sudahbayar", "false");
//                        }h++;
//                        } 
//                        
////                        }
////                    }else if(ma.size() >= x){
////                        String bulan = ma.get(x).getAngsuran_ke();
////                        log.info("========== BULAN "+bulan);
////                        if(x == Integer.parseInt(bulan)){
////                            logSimulasi.put("sudahbayar", "addendum");
////                        }
//                    }
//                    else{
//                        logSimulasi.put("sudahbayar", "Data Tidak Sesuai Dengan Lama Angsuran");
//                    }h--;
//                    if (pokok_awal > 0) {
//                        logSimulasi.put("sisa_angsuran", df.format(Math.round(pokok_awal)));
//                    } else{
//                        logSimulasi.put("sisa_angsuran", "0");
//                    }
//                    logSimulasis.put(logSimulasi);
//                    if(mdp_sz.size() <= lama_angsuran){
//                        SpringInit.getMDPinjamanDao().save(mdp);
//                    }
                            List<MDetailPinjaman> detPinjaman = SpringInit.getMDetailPinjamanDao().getAllById(Integer.parseInt(id_pinjam));
                            List<MAddendum> ma = SpringInit.getMAddendumDao().getAllById(Integer.parseInt(id_pinjam));
                            for (MDetailPinjaman datamdp : detPinjaman) {
                                log.info("===== MASUK LOOP 1");
                                if (datamdp.getAngsuran_ke() == i) {

                                    mdp.setStatus_bayar("Sudah Bayar");
                                    logSimulasi.put("statusbayar", "Sudah Bayar");
//                                log.info("======= I KE "+i);
//                                log.info("======= STATUS : Sudah Bayar");

                                }
//                            else{
//                                mdp.setStatus_bayar("Belum Bayar");
//                                logSimulasi.put("statusbayar", "Belum Bayar");
//                                log.info("======= I KE "+i);
//                                log.info("======= STATUS : Belum Bayar");
//                                
//                            }

                                for (MAddendum datama : ma) {
                                    log.info("===== MASUK LOOP 2");
                                    String bln_angsuran = datama.getAngsuran_ke();
                                    log.info("===== DATA GET ANGSURAN " + bln_angsuran);
                                    String[] getAngsuran = bln_angsuran.split(", ");

                                    int angs_size = getAngsuran.length;
                                    log.info("===== SIZE ANGSURAN " + angs_size);

                                    for (int y = 0; y < angs_size; y++) {
                                        log.info("===== BULAN ANGSURAN " + getAngsuran[y]);
                                        int xz = i - 1;
                                        if (Integer.parseInt(getAngsuran[y]) == xz) {
                                            log.info("===== YES ");
                                            mdp.setStatus_bayar("Addendum");
                                            logSimulasi.put("statusbayar", "Addendum");

                                            log.info("======= I KE " + xz);
                                            log.info("======= STATUS : Addendum");
                                        } else if (mdp.getStatus_bayar().equals("Sudah Bayar")) {
                                            mdp.setStatus_bayar("Sudah Bayar");
                                            logSimulasi.put("statusbayar", "Sudah Bayar");
                                            log.info("======= I KE " + xz);
                                            log.info("======= STATUS : Sudah Bayar");
                                        } else {
                                            mdp.setStatus_bayar("Belum Bayar");
                                            logSimulasi.put("statusbayar", "Belum Bayar");
                                            log.info("======= I KE " + i);
                                            log.info("======= STATUS : Belum Bayar");

                                        }

                                    }

//                            if(Integer.parseInt(datama.getAngsuran_ke()) == i){
//                            if(getAngsuran[y] == i){
//                                mdp.setStatus_bayar("Addendum");
//                                logSimulasi.put("statusbayar", "Addendum");
//                                
//                                log.info("======= I KE "+i);
//                                log.info("======= STATUS : Addendum");
//                            }else if(mdp.getStatus_bayar().equals("Sudah Bayar")){
//                                mdp.setStatus_bayar("Sudah Bayar");
//                                logSimulasi.put("statusbayar", "Sudah Bayar");
//                                log.info("======= I KE "+i);
//                                log.info("======= STATUS : Sudah Bayar");
//                            }else{
//                                mdp.setStatus_bayar("Belum Bayar");
//                                logSimulasi.put("statusbayar", "Belum Bayar");
//                                log.info("======= I KE "+i);
//                                log.info("======= STATUS : Belum Bayar");
//                                
//                            }
//                            else {
//                                mdp.setStatus_bayar("Belum Bayar");
//                                logSimulasi.put("statusbayar", "Belum Bayar");
//                            }
                                }
                            }
//                        List <MDPinjaman> mdp_sz = SpringInit.getMDPinjamanDao().getDetailPinjamanById(Integer.parseInt(id_pinjam));
//                        log.info("================= JUMLAH DETAIL PINJAMAN "+mdp_sz.size());
//                        if(mdp_sz.size() <= lama_angsuran-1){
                            SpringInit.getMDPinjamanDao().save(mdp);

                            logSimulasis.put(logSimulasi);
                        }
                    }

                    json = new JSONObject();
                    json.put("data", logSimulasis);

                    log.info("=============================== LOG DATA :" + json);
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json);
                }
//                else{
//                    
                List<MDPinjaman> lmdp = SpringInit.getMDPinjamanDao().getDetailPinjamanById(Integer.parseInt(id_pinjam));
//                log.info("============== SIZE LMDP "+lmdp.size());
//                List<MDetailPinjaman> detPinjaman = SpringInit.getMDetailPinjamanDao().getAllById(Integer.parseInt(id_pinjam));
//                    List<MAddendum> ma = SpringInit.getMAddendumDao().getAllById(Integer.parseInt(id_pinjam));
//                    JSONObject logs = new JSONObject();
//                    MDPinjaman mdp = new MDPinjaman();
//                    for (int i = 0; i < lmdp.size()-1; i++) {
//                        mdp.setAngsuran(lmdp.get(i).getAngsuran());
//                        mdp.setPinjam_id(lmdp.get(i).getPinjam_id());
//                        mdp.setAngsuran_jasa(lmdp.get(i).getAngsuran_jasa());
//                        mdp.setAngsuran_pokok(lmdp.get(i).getAngsuran_pokok());
//                        mdp.setBiaya_adm(lmdp.get(i).getBiaya_adm());
//                        mdp.setBiaya_provisi(lmdp.get(i).getBiaya_provisi());
//                        mdp.setJatuh_tempo(lmdp.get(i).getJatuh_tempo());
//                        mdp.setJlh_angsuran(lmdp.get(i).getJlh_angsuran());
//                    
//                    
//                    for (MDetailPinjaman datamdp : detPinjaman) {
//                            if(datamdp.getAngsuran_ke() == i+1){
//                                mdp.setStatus_bayar("Sudah Bayar");
//                                logs.put("statusbayar", "Sudah Bayar");
//                            }else{
//                                mdp.setStatus_bayar("Belum Bayar");
//                                logs.put("statusbayar", "Belum Bayar");
//                            }
//                            
//                        }
//                        
//                        for (MAddendum datama : ma) {
//                            if(Integer.parseInt(datama.getAngsuran_ke()) == i+1){
//                                mdp.setStatus_bayar("Addendum");
//                                logs.put("statusbayar", "Addendum");
//                            }else if(mdp.getStatus_bayar().equals("Sudah Bayar")){
//                                mdp.setStatus_bayar("Sudah Bayar");
//                                logs.put("statusbayar", "Sudah Bayar");
//                            }
//                            else {
//                                mdp.setStatus_bayar("Belum Bayar");
//                                logs.put("statusbayar", "Belum Bayar");
//                            }
//                        }
//                        
//                    }SpringInit.getMDPinjamanDao().update(mdp);
//                    

                JSONArray logSimulasis = new JSONArray();

                for (MDPinjaman mDPinjaman : lmdp) {
                    JSONObject logSimulasi = new JSONObject();
                    logSimulasi.put("angsuran", mDPinjaman.getAngsuran().toString());
                    logSimulasi.put("angsuranjasa", mDPinjaman.getAngsuran_jasa().toString());
                    logSimulasi.put("angsuranpokok", mDPinjaman.getAngsuran_pokok().toString());
                    logSimulasi.put("biayaadm", mDPinjaman.getBiaya_adm().toString());
                    logSimulasi.put("biayaprovisi", mDPinjaman.getBiaya_provisi().toString());
                    logSimulasi.put("jatuhtempo", mDPinjaman.getJatuh_tempo());
                    logSimulasi.put("jlhangsuran", mDPinjaman.getJlh_angsuran().toString());
                    logSimulasi.put("statusbayar", mDPinjaman.getStatus_bayar());
                    log.info("===== STATUS BAYAR " + mDPinjaman.getStatus_bayar());

                    logSimulasis.put(logSimulasi);
                }
                json = new JSONObject();
                json.put("data", logSimulasis);
                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json);
//            }
            } else {
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
