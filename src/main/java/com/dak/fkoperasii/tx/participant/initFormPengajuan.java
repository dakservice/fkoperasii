/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MJenisAngsuran;
import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MNilaiJaminan;
import com.dak.fkoperasii.entity.MPresentasePlafon;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MTanggunganPlafon;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class initFormPengajuan implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;

    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        String notelp = jsonReq.getString("notelp");
        String status = "";
        MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
        JSONArray datas = new JSONArray();
        JSONArray array = new JSONArray();
        JSONArray arrays = new JSONArray();
        JSONArray arrays_ = new JSONArray();
        JSONArray arrays2 = new JSONArray();
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            log.info("============================== DATA NASABAH " + nsbh);

            if (nsbh != null) {
                if (nsbh.getStatus_pegawai().equals("Pegawai Tetap")) {
                    status = "Pegawai Tetap";
                } else if (nsbh.getStatus_pegawai().equals("Pegawai Tidak Tetap")) {
                    status = "Pegawai Tidak Tetap";
                }

                log.info("====================== STATUS ANGGOTA " + status);
                List<MJenisPinjaman> mj = SpringInit.getMJenisPinjamanDao().jenisPinjamanByStatus(status);
                if (mj.size() > 0) {
                    log.info("====================== SIZE LIST " + mj.size());

                    for (MJenisPinjaman datamj : mj) {
                        JSONObject json = new JSONObject();
                        json.put("id", datamj.getId());
                        json.put("value", datamj.getNama_pinjaman());
                        json.put("max", datamj.getLama_angsuran().toString());

                        array.put(json);
                    }

                    List<MJenisAngsuran> mja = SpringInit.getMJenisAngsuranDao().viewJenisAngsuran();
                    if (mja.size() > 0) {
                        log.info("====================== SIZE LIST " + mj.size());

                        for (MJenisAngsuran datamja : mja) {
                            JSONObject jsons = new JSONObject();
                            jsons.put("id", datamja.getId());
                            jsons.put("value", datamja.getKet());

                            arrays.put(jsons);
                        }

                        List<MTanggunganPlafon> mtp = SpringInit.getmNasabahDao().tanggunganByIdAnggota(Integer.parseInt(nsbh.getId().toString()));
                        if (mtp.size() > 0) {
                            log.info("====================== SIZE LIST " + mtp.size());

                            for (MTanggunganPlafon datamtp : mtp) {
                                JSONObject jsons = new JSONObject();
                                jsons.put("id_plafon", datamtp.getId_plafon());
                                jsons.put("nominal_tanggungan", datamtp.getNominal_tanggungan().toString());
                                jsons.put("keterangan", datamtp.getKeterangan());
                                jsons.put("status", datamtp.getStatus());

                                arrays2.put(jsons);
                            }

//                    arrays2.put(totalTanggungan.toString());
                        } else {
                            JSONObject jsons = new JSONObject();
                            jsons.put("message", "Data Tanggungan Tidak Ada");
                        }
                        List<MNilaiJaminan> mnj = SpringInit.getMNilaiJaminanDao().viewNilaiJaminan();
                        if (mnj.size() > 0) {
                            log.info("====================== SIZE LIST " + mnj.size());

                            for (MNilaiJaminan datamnj : mnj) {
                                JSONObject jsons = new JSONObject();
                                jsons.put("id", datamnj.getId_nilai_jaminan());
                                jsons.put("value", datamnj.getNm_jaminan());

                                arrays_.put(jsons);
                            }
//MODIFIED 13 AGUSTUS 2019 ============================================================================================================                

                            BigDecimal totalTanggungan = SpringInit.getmNasabahDao().getTotalTanggungan(nsbh.getId().toString());
                            Integer a = totalTanggungan.intValue();
                            if (a >= 0) {
                                a = a;
                            } else {
                                a = 0;
                            }
                            MPresentasePlafon mp = SpringInit.getMPresentasePlafonDao().getPresentasePlafon();
                            int saldo_plafon = mp.getPresentase_plafon();
                            log.info("==================== SALDO PLAFON " + mp.getPresentase_plafon());
                            JSONObject json_ = new JSONObject();
                            json_.put("nama", nsbh.getNama());
                            json_.put("status_pegawai", nsbh.getStatus_pegawai());

                            if (nsbh.getGaji_pokok() == null) {
                                json_.put("gaji_pokok", "0");
                            } else {
                                json_.put("gaji_pokok", nsbh.getGaji_pokok().toString());
                            }

                            json_.put("plafon_kredit", saldo_plafon + "%");

                            int jlh_plafon;
                            if (nsbh.getGaji_pokok() == null) {
                                jlh_plafon = 0 * saldo_plafon / 100;
                            } else {
                                jlh_plafon = nsbh.getGaji_pokok() * saldo_plafon / 100;
                            }

                            json_.put("saldo_plafon", "" + jlh_plafon);

//=================================================================================================================                    
                            JSONObject json2 = new JSONObject();
                            json2.put("data_anggota", json_);
                            json2.put("data_tanggungan", arrays2);
                            json2.put("total_tanggungan", a.toString());
                            json2.put("jenis_pinjaman", array);
                            json2.put("lama_angsuran", arrays);
                            json2.put("jenis_jaminan", arrays_);
                            json2.put("term", "Term Not Available");
                            final String term = SpringInit.getmSettingDao().getTerm().toString();
                            if (term != null) {
                                json2.put("term", term);
                            }

                            //                mlogmobile id user
                            mACtivity.setIdUser(nsbh.getId());
                            ctx.put(Constant.WS.REQUEST, mACtivity);

                            rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);

                        } else {
                            //                mlogmobile id user
                            mACtivity.setIdUser(nsbh.getId());
                            ctx.put(Constant.WS.REQUEST, mACtivity);

                            rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                        }

                    } else {
                        //                mlogmobile id user
                        mACtivity.setIdUser(nsbh.getId());
                        ctx.put(Constant.WS.REQUEST, mACtivity);

                        rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                    }
                } else {
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            } else {
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

    }

    @Override
    public void abort(long id, Serializable context) {
    }

}
