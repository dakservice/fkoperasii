package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MSetting;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.Tripledes;
import com.jcraft.jsch.*;
import org.apache.logging.log4j.core.util.FileUtils;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UploadManager {

    private static ChannelSftp channelSftp = null;
    private static Session session = null;
    private static Channel channel = null;
    private static JSONObject jsonResponse;
    private static JSONArray jsonArray;

    public static JSONObject uploadToProduction(String type, MultipartFile[] parts) {
        Log log = Log.getLog("Q2", "UPLOAD MANAGER");
        jsonResponse = new JSONObject();
        jsonArray = new JSONArray();
        final Date date = new Date();
        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhh:mm");

        //String path = "src/main/resources/";
        String path = "/opt/koperasiDIY/webapps/cms/"+type+"/";
        final MSetting setting = SpringInit.getmSettingDao().getSettingByParam("ssh_setting");

        try {
            final Tripledes tdes = new Tripledes(Constant.SETTING.TRIDES_MOB_ENCRYPT);
            final JSONObject jsonSetting = new JSONObject(tdes.decrypt(setting.getValue()));
            final String SFTPHOST = jsonSetting.getString("ip");
            final int SFTPPORT = jsonSetting.getInt("port");
            final String SFTPUSER = jsonSetting.getString("user");
            final String SFTPPASS = jsonSetting.getString("password");
            String SFTPWORKINGDIR = jsonSetting.getString("path")+type+"/";
            String filename;
            String LOCALDIRECTORY;
            File convFile;
            FileOutputStream stream;
            int i =1;

            String originalFilename;
            final JSch jsch = new JSch();
            for (MultipartFile filed : parts) {
                originalFilename = filed.getOriginalFilename();
                //renaming file with date and number(if user sttore more than one image)
                filename = sdf.format(date)+"-"+i+"."+ originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
                i++;
                jsonArray.put(filename);

                LOCALDIRECTORY = path + filename;
                convFile = new File(LOCALDIRECTORY);
                convFile.createNewFile();
                stream = new FileOutputStream(convFile);
                stream.write(filed.getBytes());
                stream.flush();
                stream.close();

                try {

                    session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
                    session.setPassword(SFTPPASS);
                    java.util.Properties config = new java.util.Properties();
                    config.put("StrictHostKeyChecking", "no");
                    log.info("Setting ssh.....");
                    session.setConfig(config);
                    log.info("Connecting ssh.....");
                    session.connect(); // Create SFTP Session
                    log.info("opening channel....");
                    channel = session.openChannel("sftp"); // Open SFTP Channel
                    log.info("Connecting channel....");
                    channel.connect();
                    log.info("Ssh connected !");
                    channelSftp = (ChannelSftp) channel;
                    log.info("Opening target ssh directory..");
                    System.out.println("asd "+SFTPWORKINGDIR);
                    channelSftp.cd(SFTPWORKINGDIR); // Change Directory on SFTP Server
                    log.info("Uploading...");
                    recursiveUpload(LOCALDIRECTORY, SFTPWORKINGDIR);
                    log.info("Setting file....");
                    channelSftp.chown(1053, SFTPWORKINGDIR + filename);
                    log.info("Deleting file..");
                    Files.delete(convFile.toPath());


                } catch (Exception ex) {
                    ex.printStackTrace();
                    return jsonResponse.put("message","ie").put("error",ex.getMessage());
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
             return jsonResponse.put("message","ie").put("error",e.getMessage());

        }finally {
            log.info("Disconecting ssh..");
            if (channelSftp != null)
                channelSftp.disconnect();
            if (channel != null)
                channel.disconnect();
            if (session != null)
                session.disconnect();
        }
        return jsonResponse.put("message","00").put("data",jsonArray);

    }

    public static boolean uploadToDevelopment(String type) {
        return true;
    }

    private static void recursiveUpload(String sourcePath, String destinationPath) throws SftpException, FileNotFoundException {

        File sourceFile = new File(sourcePath);
        if (sourceFile.isFile()) {

            channelSftp.cd(destinationPath);
            if (!sourceFile.getName().startsWith("."))
                channelSftp.put(new FileInputStream(sourceFile), sourceFile.getName(), ChannelSftp.OVERWRITE);

        } else {

            System.out.println("inside else " + sourceFile.getName());
            File[] files = sourceFile.listFiles();

            if (files != null && !sourceFile.getName().startsWith(".")) {

                channelSftp.cd(destinationPath);
                SftpATTRS attrs = null;

                try {
                    attrs = channelSftp.stat(destinationPath + "/" + sourceFile.getName());
                } catch (Exception e) {
                    System.out.println(destinationPath + "/" + sourceFile.getName() + " not found");
                }

                // else create a directory
                if (attrs != null) {
                    System.out.println("Directory exists IsDir=" + attrs.isDir());
                } else {
                    System.out.println("Creating dir " + sourceFile.getName());
                    channelSftp.mkdir(sourceFile.getName());
                }

                for (File f: files) {
                    recursiveUpload(f.getAbsolutePath(), destinationPath + "/" + sourceFile.getName());
                }

            }
        }

    }
}
