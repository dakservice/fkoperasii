package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.Function;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

public class ViewAllPinjamanNew implements TransactionParticipant {

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();

    @Override
    public int prepare(long l, Serializable serializable) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable serializable) {
        Context ctx = (Context) serializable;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        String notelp = jsonReq.getString("notelp");
        MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
        try {
            final JSONObject response = new JSONObject(Function.getDetailPinjaman(nsbh.getId().toString()));
            rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
            dr = new ResponseWebServiceContainer(rc.getRc(),rc.getRm(),new JSONObject().put("pinjamans",response.getJSONObject("data").getJSONArray("pinjamans")));
        }catch (Exception e){
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long l, Serializable serializable) {

    }
}
