/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MOtp;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Random;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author 1star
 */
public class TestEmail implements TransactionParticipant{
    
    
    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();
    static ChannelSftp channelSftp = null;
    static Session session = null;
    static Channel channel = null;
    static String PATHSEPARATOR = "/";

    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd");
        Boolean forgot_password = false;
      
        try {

            Random rand = new Random();
            int rand_int1 = rand.nextInt(1000000);
            String otp = Integer.toString(rand_int1);
            String notelp;
//            
//                notelp = nsbh.getNotelp();
//                String email = nsbh.getEmail();
//                MOtp mOtp = new MOtp();
//                mOtp.setOtp(otp);
//                mOtp.setType("reset_password");
//                mOtp.setStatus("aktif");
//                mOtp.setNoTelp(notelp);

                String emailHtml = SpringInit.getmSettingDao().getSettingByParam("format_email_cp").getValue();
                emailHtml = emailHtml.replace("#otp", otp);
//                ClientOptions options = ClientOptions.builder()
//                        .apiKey("ee5cb04f1c66e70ad4d265f9bf2d2992")
//                        .apiSecretKey("e32af3572e6b5c0a8d24ef579a0d978f")
//                        .build();
//
//                MailjetClient client = new MailjetClient(options);
//                TransactionalEmail message1 = TransactionalEmail
//                        .builder()
//                        .to(new SendContact(email, nsbh.getNama()))
//                        .from(new SendContact("diykoperasi@gmail.com", "KOPERASI DIY"))
//                        .htmlPart(emailHtml)
//                        .subject("FORGOT PASSWORD KOPERASI APP")
//                        .trackOpens(TrackOpens.ENABLED)
//                        //.attachment(Attachment.fromFile(attachmentPath))
//                        .header("test-header-key", "test-value")
//                        .customID("custom-id-value")
//                        .build();
//
//                SendEmailsRequest request = SendEmailsRequest
//                        .builder()
//                        .message(message1) // you can add up to 50 messages per request
//                        .build();
//
//                // act
//                SendEmailsResponse response = request.sendWith(client);
//                log.info("asd response " + response);
                String email = jsonReq.getString("email");
                log.info(email);
                String email2="sttadik@gmail.com";
                JSONObject from = new JSONObject();
                from.put("email","diykoperasi1@gmail.com");
                from.put("name","KOPERASI");
                JSONObject to = new JSONObject();
                to.put("email",email);
                to.put("name","morten");
                JSONObject bodyEmail = new JSONObject();
                bodyEmail.put("subject","FORGOT PASSWORD KOPERASI APP");
                bodyEmail.put("textpart","");
                bodyEmail.put("html",emailHtml);
                bodyEmail.put("customid","sendMail");
                bodyEmail.put("from",from);
                bodyEmail.put("to",to);
                String bodyemailString=bodyEmail.toString(); 
                OkHttpClient client = new OkHttpClient();

                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, bodyemailString);
                Request request = new Request.Builder()
                        .url("https://yunna-service.herokuapp.com/v1/send_email")
                        .post(body)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("api-key", "YUNNA-2019-ARGAN")
                        .addHeader("mailjet_user", " ee5cb04f1c66e70ad4d265f9bf2d2992")
                        .addHeader("mailjet_pass", "e32af3572e6b5c0a8d24ef579a0d978f")
                        .build();

                Response response = client.newCall(request).execute();

                String bodyresp = response.body().string();
                log.info("respon email"+bodyresp);

//                SpringInit.getmOtpDao().saveOrUpdate(mOtp);
                JSONObject resp = new JSONObject(bodyresp);
//                JSONObject data = new JSONObject();
////                resp.put("email", email);
//                data.put("no_telp", notelp);
//                resp.put("data", data);
//                resp.put("rm", "OTP Sukses Dikirim, Silahkan Cek Email Anda");
//                resp.put("rc", "00");

                rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(resp);
            
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable context) {
    }

    
    
}
