/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MStatusPengajuan;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class ViewDetailPengajuan implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        
        try {
            
            Integer id_pengajuan = jsonReq.getInt("id");
            MPengajuan mp = SpringInit.getMPengajuanDao().pengajuanById(id_pengajuan);
            
//            log.info("================= MASUK DATA PENGAJUAN " +mp.getId());
            
            log.info("================= DATA MP " +mp);
            if(mp != null){
                
                JSONObject logData = new JSONObject();
                logData.put("id", mp.getId());
//                logData.put("noajuan", mp.getNo_ajuan());
                logData.put("ajuanid", mp.getAjuan_id());
//                log.info("=============================== TANGGAL INPUT "+mp.getTgl_input());
                MNasabah nsbh = SpringInit.getmNasabahDao().userById(mp.getAnggota_id().longValue());
                logData.put("namaanggota", nsbh.getNama());
                logData.put("tanggalinput", mp.getTgl_input());
                MJenisPinjaman mjpinjam = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(Integer.parseInt(mp.getJenis()));
                logData.put("jenis", mjpinjam.getNama_pinjaman());
                logData.put("nominal", mp.getNominal().toString());
                logData.put("lamaangsuran", mp.getLama_ags().toString());
                logData.put("keterangan", mp.getKeterangan());
                MStatusPengajuan msp = SpringInit.getMStatusPengajuanDao().statusById(Integer.parseInt(mp.getStatus()));
                logData.put("status", msp.getStatus());
//                logData.put("alasan", mp.getAlasan());
                logData.put("tanggalcair", mp.getTgl_cair().toString());
//                logData.put("tanggalupdate",mp.getTgl_update().toString());
                
                    
                    JSONObject json2 = new JSONObject();
                    json2.put("data", logData);
                                        //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
            }else{
                                    //                mlogmobile id user
             
                rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.DATA_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
            
            
        } catch (Exception e) {
            
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++"+stringWriter.toString());
            
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
            
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
