/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MLogTrx;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MTransSP;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.List;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class Login1 implements TransactionParticipant{
    
//    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        
        try {
            int setoran = 0;
            int penarikan = 0;
            int transaksi = 0;
            String notelp = jsonReq.getString("notelp");
            String pass = jsonReq.getString("password");
            String password = SpringInit.getmNasabahDao().encryptPasswordSHA1(pass);
            System.out.println("No Telepon " + notelp);
            log.info("========================= NO TELEPON " +notelp);
            log.info("========================= PASSWORD " +password);
            
            MNasabah nsbh = SpringInit.getmNasabahDao().loginUser(notelp, password);
            
            if (nsbh != null) {
               
//                if (notelp != nsbh.getNotelp()){
//                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USERNAME_MISMATCH);
//                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
//                } 
//                else if (nsbh.getPass_word() != password){
//                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.PASSWORD_MISMATCH);
//                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
//                }
//                if{
                    log.info("====================== LOGIN SUKSES");
                    log.info("====================== SALDO "+nsbh.getSaldo_plafon());
                    
                List<MTransSP> mt = SpringInit.getMTransSPDao().getAllSetoran(nsbh.getId().intValue());
                List<MTransSP> mts = SpringInit.getMTransSPDao().getAllPenarikan(nsbh.getId().intValue());
                List<MLogTrx> ml = SpringInit.getMLogTrxDao().getHistorySaldo(nsbh.getId().toString());
                
                if (mt.size() <= 0 && mts.size() <=0 && ml.size() <=0) {
//                    log.info("====================== SIZE LIST " +mt.size());
                    
                    JSONObject json = new JSONObject();
                    json.put("username", notelp);
                    json.put("saldo", "0");
                    json.put("nama", nsbh.getNama());
                    JSONObject json2 = new JSONObject();
                    json2.put("data", json);
                    
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(),json2);
                    
                } else if(mt.size() > 0 && mts.size() > 0 && ml.size() > 0){
//                    log.info("====================== SIZE LIST " +mt.size());
//                    log.info("====================== SIZE LIST " +mts.size());
//                    log.info("====================== SBLM FOR ");
                    
                    for (MTransSP datamt : mt) {
//                        log.info("====================== SBLM JSON");
                        JSONObject logData = new JSONObject();
//                        log.info("====================== SBLM JUMLAH SETORAN");
//                        log.info("====================== JUMLAH SETORAN " +mt);
                    
                        int jumlah = datamt.getJumlah().intValue();
                        setoran +=jumlah;
                        logData.put("setoran",setoran);
                    }
                    
                    for (MTransSP datamts : mts) {
//                        log.info("====================== SBLM JSON");
                        JSONObject logData = new JSONObject();
//                        log.info("====================== SBLM JUMLAH SETORAN");
//                        log.info("====================== JUMLAH SETORAN " +mt);

                        int jumlahs = datamts.getJumlah().intValue();
                        penarikan +=jumlahs;
                        logData.put("penarikan",penarikan);
                    }
                    
                    for (MLogTrx datamlt : ml) {
//                        log.info("====================== SBLM JSON");
                        JSONObject logData = new JSONObject();
//                        log.info("====================== SBLM JUMLAH SETORAN");
//                        log.info("====================== JUMLAH SETORAN " +mt);

                        double nominal = datamlt.getAmount();
                        transaksi +=nominal;
                        logData.put("transaksi",transaksi);
                    }
                    
                    JSONObject json = new JSONObject();
                    json.put("username", notelp);
                    json.put("saldo", setoran - penarikan - transaksi);
                    json.put("nama", nsbh.getNama());
                    JSONObject json2 = new JSONObject();
                    json2.put("data", json);
//                    log.info("=============================== LOG DATA :"+json2);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
                }else{
                    for (MTransSP datamt : mt) {
//                        log.info("====================== SBLM JSON");
                        JSONObject logData = new JSONObject();
//                        log.info("====================== SBLM JUMLAH SETORAN");
//                        log.info("====================== JUMLAH SETORAN " +mt);
                    
                        int jumlah = datamt.getJumlah().intValue();
                        setoran +=jumlah;
                        logData.put("setoran",setoran);
                    }
                    JSONObject json = new JSONObject();
                    json.put("notelp", notelp);
                    json.put("saldo", setoran);
                    json.put("nama", nsbh.getNama());
                    JSONObject json2 = new JSONObject();
                    
                    json2.put("data", json);
                    log.info("=============================== LOG DATA :"+json2);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
                }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

//                    JSONObject json = new JSONObject();
//                    json.put("notelp", nsbh.getNotelp());
//                    json.put("saldo", nsbh.getSaldo_plafon());
//                    json.put("nama", nsbh.getNama());
//
//                    JSONObject json2 = new JSONObject();
//                    json2.put("data", json);
//                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_SUCCESS);
//                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
//                }
                
                if (nsbh.getAktif().equals("N")) {
                    log.info("====================== USER NOT ACTIVE");
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
                
            } else {
                if (notelp == null){
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USERNAME_MISMATCH);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                } 
                else if (password == null){
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.PASSWORD_MISMATCH);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }else{
                
                    log.info("====================== LOGIN FAILED");
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_FAILED);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            }

//            if (nsbh == null) {
//                log.info("====================== LOGIN FAILED");
//                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_FAILED);
//                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
//
//            } else if (nsbh.getAktif().equals("N")) {
//                log.info("====================== USER NOT ACTIVE");
//
//                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
//                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
//
//            } else {
//                log.info("====================== LOGIN SUKSES");
//                log.info("====================== SALDO "+nsbh.getSaldo_plafon());
//                             
//                
//                JSONObject json = new JSONObject();
//                json.put("username", nsbh.getNotelp());
//                json.put("saldo", nsbh.getSaldo_plafon());
//                
//                JSONObject json2 = new JSONObject();
//                json2.put("data", json);
//                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_SUCCESS);
//                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
//
//            }
        } catch (Exception e) {
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

    
}
