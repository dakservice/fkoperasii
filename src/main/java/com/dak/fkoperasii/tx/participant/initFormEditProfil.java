/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MAgama;
import com.dak.fkoperasii.entity.MDepartement;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class initFormEditProfil implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;

    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        String notelp = jsonReq.getString("notelp");
        MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
//        JSONArray datas = new JSONArray();
//        JSONArray array = new JSONArray();
        JSONArray arrays = new JSONArray();
        JSONArray arrays_ = new JSONArray();
        JSONArray arrays2 = new JSONArray();
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            log.info("============================== DATA NASABAH " + nsbh);

            if (nsbh != null) {

                List<MAgama> ma = SpringInit.getMProfileDao().getAgama();
                if (ma.size() > 0) {
//                    log.info("====================== SIZE LIST " +mj.size());

                    for (MAgama datama : ma) {
                        JSONObject jsons = new JSONObject();
                        jsons.put("id_agama", datama.getId_agama());
                        jsons.put("nama_agama", datama.getNm_agama());

                        arrays.put(jsons);
                    }

                    List<Object[]> mp = SpringInit.getMProfileDao().getPekerjaan();
                    if (mp.size() > 0) {
                        log.info("====================== SIZE LIST " + mp.size());

                        for (Object[] datamp : mp) {
                            JSONObject jsons = new JSONObject();
                            jsons.put("id_pekerjaan", datamp[0].toString());
                            jsons.put("jenis_pekerjaan", datamp[1].toString());

                            arrays2.put(jsons);
                        }

//                    arrays2.put(totalTanggungan.toString());
                    } else {
                        JSONObject jsons = new JSONObject();
                        jsons.put("message", "Data Pekerjaan Tidak Ada");
                    }

                    List<MDepartement> md = SpringInit.getMProfileDao().getDepartement();
                    if (md.size() > 0) {
                        log.info("====================== SIZE LIST " + md.size());

                        for (MDepartement datamd : md) {
                            JSONObject jsons = new JSONObject();
                            jsons.put("id_departement", datamd.getId_departement());
                            jsons.put("nama_departement", datamd.getNm_departement());

                            arrays_.put(jsons);
                        }

                        JSONObject json_ = new JSONObject();
                        if (nsbh.getNik() == null) {
                            json_.put("nik", "");
                        } else {
                            json_.put("nik", nsbh.getNik());
                        }
                        if (nsbh.getNama() == null) {
                            json_.put("nama_lengkap", "");
                        } else {
                            json_.put("nama_lengkap", nsbh.getNama());
                        }
                        if (nsbh.getStatus_pegawai() == null) {
                            json_.put("status_pegawai", "");
                        } else {
                            json_.put("status_pegawai", nsbh.getStatus_pegawai());
                        }
                        if (nsbh.getNotelp() == null) {
                            json_.put("telepon", "");
                        } else {
                            json_.put("telepon", nsbh.getNotelp());
                        }
                        if (nsbh.getTmp_lahir() == null) {
                            json_.put("tempat_lahir", "");
                        } else {
                            json_.put("tempat_lahir", nsbh.getTmp_lahir());
                        }

                        MAgama ag = SpringInit.getMProfileDao().agamaById(nsbh.getId_agama());
                        if (nsbh.getId_agama() != null) {
                            if (nsbh.getId_agama() == 0) {
                                json_.put("agama", "Tidak Diketahui");
                            } else if (nsbh.getId_agama() == null) {
                                json_.put("agama", "");
                            } else {
                                json_.put("agama", ag.getNm_agama());
                            }
                        }

                        if (nsbh.getAlamat() == null) {
                            json_.put("alamat", "");
                        } else {
                            json_.put("alamat", nsbh.getAlamat());
                        }
                        if (nsbh.getPekerjaan() == null) {
                            json_.put("pekerjaan", "");
                        } else {
                            json_.put("pekerjaan", nsbh.getPekerjaan());
                        }
                        if (nsbh.getNip() == null) {
                            json_.put("nip", "");
                        } else {
                            json_.put("nip", nsbh.getNip());
                        }
                        if (nsbh.getEmail() == null) {
                            json_.put("email", "");
                        } else {
                            json_.put("email", nsbh.getEmail());
                        }
                        if (nsbh.getJk() == null) {
                            json_.put("jenis_kelamin", "");
                        } else {
                            json_.put("jenis_kelamin", nsbh.getJk());
                        }
                        if (nsbh.getTgl_lahir() == null) {
                            json_.put("tanggal_lahir", "");
                        } else {
                            json_.put("tanggal_lahir", nsbh.getTgl_lahir());
                        }
                        if (nsbh.getStatus() == null) {
                            json_.put("status", "");
                        } else {
                            json_.put("status", nsbh.getStatus());
                        }
                        if (nsbh.getKota() == null) {
                            json_.put("kota", "");
                        } else {
                            json_.put("kota", nsbh.getKota());
                        }
                        if (nsbh.getRek_simpeda() == null) {
                            json_.put("rekening_simpeda", "");
                        } else {
                            json_.put("rekening_simpeda", nsbh.getRek_simpeda());
                        }
                        if (nsbh.getDepartement() == null) {
                            json_.put("departement", "");
                        } else {
                            json_.put("departement", nsbh.getDepartement());
                        }

//=================================================================================================================                    
                        JSONObject json2 = new JSONObject();
                        json2.put("data_anggota", json_);
                        json2.put("data_pekerjaan", arrays2);
                        json2.put("data_agama", arrays);
                        json2.put("data_departement", arrays_);
                        //                mlogmobile id user
                        mACtivity.setIdUser(nsbh.getId());
                        ctx.put(Constant.WS.REQUEST, mACtivity);

                        rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);

                    } else {
                        //                mlogmobile id user
                        mACtivity.setIdUser(nsbh.getId());
                        ctx.put(Constant.WS.REQUEST, mACtivity);

                        rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                    }

                } else {
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }

            } else {
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

    }

    @Override
    public void abort(long id, Serializable context) {
    }

}
