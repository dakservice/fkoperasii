/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MDeviceAnggota;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MOtp;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Date;
import java.util.Random;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author 1star
 */
public class AddDevice implements TransactionParticipant {

//    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);

        try {
            String device_id = jsonReq.getString("device_id");
            String device_name = jsonReq.getString("device_name");
            String noTelp = jsonReq.getString("no_telp");
            String otp = jsonReq.getString("otp");
//            Tripledes tdes = new Tripledes(Constant.SETTING.TRIDES_MOB_ENCRYPT);

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(noTelp);
            log.info("====================== DATA NSBH " + nsbh);

            if (nsbh == null) {
                log.info("====================== User Tidak Ditemukan");
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_FAILED);
                dr = new ResponseWebServiceContainer(rc.getRc(), "User Tidak Ditemukan");
                ctx.put(Constant.LOGIN.STATUS_LOGIN, "User Tidak Ditemukan");
//                log.info("====================== STATUS LOGIN" +ctx.get(Constant.LOGIN.STATUS_LOGIN, rc));

                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return ABORTED | NO_JOIN;

            } else if (nsbh.getAktif().equals("N")) {

                log.info("====================== USER NOT ACTIVE");

                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);

                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return ABORTED | NO_JOIN;

            } else {
                log.info("---- Cek OTP");
                MOtp motp = SpringInit.getmOtpDao().getOtpByDate(otp);

                if (motp == null) {
                    log.info("====================== OTP Salah");

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer("02", "OTP Salah");
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);

                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return ABORTED | NO_JOIN;
                }

                Date date = new Date();
                Date expired = motp.getCreateAt();
                long diffInMillies = date.getTime() - expired.getTime();
                log.info(expired.toString());
                log.info(date.toString());
                long difference_In_Minutes = (diffInMillies
                        / (1000 * 60));
                if (difference_In_Minutes > 10) {

                    log.info("OTP Kadaluarsa");
                    rc = SpringInit.getmRcDao().getRc("FA");
                    dr = new ResponseWebServiceContainer("02", "OTP Anda Kadalaursa, Silahkan Request OTP Lagi");

                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
                    return ABORTED | NO_JOIN;

                }
//                log.info("enpas :" + enPass + "\n" + "pass " + nsbh.getPass_word());
                log.info("OTP BENAR");
                MDeviceAnggota device = new MDeviceAnggota();

                device.setDevice_id(device_id);
                device.setDevices_name(device_name);
                device.setIdAnggota(nsbh.getId());
                device.setStatus("false");
                SpringInit.getMDeviceAnggotaDao().saveOrUpdate(device);
                dr = new ResponseWebServiceContainer("00", "Device Berhasil Ditambahkan, Silahkan Aktifkan Lewat Web");
                rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);

                ctx.put(Constant.USER_DETAIL, nsbh);
                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return PREPARED | NO_JOIN;

            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
            ctx.put(Constant.WS.RESPONSE, dr);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
            return ABORTED | NO_JOIN;
        }

    }

    @Override
    public void commit(long id, Serializable srlzbl) {

    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

}
