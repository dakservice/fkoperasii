/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.utility.Constant;
import java.io.Serializable;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.transaction.Context;
import org.jpos.transaction.GroupSelector;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;

/**
 *
 * @author Morten Jonathan
 */
public class ControllerSwitch implements GroupSelector, Configurable{
    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public String select(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        return cfg.get(ctx.getString(Constant.CONTROLLER.GROUP), "NoRoute");
    }

    @Override
    public int prepare(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        String path = ctx.getString(Constant.PATH);
        if(path == null || "".equals(path)) {
            ctx.put(Constant.CONTROLLER.GROUP, "NoRoute");
        } else {
            ctx.put(Constant.CONTROLLER.GROUP, path);
        }
        return PREPARED | NO_JOIN;
    }

    @Override
    public void setConfiguration(Configuration c) throws ConfigurationException {
        this.cfg = c;
    }

    @Override
    public void commit(long id, Serializable context) {
    }

    @Override
    public void abort(long id, Serializable context) {
    }
    
}
