/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MBuktiJaminan;
import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MProduct;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MStatusPengajuan;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Morten Jonathan
 */
public class SavePengajuan implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());
    static ChannelSftp channelSftp = null;
    static Session session = null;
    static Channel channel = null;
    static String PATHSEPARATOR = "/";

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(new Date());
        Calendar cal = Calendar.getInstance();
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            log.info("================= MASUK TRY ");
            int total = 0;
            String ajuan_id;
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            log.info("================= MASUK NASABAH " + nsbh.getAktif());
            int jenis = jsonReq.getInt("jenis");
            MJenisPinjaman mjp = SpringInit.getMJenisPinjamanDao().jenisPinjamanByIdStatus(jenis, nsbh.getStatus_pegawai());
            String id_barang = jsonReq.getString("idbarang");
            MProduct mpr = SpringInit.getMProductDao().barangById(Long.parseLong(id_barang));
            Integer nominal = mpr.getHarga();
            Integer lama_angsuran = mjp.getLama_angsuran();
            String ket = null;
            String jaminan = jsonReq.getString("jaminan");
            MultipartFile[] file_images = (MultipartFile[]) ctx.get(Constant.IMAGE);

            if (nsbh.getAktif().equals("Y")) {
                log.info("================= MASUK IF ");
                MPengajuan mj = SpringInit.getMPengajuanDao().pengajuanByAjuanId();
                MPengajuan mp = new MPengajuan();
                int no_aj = Integer.valueOf(mj.getNo_ajuan()) + 1;

                mp.setNo_ajuan("" + no_aj);
                log.info("================= NO AJUAN " + mp.getNo_ajuan());

                if (mj != null) {
                    String id_trx = mj.getAjuan_id().substring(12, 13);
                    log.info("======================= GET ID TRANSAKSI " + mj.getAjuan_id());

                    if (Integer.parseInt(id_trx) == 0) {
                        id_trx = mj.getAjuan_id().substring(11, 13);
                        log.info("============== SUBSTRING ID TRX " + id_trx);

                    } else {
                        id_trx = mj.getAjuan_id().substring(11, 13);
                        log.info("============== SUBSTRING ID TRX " + id_trx);

                    }
                    String AN = "" + (Integer.parseInt(id_trx) + 1);
                    log.info("======================= ISI AN " + AN);
                    String Nol = "";
                    if (AN.length() == 1) {
                        Nol = "00";
                    } else if (AN.length() == 2) {
                        Nol = "0";
                    } else if (AN.length() == 3) {
                        Nol = "";
                    }
                    ajuan_id = "PRM.19.08." + Nol + AN;
                } else {
                    ajuan_id = "PRM.19.08.001";
                }

                log.info("======================= ISI AJUAN ID " + ajuan_id);
                mp.setAjuan_id(ajuan_id);
                mp.setAnggota_id(Integer.parseInt(nsbh.getId().toString()));
                mp.setTgl_input(datetime.format(new Date()));
                mp.setJenis("" + jenis);
                mp.setNominal(nominal.toString());
                mp.setLama_ags(lama_angsuran.toString());

                log.info("==================== NAMA PINJAMAN " + mjp.getNama_pinjaman());
                if (mjp.getNama_pinjaman().equals("Pinjaman Motor")) {
                    ket = "Pengajuan Kredit Motor";
                    mp.setKeterangan(ket);
                } else if (mjp.getNama_pinjaman().equals("Pinjaman Barang")) {
                    ket = "Pengajuan Kredit Barang";
                    mp.setKeterangan(ket);
                } else {
                    ket = "Pengajuan Kredit Barang";
                    mp.setKeterangan(ket);
                }

                mp.setStatus("0");
                mp.setAlasan("");
                mp.setTgl_cair(date);
                mp.setTgl_update(datetime.format(new Date()));
                mp.setId_barang(Integer.parseInt(id_barang));
                mp.setId_nilai_jaminan(Integer.parseInt(jaminan));

                log.info("=============================== MASUK LOG DATA");
                JSONObject logData = new JSONObject();
                logData.put("noajuan", mp.getNo_ajuan().toString());
                logData.put("ajuanid", mp.getAjuan_id());
                logData.put("anggotaid", mp.getAnggota_id().toString());
                log.info("=============================== TANGGAL INPUT " + mp.getTgl_input());
                logData.put("tanggalinput", mp.getTgl_input().toString());
                MJenisPinjaman mjpinjam = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(jenis);
                logData.put("jenis", mjpinjam.getNama_pinjaman());
                logData.put("nominal", mp.getNominal().toString());
                logData.put("lamaangsuran", mp.getLama_ags().toString());
                logData.put("keterangan", mp.getKeterangan());
                MStatusPengajuan msp = SpringInit.getMStatusPengajuanDao().statusById(Integer.parseInt(mp.getStatus()));
                logData.put("status", msp.getStatus());
                logData.put("alasan", mp.getAlasan());
                logData.put("tanggalcair", mp.getTgl_cair().toString());
                logData.put("tanggalupdate", mp.getTgl_update().toString());
                logData.put("idbarang", mp.getId_barang().toString());
                logData.put("idjaminan", mp.getId_nilai_jaminan().toString());

                log.info("================= MASUK MAU SAVE ");

                mp = SpringInit.getMPengajuanDao().saveOrUpdate(mp);

//==== UPLOAD FOTO MULTIPART BEDA SERVER ==================================================================================                    
                if (file_images.length > 0) {

                    int i = 1;
                    String path = "/opt/fkoperasii/webapps/cms/bukti_jaminan/";
                    String SFTPHOST = "94.237.76.76";
                    int SFTPPORT = 1789;
                    String SFTPUSER = "root";
                    String SFTPPASS = "Qawsed#1689";
                    String SFTPWORKINGDIR = "/home/koperasidakco/public_html/uploads/pengajuan/";

                    JSch jsch = new JSch();
                    for (MultipartFile filed : file_images) {
                        String filename = filed.getOriginalFilename();
                        String LOCALDIRECTORY = path + filename;
                        File convFile = new File(LOCALDIRECTORY);
                        convFile.createNewFile();
                        FileOutputStream stream = new FileOutputStream(convFile);
                        stream.write(filed.getBytes());
                        stream.flush();
                        stream.close();

                        try {

                            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
                            log.info("LEWATTTTTTTTTTTTTTTTT1");

                            session.setPassword(SFTPPASS);
                            log.info("LEWATTTTTTTTTTTTTTTTT2");

                            java.util.Properties config = new java.util.Properties();
                            log.info("LEWATTTTTTTTTTTTTTTTT3");

                            config.put("StrictHostKeyChecking", "no");
                            log.info("LEWATTTTTTTTTTTTTTTTT4");

                            session.setConfig(config);
                            log.info("LEWATTTTTTTTTTTTTTTTT5");

                            session.connect(); // Create SFTP Session
                            log.info("LEWATTTTTTTTTTTTTTTTT6");

                            channel = session.openChannel("sftp"); // Open SFTP Channel
                            log.info("LEWATTTTTTTTTTTTTTTTT7");

                            channel.connect();
                            log.info("LEWATTTTTTTTTTTTTTTTT8");

                            channelSftp = (ChannelSftp) channel;
                            log.info("LEWATTTTTTTTTTTTTTTTT9");

                            channelSftp.cd(SFTPWORKINGDIR); // Change Directory on SFTP Server
                            log.info("LEWATTTTTTTTTTTTTTTTT10");

                            recursiveUpload(LOCALDIRECTORY, SFTPWORKINGDIR);
                            log.info("LEWATTTTTTTTTTTTTTTTT11");

                            channelSftp.chown(1036, SFTPWORKINGDIR + filename);

                            MBuktiJaminan mbj = new MBuktiJaminan();
                            MPengajuan mpn = SpringInit.getMPengajuanDao().getIdPengajuan();
                            int id_pengajuan = mpn.getId();
                            log.info("============================ GET ID PENGAJUAN " + id_pengajuan);
                            mbj.setId_bukti_jaminan(id_pengajuan);
                            mbj.setFoto_bukti(filename);
                            mbj.setNilai_agunan(0);
                            SpringInit.getMBuktiJaminanDao().saveOrUpdate(mbj);

                        } catch (Exception ex) {
                            log.info("MASUK CATCH GAGAL");
                            ex.printStackTrace();
                        } finally {
                            if (channelSftp != null) {
                                channelSftp.disconnect();
                            }
                            if (channel != null) {
                                channel.disconnect();
                            }
                            if (session != null) {
                                session.disconnect();
                            }

                        }

                    }
                }
//==== UPLOAD FOTO MULTIPART BEDA SERVER ==================================================================================

                log.info("========== MASUK KE HAPUS DATA KERANJANG");
                SpringInit.getTmpDetailBrgDao().deleteCartBrg(Integer.parseInt(nsbh.getId().toString()), Integer.parseInt(id_barang));

                JSONArray array = new JSONArray();
                JSONArray arrays = new JSONArray();

                array.put(logData);
//                    JSONObject json = new JSONObject();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsons = array.getJSONObject(i);
                    JSONArray array2 = jsons.names();
                    for (int j = i; j < array2.length(); j++) {
                        JSONObject logs = new JSONObject();
                        logs.put("item", array2.getString(j));
                        logs.put("value", jsons.get(array2.getString(j)));
                        arrays.put(logs);
                    }
                }

                JSONObject json2 = new JSONObject();
                json2.put("data", arrays);

                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable context) {
    }

    private static void recursiveUpload(String sourcePath, String destinationPath) throws SftpException, FileNotFoundException {

        File sourceFile = new File(sourcePath);
        if (sourceFile.isFile()) {

            channelSftp.cd(destinationPath);
            if (!sourceFile.getName().startsWith(".")) {
                channelSftp.put(new FileInputStream(sourceFile), sourceFile.getName(), ChannelSftp.OVERWRITE);
            }

        } else {

            System.out.println("inside else " + sourceFile.getName());
            File[] files = sourceFile.listFiles();

            if (files != null && !sourceFile.getName().startsWith(".")) {

                channelSftp.cd(destinationPath);
                SftpATTRS attrs = null;

                try {
                    attrs = channelSftp.stat(destinationPath + "/" + sourceFile.getName());
                } catch (Exception e) {
                    System.out.println(destinationPath + "/" + sourceFile.getName() + " not found");
                }

                // else create a directory
                if (attrs != null) {
                    System.out.println("Directory exists IsDir=" + attrs.isDir());
                } else {
                    System.out.println("Creating dir " + sourceFile.getName());
                    channelSftp.mkdir(sourceFile.getName());
                }

                for (File f : files) {
                    recursiveUpload(f.getAbsolutePath(), destinationPath + "/" + sourceFile.getName());
                }

            }
        }

    }

}
