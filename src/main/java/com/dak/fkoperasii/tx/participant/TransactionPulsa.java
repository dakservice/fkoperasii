/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MCoa;
import com.dak.fkoperasii.entity.MJurnal;
import com.dak.fkoperasii.entity.MKas;
import com.dak.fkoperasii.entity.MLogTrx;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MProduct;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MTransSP;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class TransactionPulsa implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    String notelp, nama_produk,type, merk, kategori, idtrx;
    int saldo, harga, sisa_saldo;
    int penarikan = 0;
    int setoran = 0;
    int transaksi = 0;
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        String date = format.format(new Date());
//        DecimalFormat df = new DecimalFormat("#");
        
        try {
            log.info("======================= MASUK TRY");
            notelp = jsonReq.getString("notelp");
            merk = jsonReq.getString("provider");
            
            log.info("======================= MERK "+merk);
            
            List<MProduct> mp = SpringInit.getMProductDao().productByMerk(merk);
            
            if(mp.size() > 0){
                JSONArray data = new JSONArray();
            for (MProduct mProduct : mp) {
                JSONObject logData = new JSONObject();
                    logData.put("id_produk", mProduct.getId());
                    logData.put("nama_produk", mProduct.getNm_barang());
                    logData.put("merk", mProduct.getMerk());
                    logData.put("harga", mProduct.getHarga().toString());
                    logData.put("jumlah_produk", mProduct.getJml_brg().toString());
                    
                    
                    String foto_produk = mProduct.getFoto_produk();
                    log.info("================= MASUK FOTO ");
                    if (foto_produk != null) {
                        if (!"".equals(foto_produk)) {
                            log.info("================= MASUK IF PERTAMA ");
                            String[] data_image = foto_produk.split(";", -1);
                            log.info("================= MASUK DATA IMAGE ");
                            logData.put("foto_produk", SpringInit.getMRoutingDao().getConstantDB("img_url") + data_image[0]);
                            log.info("================= MASUK LOG DATA FOTO ");
                        } else {
                            log.info("================= MASUK ELSE ");
                            logData.put("foto_produk", "");
                        }
                    } else {
                        log.info("================= MASUK FOTO NULL");
                        logData.put("foto_produk", "");
                    }

                    data.put(logData);
                    JSONObject json2 = new JSONObject();
                    json2.put("data", data);
                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
            }
            }else{
                
                rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.DATA_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                
            }

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++"+stringWriter.toString());
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);

        }
            ctx.put(Constant.WS.RESPONSE, dr);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
        
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
    
}
