/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.Date;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class ChangePassword implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
    MRc rc = new MRc();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject bodyData = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MNasabah nsbh = (MNasabah) ctx.get(Constant.TRX.USER_DETAIL);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);
//        MPartnerCore partnerCore = (MPartnerCore) ctx.get(Constant.WS.PARTNER_CORE);

        try {

            String oldPin = bodyData.getString("password_lama");
            String newPin = bodyData.getString("password_baru");
            String confNewPin = bodyData.getString("konfirmasi");
            if (oldPin.equals(nsbh.getPass_word())) {
                if (newPin.equals(confNewPin)) {
//                    String otp = Utility.getOtp();
//                    MOtp mOtp = new MOtp();
//                    mOtp.setIdActivity(activity);
//                    mOtp.setCreateDate(new Date());
//                    mOtp.setOtp(tripledes.encrypt(otp));
//                    mOtp.setStatus(true);
//                    mOtp.setCorePartnerId(partnerCore);
//
//                    mOtp = SpringInit.getmOtpDao().saveOrUpdate(mOtp);

                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);

//                    ctx.put(Constant.WS.OTP, otp);
                    ctx.put(Constant.WS.MSISDN, nsbh.getNotelp());
                    ctx.put(Constant.WS.RESPONSE, wsResponse);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return PREPARED | NO_JOIN;
                } else {
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.PASSWORD_MISMATCH);
                    wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);

                    ctx.put(Constant.WS.RESPONSE, wsResponse);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return ABORTED | NO_JOIN;
                }
            } else {
                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.PASSWORD_AUTH_FAILED);
                wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);

                ctx.put(Constant.WS.RESPONSE, wsResponse);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return ABORTED | NO_JOIN;

            }

        } catch (Exception e) {

            e.printStackTrace();
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            wsResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), bodyData);

            ctx.put(Constant.WS.RESPONSE, wsResponse);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
            return ABORTED | NO_JOIN;

        }

    }

    @Override
    public void commit(long id, Serializable context) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void abort(long id, Serializable context) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
