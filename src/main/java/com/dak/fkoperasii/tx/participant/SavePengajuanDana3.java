/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MBuktiJaminan;
import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MStatusPengajuan;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Morten Jonathan
 */
public class SavePengajuanDana3 implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(new Date());
        Calendar cal = Calendar.getInstance();
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        
        try{
            StringBuilder sb = new StringBuilder();
            
            log.info("================= MASUK TRY ");
            int total = 0;
            String ajuan_id;
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            log.info("================= MASUK NASABAH " +nsbh.getAktif());
            int jenis = jsonReq.getInt("jenis");
            MJenisPinjaman mjp = SpringInit.getMJenisPinjamanDao().jenisPinjamanByIdStatus(jenis, nsbh.getStatus_pegawai());
            Integer nominal = jsonReq.getInt("nominal");
            Integer lama_angsuran = jsonReq.getInt("lamaangsuran");
            String ket = jsonReq.getString("keterangan");
            String jaminan = jsonReq.getString("jaminan");
            MultipartFile[] file_images = (MultipartFile[]) ctx.get(Constant.IMAGE);
            
            if(nsbh.getAktif().equals("Y")){        
                log.info("================= MASUK IF ");
                MPengajuan mj = SpringInit.getMPengajuanDao().pengajuanByAjuanId();
                MPengajuan mp = new MPengajuan();
                int no_aj = Integer.valueOf(mj.getNo_ajuan())+1;
                    
                mp.setNo_ajuan(""+no_aj);
                log.info("================= NO AJUAN "+mp.getNo_ajuan());
                    
                    if(mj != null){
                        String id_trx = mj.getAjuan_id().substring(12, 13);
                        log.info("======================= GET ID TRANSAKSI "+mj.getAjuan_id());
                        
                        if(Integer.parseInt(id_trx) == 0){
                            id_trx = mj.getAjuan_id().substring(11, 13);
                            log.info("============== SUBSTRING ID TRX "+id_trx);
                            
                        }else{
                                id_trx = mj.getAjuan_id().substring(11, 13);
                                log.info("============== SUBSTRING ID TRX "+id_trx);
                            
                        }
                        String AN = "" + (Integer.parseInt(id_trx) + 1);
                        log.info("======================= ISI AN "+AN);
                        String Nol = "";
                        if(AN.length()==1){
                            Nol = "00";
                        }
                        else if(AN.length()==2){
                            Nol = "0";
                        }
                        else if(AN.length()==3){
                            Nol = "";
                        }
                        ajuan_id = "PRM.19.08." + Nol + AN;           
                    }else{
                        ajuan_id = "PRM.19.08.001";
                    }
                    
                    log.info("======================= ISI AJUAN ID "+ajuan_id);
                    mp.setAjuan_id(ajuan_id);
                    mp.setAnggota_id(Integer.parseInt(nsbh.getId().toString()));
                    mp.setTgl_input(datetime.format(new Date()));
                    mp.setJenis(""+jenis);
                    mp.setNominal(nominal.toString());
                    mp.setLama_ags(lama_angsuran.toString());
                    mp.setKeterangan(ket);
                    mp.setStatus("0");
                    mp.setAlasan("");
                    mp.setTgl_cair(date);
                    mp.setTgl_update(datetime.format(new Date()));
                    mp.setId_barang(4);
                    mp.setId_nilai_jaminan(Integer.parseInt(jaminan));
                    
                    log.info("=============================== MASUK LOG DATA");
                    JSONObject logData = new JSONObject();
                    logData.put("noajuan", mp.getNo_ajuan().toString());
                    logData.put("ajuanid", mp.getAjuan_id());
                    logData.put("anggotaid", mp.getAnggota_id().toString());
                    log.info("=============================== TANGGAL INPUT "+mp.getTgl_input());
                    logData.put("tanggalinput", mp.getTgl_input().toString());
                    MJenisPinjaman mjpinjam = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(jenis);
                    logData.put("jenis", mjpinjam.getNama_pinjaman());
                    logData.put("nominal", mp.getNominal().toString());
                    logData.put("lamaangsuran", mp.getLama_ags().toString());
                    logData.put("keterangan", mp.getKeterangan());
                    MStatusPengajuan msp = SpringInit.getMStatusPengajuanDao().statusById(Integer.parseInt(mp.getStatus()));
                    logData.put("status", msp.getStatus());
                    logData.put("alasan", mp.getAlasan());
                    logData.put("tanggalcair", mp.getTgl_cair().toString());
                    logData.put("tanggalupdate",mp.getTgl_update().toString());
                    logData.put("idbarang",mp.getId_barang().toString());
                    logData.put("idjaminan",mp.getId_nilai_jaminan().toString());
            
                    log.info("================= MASUK MAU SAVE ");
                    mp = SpringInit.getMPengajuanDao().saveOrUpdate(mp);
            
                    
                    if (file_images.length > 0) {
                    
                    int i = 1;
                for (MultipartFile file : file_images) {
                    String filename = file.getOriginalFilename();
                    MBuktiJaminan mbj = new MBuktiJaminan();
                    MPengajuan mpn = SpringInit.getMPengajuanDao().getIdPengajuan();
                    int id_pengajuan = mpn.getId();
                    log.info("============================ GET ID PENGAJUAN "+id_pengajuan);
                    mbj.setId_bukti_jaminan(id_pengajuan);
                    mbj.setFoto_bukti(filename);
                    mbj.setNilai_agunan(0);
                    
                    
                    
                    File convFile = null;
                    convFile = new File(SpringInit.getMRoutingDao().getConstantDB("buktijaminan_url").getValue()+ filename);
                    log.info("===================== BUKTI "+convFile.toString());
//                    convFile.createNewFile();
                    SpringInit.getMBuktiJaminanDao().saveOrUpdate(mbj);
                    
                    FileOutputStream stream = new FileOutputStream(convFile);
                    stream.write(file.getBytes());
                    stream.flush();
                    stream.close();
                    
                }

}
                        
                    JSONArray array = new JSONArray();
                    JSONArray arrays = new JSONArray();

                    array.put(logData);
//                    JSONObject json = new JSONObject();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsons = array.getJSONObject(i);
                        JSONArray array2 = jsons.names();
                        for (int j = i; j < array2.length(); j++) {
                            JSONObject logs = new JSONObject();
                            logs.put("item", array2.getString(j));
                            logs.put("value", jsons.get(array2.getString(j)));
                            arrays.put(logs);
                        }
                    }
                    
                    JSONObject json2 = new JSONObject();
                    json2.put("data", arrays);
                                        //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
            }
        }catch(Exception e){
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++"+stringWriter.toString());
            
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable context) {
    }
    
}
