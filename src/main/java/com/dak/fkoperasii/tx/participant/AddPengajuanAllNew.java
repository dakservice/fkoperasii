/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MCustomCoa;
import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MRateAsuransi;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MStatusPengajuan;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author arfandiusemahu
 */
public class AddPengajuanAllNew implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();

    @Override
    public int prepare(long l, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(new Date());
        Calendar cal = Calendar.getInstance();
        DecimalFormat df = new DecimalFormat("#");

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            log.info("================= MASUK NEW TRY");
            int total = 0;
            String ajuan_id;
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            log.info("================= MASUK NASABAH " + nsbh.getAktif());
            int jenis = jsonReq.getInt("jenis");
            double jasas = 0;
            String dateParts[] = date.split("-");
            String rBulan = dateParts[1];
            int rTahun = Integer.parseInt(dateParts[0]);
            log.info("================= bulan " + rBulan);
            log.info("================= tahun " + rTahun);
            MJenisPinjaman mjp = SpringInit.getMJenisPinjamanDao().jenisPinjamanByIdStatus(jenis, nsbh.getStatus_pegawai());
            Integer nominal = jsonReq.getInt("nominal");
            Integer lama_angsuran = jsonReq.getInt("lamaangsuran");
            String ket = jsonReq.getString("keterangan");

            if (jenis == 1 || jenis == 5) {
                jasas = Double.parseDouble(SpringInit.getMPinjamanRutinDao().pinjamanRutinByBulanTahun(Integer.parseInt(rBulan), rTahun).toString());
            }
            if (jenis == 2) {
                jasas = Double.parseDouble(SpringInit.getMPinjamanNonRutinDao().pinjamanNonRutinByBulanTahun(Integer.parseInt(rBulan), rTahun).toString());
            }

            if (jenis == 3 || jenis == 8) {
                jasas = Double.parseDouble(SpringInit.getMPinjamanBarangDao().pinjamanBarangByBulanTahun(Integer.parseInt(rBulan), rTahun).toString());
            }

            if (jenis == 4 || jenis == 7) {
                jasas = Double.parseDouble(SpringInit.getMPinjamanMotorDao().pinjamanMotorByBulanTahun(Integer.parseInt(rBulan), rTahun).toString());
            }

            if (jenis == 10) {
                String jasa = SpringInit.getMPinjamanRutinPromoDao().pinjamanRutinPromoByBulanTahunNominal(Integer.parseInt(rBulan), rTahun, nominal).toString();
                log.info("================= Jasa" + jasa);
                log.info("================= Jasa Double" + Double.parseDouble(jasa));
                jasas = Double.parseDouble(jasa);
            }

            if (jenis == 11 || jenis == 12) {
                jasas = Double.parseDouble(SpringInit.getMPinjamanBarangPromoDao().pinjamanBarangPromoByBulanTahun(Integer.parseInt(rBulan), rTahun).toString());
            }

            if (nsbh.getAktif().equals("Y")) {
                if (jenis == 2 && nsbh.getStatus_pegawai().equals("Pegawai Tetap")) {
                    log.info("================ MASUK SIMULASI NON RUTIN");
                    JSONArray skema_bayar = jsonReq.getJSONArray("skema_bayar");
                    Integer sb = skema_bayar.length();
//                    Integer sb = skema_bayar.length() + 1;
                    log.info("======== ISI SB LENGTH " + sb);
                    log.info("======== Bunga " + jasas);
                    Double bunga = jasas;
                    Integer sisbayar = 0;
                    Integer sisa = 0;

                    JSONObject json2 = new JSONObject();

                    JSONArray logSimulasis = new JSONArray();

                    for (int a = 0; a < sb; a++) {

                        JSONObject logSimulasi = new JSONObject();
//                        if(a == 0){
//                            log.info("==== A = "+a);
//                            logSimulasi.put("angsuran_ke",""+ a);
//                            log.info("===== ANGSURAN "+a);
//                            
//                            logSimulasi.put("sisa_angsuran", nominal.toString());
//                            log.info("====== SISA ANGSURAN "+nominal);
//                            logSimulasi.put("total", "0");
//                            logSimulasi.put("pokok", "0");
//                            logSimulasi.put("jasa", bunga);
//                        } else{
                        int b = a - 1;
                        JSONObject skema_bayars = skema_bayar.getJSONObject(a);

                        String bulan = skema_bayars.getString("bulan");
                        Integer nominal_bayar = skema_bayars.getInt("nominal_bayar");

                        log.info("========== ISI BULAN KE " + a + " " + a);
                        log.info("========== ISI NOMINAL BAYAR KE " + a + " " + nominal_bayar);

                        //==========================================================================================================
                        sisa = nominal - sisbayar;
                        double jasa = jasas / 100;
                        log.info("============= AMBIL BUNGA " + jasa);
                        double hasil = 0;
                        double angsuran_jasa, pokok;

                        if (Integer.parseInt(bulan) == 1) {
                            angsuran_jasa = nominal * jasa;
                        } else {
                            angsuran_jasa = sisa * jasa;
                        }

                        pokok = nominal_bayar;
                        hasil = nominal_bayar + angsuran_jasa;

                        logSimulasi.put("angsuran_ke", bulan);
                        logSimulasi.put("pokok", df.format(pokok));
                        log.info("============= POKOK " + df.format(pokok));
                        logSimulasi.put("jasa", df.format(angsuran_jasa));
                        log.info("============= BUNGA " + df.format(angsuran_jasa));
                        logSimulasi.put("total", df.format(hasil));
                        log.info("============= HASIL " + df.format(hasil));
                        logSimulasi.put("sisa_angsuran", df.format(sisa));
                        log.info("============= HASIL " + df.format(sisa));

                        Integer sisas = nominal - nominal_bayar;

                        sisbayar += nominal_bayar;
                        log.info("==== SISBAYAR " + sisbayar);

//                            if (Integer.parseInt(bulan) > 0) {
//
//                                sisa = nominal - sisbayar;
//                                logSimulasi.put("sisa_angsuran", sisa.toString());
//                                log.info("===== SISA "+sisa);
//
//                            }
//                        }
                        logSimulasis.put(logSimulasi);
                    }

                    // =============================================================================    
//                    MPengajuan mj = SpringInit.getMPengajuanDao().pengajuanByAjuanId();
//                    MPengajuan mp = new MPengajuan();
                    JSONObject logData = new JSONObject();
                    MJenisPinjaman mjpinjam = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(jenis);

                    logData.put("Plafon", kursIndonesia.format(nominal).toString());
                    logData.put("Jangka Waktu", lama_angsuran.toString());

                    Double waktu = Double.valueOf(lama_angsuran) / 12;
                    Double wkt = Math.ceil(waktu);
                    log.info("====== HASIL WKT " + wkt);
                    Integer k = Integer.parseInt(df.format(wkt.intValue()));

                    MRateAsuransi mra = SpringInit.getMPresentasePlafonDao().getRateAsuransi(k);
                    logData.put("Rate Asuransi", mra.getPersentase_rate().toString() + "%");

                    Double premi = nominal * mra.getPersentase_rate() / 100;
                    Integer prem = Integer.parseInt(df.format(premi));
                    logData.put("Premi Asuransi", kursIndonesia.format(prem));
                    log.info("====== PREMI ASURANSI " + premi);
                    logData.put("Biaya Polis", kursIndonesia.format(mjpinjam.getBiaya_polis())).toString();

                    MCustomCoa b_materai = SpringInit.getMPresentasePlafonDao().getBiayaMaterai();
                    logData.put("Biaya Materai", kursIndonesia.format(b_materai.getValue_awal()).toString());

                    Integer jlh_biaya = Integer.parseInt(df.format(premi)) + mjpinjam.getBiaya_polis() + b_materai.getValue_awal();
                    logData.put("Jumlah Biaya", kursIndonesia.format(jlh_biaya).toString());
                    log.info("===== JUMLAH BIAYA " + jlh_biaya);

                    Integer b_plafon = mjpinjam.getPlafon() * 45 / 100;
                    Integer b_adm = nominal * 5 / 1000;
                    log.info("===== BIAYA ADM " + b_adm);
                    Integer biaya_adm = 0;
                    if (b_adm < 25000) {
                        biaya_adm = 25000;
                    } else {
                        biaya_adm = b_adm;
                    }

                    logData.put("Biaya Administrasi", kursIndonesia.format(biaya_adm).toString());
                    logData.put("Pinjaman Diterima", kursIndonesia.format(nominal - (jlh_biaya + biaya_adm)).toString());
                    log.info("===== PINJAMAN DITERIMA " + (nominal - (jlh_biaya + biaya_adm)));

                    JSONArray array = new JSONArray();
                    JSONArray arrays = new JSONArray();

                    array.put(logData);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsons = array.getJSONObject(i);
                        JSONArray array2 = jsons.names();
                        for (int j = i; j < array2.length(); j++) {
                            JSONObject logs = new JSONObject();
                            logs.put("item", array2.getString(j));
                            logs.put("value", jsons.get(array2.getString(j)));
                            arrays.put(logs);

                        }
                    }

                    //                    JSONObject json2 = new JSONObject();
                    json2.put("detail_simulasi", arrays);
                    json2.put("log_simulasi", logSimulasis);
                    log.info("=============================== LOG DATA :" + json2);
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);

                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                } else {

                    log.info("================ MASUK SIMULASI RUTIN");
                    MPengajuan mj = SpringInit.getMPengajuanDao().pengajuanByAjuanId();
                    MPengajuan mp = new MPengajuan();
                    int no_aj = 1;
                    if (mj != null) {
                        no_aj = Integer.valueOf(mj.getNo_ajuan()) + 1;
                    }

                    mp.setNo_ajuan("" + no_aj);
                    log.info("================= NO AJUAN " + mp.getNo_ajuan());

                    if (mj != null) {
                        String id_trx = mj.getAjuan_id().substring(12, 13);
                        log.info("======================= GET ID TRANSAKSI " + mj.getAjuan_id());
                        String AN = "" + (Integer.parseInt(id_trx) + 1);
                        log.info("======================= ISI AN " + AN);
                        String Nol = "";
                        if (AN.length() == 1) {
                            Nol = "00";
                        } else if (AN.length() == 2) {
                            Nol = "0";
                        } else if (AN.length() == 3) {
                            Nol = "";
                        }
                        ajuan_id = "PRM.19.08." + Nol + AN;
                    } else {
                        ajuan_id = "PRM.19.08.001";
                    }

                    log.info("======================= ISI AJUAN ID " + ajuan_id);
                    mp.setAjuan_id(ajuan_id);
                    mp.setAnggota_id(Integer.parseInt(nsbh.getId().toString()));
                    mp.setTgl_input(datetime.format(new Date()));
                    mp.setJenis("" + jenis);
                    mp.setNominal(nominal.toString());
                    mp.setLama_ags(lama_angsuran.toString());
                    mp.setKeterangan(ket);
                    mp.setStatus("5");
                    mp.setAlasan("");
                    mp.setTgl_cair(date);
                    mp.setTgl_update(datetime.format(new Date()));
                    mp.setId_barang(4);

                    log.info("=============================== MASUK LOG DATA");
                    JSONObject logData = new JSONObject();
                    logData.put("No Ajuan", mp.getNo_ajuan().toString());
                    logData.put("Ajuan ID", mp.getAjuan_id());
                    logData.put("Anggota ID", mp.getAnggota_id().toString());
                    log.info("=============================== TANGGAL INPUT " + mp.getTgl_input());
                    logData.put("Tanggal Input", mp.getTgl_input().toString());
                    MJenisPinjaman mjpinjam = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(jenis);
                    logData.put("Jenis", mjpinjam.getNama_pinjaman());
                    log.info("=============================== JENIS PINJAMAN " + mjpinjam.getNama_pinjaman());
                    Integer nom = Integer.parseInt(mp.getNominal());
                    logData.put("Nominal", kursIndonesia.format(nom));
                    logData.put("Lama Angsuran", mp.getLama_ags().toString());
                    logData.put("Keterangan", mp.getKeterangan());
                    MStatusPengajuan msp = SpringInit.getMStatusPengajuanDao().statusById(Integer.parseInt(mp.getStatus()));
                    logData.put("Status", msp.getStatus());
                    logData.put("Alasan", mp.getAlasan());
                    logData.put("Tanggal Cair", mp.getTgl_cair().toString());
                    logData.put("Tanggal Update", mp.getTgl_update().toString());
                    logData.put("ID Barang", mp.getId_barang().toString());

                    logData.put("Plafon", kursIndonesia.format(nominal).toString());
                    logData.put("Jangka Waktu", mp.getLama_ags().toString());

                    Double waktu = Double.valueOf(lama_angsuran) / 12;
                    Double wkt = Math.ceil(waktu);
                    log.info("====== HASIL WKT " + wkt);
                    Integer k = Integer.parseInt(df.format(wkt.intValue()));

                    MRateAsuransi mra = SpringInit.getMPresentasePlafonDao().getRateAsuransi(k);
                    logData.put("Rate Asuransi", mra.getPersentase_rate().toString() + "%");

                    Double premi = nominal * mra.getPersentase_rate() / 100;
                    Integer prem = Integer.parseInt(df.format(premi));
                    logData.put("Premi Asuransi", kursIndonesia.format(prem));
                    log.info("====== PREMI ASURANSI " + premi);
                    logData.put("Biaya Polis", kursIndonesia.format(mjpinjam.getBiaya_polis())).toString();

                    MCustomCoa b_materai = SpringInit.getMPresentasePlafonDao().getBiayaMaterai();
                    logData.put("Biaya Materai", kursIndonesia.format(b_materai.getValue_awal()).toString());

                    Integer jlh_biaya = Integer.parseInt(df.format(premi)) + mjpinjam.getBiaya_polis() + b_materai.getValue_awal();
                    logData.put("Jumlah Biaya", kursIndonesia.format(jlh_biaya).toString());
                    log.info("===== JUMLAH BIAYA " + jlh_biaya);

                    Integer b_plafon = mjpinjam.getPlafon() * 45 / 100;
                    Integer b_adm = nominal * 5 / 1000;
                    log.info("===== BIAYA ADM " + b_adm);
                    Integer biaya_adm = 0;
                    if (b_adm < 25000) {
                        biaya_adm = 25000;
                    } else {
                        biaya_adm = b_adm;
                    }

                    logData.put("Biaya Administrasi", kursIndonesia.format(biaya_adm).toString());
                    logData.put("Pinjaman Diterima", kursIndonesia.format(nominal - (jlh_biaya + biaya_adm)).toString());
                    log.info("===== PINJAMAN DITERIMA " + (nominal - (jlh_biaya + biaya_adm)));

                    //==========================================================================================================
                    String status = nsbh.getStatus_pegawai();

                    total = nominal;
                    double pokok_awal = total;
//                    double jasas = mjp.getSuku_bunga();
                    double jasa = jasas / 100;
                    log.info("============= pokok_awal " + df.format(Math.round(pokok_awal)));
                    log.info("============= AMBIL BUNGA " + jasas);
//                    double month = 12;
                    double hasil = 0;
                    double saldo_jasa = total, bunga, total_angsuran = 0, pokok_jasa, e, angsuran_jasa, pokok;

                    pokok_jasa = total / lama_angsuran;

                    for (int i = 0; i < lama_angsuran; i++) {
                        bunga = jasa * saldo_jasa;
                        angsuran_jasa = pokok_jasa + bunga;
                        saldo_jasa -= pokok_jasa;
                        total_angsuran += angsuran_jasa;
                    }

//                    b = 1+jasa/month;
//                    c = Math.pow(b, lama_angsuran);
//                    d = 1/c;
//                    e = 1-d;
                    saldo_jasa = total;
//                    DecimalFormat af = new DecimalFormat("####.###");
                    hasil = total_angsuran / lama_angsuran;
                    log.info("============= pokok_awal " + df.format(Math.round(pokok_awal)));

                    JSONArray logSimulasis = new JSONArray();
                    for (int i = 0; i < lama_angsuran; i++) {
//                        hasil = a / e;
                        angsuran_jasa = saldo_jasa * jasa;
                        pokok = hasil - angsuran_jasa;
                        pokok_awal = Math.round(pokok_awal - pokok);
                        saldo_jasa = saldo_jasa - pokok_jasa;
                        int x = i + 1;

                        JSONObject logSimulasi = new JSONObject();
                        logSimulasi.put("angsuran", x);
                        logSimulasi.put("total", df.format(hasil));
                        log.info("============= HASIL " + df.format(hasil));
                        logSimulasi.put("pokok", df.format(pokok));
                        log.info("============= POKOK " + df.format(pokok));
                        logSimulasi.put("jasa", df.format(angsuran_jasa));
                        log.info("============= BUNGA " + df.format(angsuran_jasa));

                        if (pokok_awal > 0) {
                            logSimulasi.put("sisa_angsuran", df.format(pokok_awal));
                        } else {
                            logSimulasi.put("sisa_angsuran", "0");
                        }
                        logSimulasis.put(logSimulasi);
                    }

                    // =============================================================================  
                    JSONArray array = new JSONArray();
                    JSONArray arrays = new JSONArray();

                    array.put(logData);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsons = array.getJSONObject(i);
                        JSONArray array2 = jsons.names();
                        for (int j = i; j < array2.length(); j++) {
                            JSONObject logs = new JSONObject();
                            logs.put("item", array2.getString(j));
                            logs.put("value", jsons.get(array2.getString(j)));
                            arrays.put(logs);

                        }
                    }

                    JSONObject json2 = new JSONObject();
                    json2.put("detail_simulasi", arrays);
                    json2.put("log_simulasi", logSimulasis);
                    log.info("=============================== LOG DATA :" + json2);
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);

                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                }
            } else {
                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_AUTH_FAILED);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

            }

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());

            ctx.put(Constant.WS.RESPONSE, dr);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
        }
    }

    @Override
    public void abort(long l, Serializable srlzbl) {
    }

}
