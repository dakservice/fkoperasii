/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class NoRoute implements TransactionParticipant{
    Log log = Log.getLog("Q2", getClass().getName());

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        JSONObject reqData = (JSONObject) ctx.get(Constant.REQUEST_BODY);
        MRc rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.SERVICE_PATH_NOT_FOUND);
        ResponseWebServiceContainer defaultResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), reqData);

        ctx.put(Constant.RESPONSE, defaultResponse);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.FAILED);
        
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        JSONObject reqData = (JSONObject) ctx.get(Constant.REQUEST_BODY);
        MRc rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.SERVICE_PATH_NOT_FOUND);
        ResponseWebServiceContainer defaultResponse = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), reqData);

        ctx.put(Constant.RESPONSE, defaultResponse);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.FAILED);
        
    }
}
