/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MJenisSimpan;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class HistorySaldo2 implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    DecimalFormat df = new DecimalFormat("#");
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            String notelp = jsonReq.getString("notelp");
            String strdate = jsonReq.getString("start_date");
            String enddate = jsonReq.getString("end_date");

            log.info("========================= NO TELEPON " + notelp);

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            JSONArray data = new JSONArray();
            log.info("========================= MASUK BARIS JSON ARRAY ");
//            Long anggota_id = Long.parseLong(nsbh.getId());
            log.info("========================= ID ANGGOTA " + nsbh.getId().toString());

            List<Object[]> mt = SpringInit.getMTransSPDao().getHistorySpByDate(nsbh.getId().toString(), strdate, enddate);
            log.info("==== STR DATE " + strdate + " END DATE " + enddate);
            // List<Object[]> mt = SpringInit.getMTransSPDao().getAllByDate();
//            List<MLogTrx> ml = SpringInit.getMLogTrxDao().getHistorySaldo(nsbh.getId().toString());
            List<Object[]> ml = SpringInit.getMLogTrxDao().getHistoryByDate(nsbh.getId().toString(), strdate, enddate);
            log.info("================= SEBELUM LIST. DATA: " + ml.size());
            log.info("========================= Mtransp SIZE " + mt.size());
            log.info("========================= MLogTrx SIZE " + ml.size());
            int sz = ml.size();
            int szmt = mt.size();

            if (szmt > 0 && sz > 0) {
                int za = szmt - 1;
                for (int a = 0; a <= za; a++) {
                    JSONObject logDatas = new JSONObject();
                    logDatas.put("id", mt.get(a)[0]);
                    logDatas.put("tanggal_trx", mt.get(a)[1]);
                    log.info("=============================== MASUK TANGGAL TRX");
                    MJenisSimpan mjs = SpringInit.getMJenisSimpanDao().jenisSimpanById(Integer.parseInt(mt.get(a)[2].toString()));
                    log.info("=============================== MASUK MJS");
                    logDatas.put("jenis_simpanan", mjs.getJns_simpan());
                    log.info("=============================== MASUK JENIS SIMPANAN " + mjs.getJns_simpan());
                    logDatas.put("jumlah", df.format(mt.get(a)[3]));
                    logDatas.put("keterangan", mt.get(a)[4]);

                    data.put(logDatas);
                }

                int z = sz - 1;
                for (int a = 0; a <= z; a++) {
                    JSONObject logData = new JSONObject();
                    logData.put("id", ml.get(a)[0]);
                    logData.put("tanggal_trx", format.format(ml.get(a)[3]));
                    logData.put("jenis_simpanan", "Simpanan Sukarela");
                    logData.put("jumlah", "" + df.format(ml.get(a)[2]));
                    logData.put("keterangan", ml.get(a)[6] + " ke " + notelp);

                    data.put(logData);
                }

                JSONObject json2 = new JSONObject();
                json2.put("data", data);
                log.info("=============================== LOG DATA :" + json2);

                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);

            } else if (szmt > 0 && sz < 1) {
                int z = szmt - 1;
                for (int a = 0; a <= z; a++) {
                    JSONObject logDatas = new JSONObject();
                    logDatas.put("id", mt.get(a)[0]);
                    logDatas.put("tanggal_trx", mt.get(a)[1]);
                    log.info("=============================== MASUK TANGGAL TRX");
                    MJenisSimpan mjs = SpringInit.getMJenisSimpanDao().jenisSimpanById(Integer.parseInt(mt.get(a)[2].toString()));
                    log.info("=============================== MASUK MJS");
                    logDatas.put("jenis_simpanan", mjs.getJns_simpan());
                    log.info("=============================== MASUK JENIS SIMPANAN " + mjs.getJns_simpan());
                    logDatas.put("jumlah", df.format(mt.get(a)[3]));
                    logDatas.put("keterangan", mt.get(a)[4]);

                    data.put(logDatas);

                    JSONObject json2 = new JSONObject();
                    json2.put("data", data);
                    log.info("=============================== LOG DATA :" + json2);
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
                }
            } else if (sz > 0 && szmt < 1) {
//                for (MLogTrx datamlt : ml) {
                int z = sz - 1;
                for (int a = 0; a <= z; a++) {
                    JSONObject logData = new JSONObject();
                    logData.put("id", ml.get(a)[0]);
                    logData.put("tanggal_trx", format.format(ml.get(a)[3]));
                    logData.put("jenis_simpanan", "Simpanan Sukarela");
                    logData.put("jumlah", "" + df.format(ml.get(a)[2]));
                    logData.put("keterangan", ml.get(a)[6] + " ke " + notelp);

                    data.put(logData);

                    JSONObject json2 = new JSONObject();
                    json2.put("data", data);
                    log.info("=============================== LOG DATA :" + json2);
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);
                }
            } else {
                log.info("====================== SIZE KERANJANG " + sz);
                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++" + stringWriter.toString());
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }

}
