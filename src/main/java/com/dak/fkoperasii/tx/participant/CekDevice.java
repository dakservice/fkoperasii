/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MDeviceAnggota;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.ABORTED;
import static org.jpos.transaction.TransactionConstants.NO_JOIN;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author 1star
 */
public class CekDevice implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            String device_id = jsonReq.getString("device_id");
            String noTelp = jsonReq.getString("notelp");

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(noTelp);
            log.info("====================== DATA NSBH " + nsbh);

            if (nsbh == null) {
                log.info("====================== User Tidak Ditemukan");
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_FAILED);
                dr = new ResponseWebServiceContainer(rc.getRc(), "User Tidak Ditemukan");
                ctx.put(Constant.LOGIN.STATUS_LOGIN, "User Tidak Ditemukan");
//                log.info("====================== STATUS LOGIN" +ctx.get(Constant.LOGIN.STATUS_LOGIN, rc));

                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return ABORTED | NO_JOIN;

            } else if (nsbh.getAktif().equals("N")) {

                log.info("====================== USER NOT ACTIVE");

                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);

                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return ABORTED | NO_JOIN;

            } else {
                log.info("---- Cek Device id");

                MDeviceAnggota device = SpringInit.getMDeviceAnggotaDao().finddeviceIdByIdaAnggotaAndDeviceID(device_id, nsbh.getId());

                if (device == null) {

                    log.info("====================== Device Tidak Terdaftar");

                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm("FE");
                    dr = new ResponseWebServiceContainer(rc.getRc(), "Device Tidak Terdaftar");
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);

                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return ABORTED | NO_JOIN;

                }

                if (!Boolean.getBoolean(device.getStatus())) {

                    log.info("====================== Device Tidak Aktif");
                    
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().findRm("FE");
                    dr = new ResponseWebServiceContainer(rc.getRc(), "Device Tidak Aktif,");
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);

                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return ABORTED | NO_JOIN;

                }
                
                    //                mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

//                dr = new ResponseWebServiceContainer("00", "Device Berhasil Ditambahkan, Silahkan Aktifkan Lewat Web");
                rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);

                ctx.put(Constant.USER_DETAIL, nsbh);
//                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return PREPARED | NO_JOIN;

            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
            ctx.put(Constant.WS.RESPONSE, dr);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
            return ABORTED | NO_JOIN;
        }

    }

    @Override
    public void commit(long id, Serializable srlzbl) {

    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

}
