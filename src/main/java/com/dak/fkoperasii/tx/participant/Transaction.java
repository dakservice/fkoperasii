/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MJurnal;
import com.dak.fkoperasii.entity.MKas;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MLogTrx;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MProduct;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class Transaction implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    String notelp, nama_produk, type, merk, kategori, idtrx;
    int saldo, harga, sisa_saldo;
    int penarikan = 0;
    int setoran = 0;
    int transaksi = 0;

    @Override
    public int prepare(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(new Date());
        DecimalFormat df = new DecimalFormat("#");
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {
            log.info("======================= MASUK TRY");
            Long id_barang = jsonReq.getLong("id_produk");
            log.info("======================= ID PRODUK " + id_barang);

            MProduct mp = SpringInit.getMProductDao().barangById(id_barang);

            log.info("======================= MASUK MP " + mp);
            notelp = jsonReq.getString("notelp");
            nama_produk = mp.getNm_barang();
            type = mp.getType();
            merk = mp.getMerk();
            harga = mp.getHarga();
            kategori = mp.getKategori();

            log.info("======================= MASUK MNASABAH " + mp);
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            if (nsbh == null) {
                log.info("====================== DATA" + nsbh);

                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.DATA_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            } else {

                Integer saldo_sukarela = SpringInit.getmNasabahDao().getSaldoSukarelas(nsbh.getId().toString());
                MLogTrx log_trx = new MLogTrx();

                log_trx.setId_logtrx(id);
                log_trx.setCreated_time(new Date());
                log_trx.setDescription("KREDIT Rp." + harga);
                log_trx.setInfo("Beli Pulsa " + nama_produk);
                log_trx.setStatus("0");
                log_trx.setTransaction_type("Purchase Pulsa");
                log_trx.setUser_created("-");
                log_trx.setUser_updated("-");
                log_trx.setId_order("-");
                log_trx.setAnggota_id(nsbh.getId().toString());
                log_trx.setTotal_fee(0);
                log_trx.setNominal(harga);
                log_trx.setReff("-");
                log_trx.setRrn("-");
                log_trx.setStan("-");
                log_trx.setType("Purchase Pulsa");
                log_trx.setLevel_setby("-");

                log_trx.setId_product(mp);
                log_trx.setAmount(harga);

//            log_trx.setResponseMsg(rc.getRm());
//            log_trx.setRc(rc.getRc());
                log_trx = SpringInit.getMLogTrxDao().saveOrUpdate(log_trx);
                log.info("======================= SUKSES SIMPAN LOG ");

                MJurnal mj = SpringInit.getMJurnalDao().jurnalByIdtrx();
                log.info("======================= MASUK MJURNAL ");
                MJurnal jurnal = new MJurnal();

                log.info("======================= SEBELUM MASUK IF " + mj);
                if (mj != null) {
                    String id_trx = mj.getId_transaksi().substring(4, 7);
                    log.info("======================= GET ID TRANSAKSI " + mj.getId_transaksi());
                    String AN = "" + (Integer.parseInt(id_trx) + 1);
                    log.info("======================= ISI AN " + AN);
                    String Nol = "";
                    if (AN.length() == 1) {
                        Nol = "000";
                    } else if (AN.length() == 2) {
                        Nol = "00";
                    } else if (AN.length() == 3) {
                        Nol = "0";
                    } else if (AN.length() == 4) {
                        Nol = "";
                    }

                    idtrx = "TRM" + Nol + AN;

                } else {
                    idtrx = "TRM0001";
                }
                log.info("======================== ID TRANSAKSI " + idtrx);

                jurnal.setId_transaksi(idtrx);
                log.info("======================== MASUK ID TRANSAKSI ");
                jurnal.setOutlet_id(0);
                log.info("======================== MASUK OUTLET ID ");
                jurnal.setTgl_jurnal(date);
                log.info("======================== MASUK TANGGAL " + date);
                MKas mk = SpringInit.getMJurnalDao().cekKas();
                log.info("======================== MASUK MKAS");
                jurnal.setKd_akun(mk.getId_coa().toString());
                log.info("======================== MASUK KODE COA " + mk.getId_coa().toString());
                jurnal.setPosisi(Constant.TRX.DEBET);
                log.info("======================== MASUK POSISI");
                double nom = new Double(df.format(harga));
                jurnal.setNominal(nom);
                log.info("======================== MASUK NOMINAL " + nom);
                jurnal.setKeterangan("Beli Pulsa");

                jurnal = SpringInit.getMJurnalDao().saveOrUpdate(jurnal);
                log.info("======================= SIMPAN JURNAL PERTAMA ");
//==============================================================================

                jurnal = new MJurnal();
                jurnal.setId_transaksi(idtrx);
                log.info("======================== MASUK ID TRANSAKSI 2");
                jurnal.setOutlet_id(0);
                log.info("======================== MASUK OUTLET ID 2");
                jurnal.setTgl_jurnal(date);
                log.info("======================== MASUK TANGGAL 2 ");
                Integer mc = SpringInit.getMJurnalDao().cekCoa();
                log.info("======================== MASUK MCOA " + mc);
                log.info("======================== MASUK MCOA");
                jurnal.setKd_akun(mc.toString());
                log.info("======================== MASUK KODE AKUN COA");
                jurnal.setPosisi(Constant.TRX.KREDIT);
                log.info("======================== MASUK KREDIT");
                jurnal.setNominal(nom);
                log.info("======================== MASUK NOMINAL 2");
                jurnal.setKeterangan("Beli Pulsa");
                log.info("======================== MASUK KETERANGAN 2");

                jurnal = SpringInit.getMJurnalDao().saveOrUpdate(jurnal);
                log.info("======================= SIMPAN JURNAL KEDUA ");

                jurnal = new MJurnal();
                jurnal.setId_transaksi(idtrx);
                log.info("======================== MASUK ID TRANSAKSI 3");
                jurnal.setOutlet_id(0);
                log.info("======================== MASUK OUTLET ID ");
                jurnal.setTgl_jurnal(date);
                log.info("======================== MASUK TANGGAL 3");
                Integer mc_ = SpringInit.getMJurnalDao().cekCoaSukarela();
                log.info("======================== MASUK MCOA " + mc);
                log.info("======================== MASUK MCOA");
                jurnal.setKd_akun(mc_.toString());
                log.info("======================== MASUK KODE AKUN COA");
                jurnal.setPosisi(Constant.TRX.DEBET);
                log.info("======================== MASUK KREDIT");
                jurnal.setNominal(nom);
                log.info("======================== MASUK NOMINAL 3");
                jurnal.setKeterangan("Beli Pulsa");
                log.info("======================== MASUK KETERANGAN 3");

                jurnal = SpringInit.getMJurnalDao().saveOrUpdate(jurnal);
                log.info("======================= SIMPAN JURNAL KETIGA ");

                jurnal = new MJurnal();
                jurnal.setId_transaksi(idtrx);
                log.info("======================== MASUK ID TRANSAKSI ");
                jurnal.setOutlet_id(0);
                log.info("======================== MASUK OUTLET ID ");
                jurnal.setTgl_jurnal(date);
                log.info("======================== MASUK TANGGAL " + date);
                MKas mk_ = SpringInit.getMJurnalDao().cekKas();
                log.info("======================== MASUK MKAS");
                jurnal.setKd_akun(mk_.getId_coa().toString());
                log.info("======================== MASUK KODE COA " + mk.getId_coa().toString());
                jurnal.setPosisi(Constant.TRX.KREDIT);
                log.info("======================== MASUK POSISI");
                jurnal.setNominal(nom);
                log.info("======================== MASUK NOMINAL " + nom);
                jurnal.setKeterangan("Beli Pulsa");

                jurnal = SpringInit.getMJurnalDao().saveOrUpdate(jurnal);
                log.info("======================= SIMPAN JURNAL KEEMPAT ");

                Integer sisa_saldo = saldo_sukarela - harga;
                if (sisa_saldo < 0) {
                    sisa_saldo = 0;
                } else {
                    sisa_saldo = sisa_saldo;
                }
                SpringInit.getmNasabahDao().updateSaldoSukarela(Integer.parseInt(nsbh.getId().toString()), sisa_saldo);
                log.info("======================= BERHASIL UPDATE SALDO");

                //                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                ctx.put(Constant.USER_DETAIL, nsbh);
                ctx.put(Constant.TRX.HARGA, harga);
            }

            return PREPARED | NO_JOIN;

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);

            ctx.put(Constant.WS.RESPONSE, dr);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

            return ABORTED | NO_JOIN;
        }
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
//        Context ctx = (Context) srlzbl;
//        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        String date = format.format(new Date());
//        DecimalFormat df = new DecimalFormat("#");
//        
//        try {
//            log.info("======================= MASUK TRY");
//            Long id_barang = jsonReq.getLong("id_produk");
//            log.info("======================= ID PRODUK "+id_barang);
//            
//            MProduct mp = SpringInit.getMProductDao().barangById(id_barang);
//            
//            log.info("======================= MASUK MP "+mp);
//            notelp = jsonReq.getString("notelp");
//            nama_produk = mp.getNm_barang();
//            type = mp.getType();
//            merk = mp.getMerk();
//            harga = mp.getHarga();
//            kategori = mp.getKategori();
//            
//            log.info("======================= MASUK MNASABAH "+mp);
//            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
//            MLogTrx log_trx = new MLogTrx();
//            
//            log_trx.setId_logtrx(id);
//            log_trx.setCreated_time(new Date());
//            log_trx.setDescription("KREDIT Rp."+harga);
//            log_trx.setInfo("Beli Pulsa "+nama_produk);
//            log_trx.setStatus("0");
//            log_trx.setTransaction_type("Purchase Pulsa");
//            log_trx.setUser_created("-");
//            log_trx.setUser_updated("-");
//            log_trx.setId_order("-");
//            log_trx.setAnggota_id(nsbh.getId().toString());
//            log_trx.setTotal_fee(0);
//            log_trx.setNominal(harga);
//            log_trx.setReff("-");
//            log_trx.setRrn("-");
//            log_trx.setStan("-");
//            log_trx.setType("Purchase Pulsa");
//            log_trx.setLevel_setby("-");
//            
//            log_trx.setId_product(mp);
//            log_trx.setAmount(harga);
//
//            rc = SpringInit.getmRcDao().findRm(Constant.WS.STATUS.SUCCESS);
//            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
//            
//            log_trx.setResponseMsg(rc.getRm());
//            log_trx.setRc(rc.getRc());
//            
//            log_trx = SpringInit.getMLogTrxDao().saveOrUpdate(log_trx);
//            log.info("======================= SUKSES SIMPAN LOG ");
//            
//            
//            MJurnal mj = SpringInit.getMJurnalDao().jurnalByIdtrx();
//            log.info("======================= MASUK MJURNAL ");
//            MJurnal jurnal = new MJurnal();
//            
//            log.info("======================= SEBELUM MASUK IF "+mj);
//            if(mj != null){
//                String id_trx = mj.getId_transaksi().substring(1);
//                log.info("======================= GET ID TRANSAKSI "+id_trx);
//                String AN = "" + (Integer.parseInt(id_trx) + 1);
//                String Nol = "";
//                    if(AN.length()==1){
//                        Nol = "000";
//                    }
//                    else if(AN.length()==2){
//                        Nol = "00";
//                    }
//                    else if(AN.length()==3){
//                        Nol = "0";
//                    }
//                    else if(AN.length()==4){
//                        Nol = "";
//                    }
//
//                idtrx = "TRM" + Nol + AN;
//                
//            }else{
//                idtrx = "TRM0001";
//            }
//            log.info("======================== ID TRANSAKSI "+idtrx);
//            
//            jurnal.setId_transaksi(idtrx);
//            log.info("======================== MASUK ID TRANSAKSI ");
//            jurnal.setTgl_jurnal(date);
//            log.info("======================== MASUK TANGGAL "+date);
//            MKas mk = SpringInit.getMJurnalDao().cekKas();
//            log.info("======================== MASUK MKAS");
//            jurnal.setKd_akun(mk.getId_coa().toString());
//            log.info("======================== MASUK KODE COA "+mk.getId_coa().toString());
//            jurnal.setPosisi(Constant.TRX.DEBET);
//            log.info("======================== MASUK POSISI");
//            double nom = new Double(df.format(harga));
//            jurnal.setNominal(nom);
//            log.info("======================== MASUK NOMINAL " +nom);
//            jurnal.setKeterangan("Beli Pulsa");
//            
//            jurnal = SpringInit.getMJurnalDao().saveOrUpdate(jurnal); 
//            log.info("======================= SIMPAN JURNAL PERTAMA ");
////==============================================================================
//           
//           
//            jurnal.setId_transaksi(idtrx);
//            log.info("======================== MASUK ID TRANSAKSI 2");
//            jurnal.setTgl_jurnal(date);
//            log.info("======================== MASUK TANGGAL 2 ");
//            MCoa mc = SpringInit.getMJurnalDao().cekCoa();
//            log.info("======================== MASUK MCOA "+mc);
//            log.info("======================== MASUK MCOA");
//            jurnal.setKd_akun(mc.getId_coa().toString());
//            log.info("======================== MASUK KODE AKUN COA");
//            jurnal.setPosisi(Constant.TRX.KREDIT);
//            log.info("======================== MASUK KREDIT");
//            jurnal.setNominal(nom);
//            log.info("======================== MASUK NOMINAL 2");
//            jurnal.setKeterangan("Beli Pulsa");
//            log.info("======================== MASUK KETERANGAN 2");
//            jurnal = SpringInit.getMJurnalDao().saveOrUpdate(jurnal); 
//            
//            jurnal = SpringInit.getMJurnalDao().saveOrUpdate(jurnal);
//            log.info("======================= SIMPAN JURNAL KEDUA ");
//
//                
//  
//        } catch (Exception e) {
//            StringWriter stringWriter = new StringWriter();
//            PrintWriter printWriter = new PrintWriter(stringWriter);
//            e.printStackTrace(printWriter);
//            log.error("+++++++++++++++++++++++++++++++++++++++++++++"+stringWriter.toString());
//            
//            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
//            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
//        }
//        
//        ctx.put(Constant.WS.RESPONSE, dr);
//        ctx.put(Constant.TRX.RC, rc);
//        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }

}
