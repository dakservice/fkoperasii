/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.AbortParticipant;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.util.Log;

/**
 *
 * @author Morten Jonathan
 */
public class SendResponse implements AbortParticipant{
    
    Log log = Log.getLog("Q2", getClass().getName());
    
    @Override
    public int prepareForAbort(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MLogMobile activity = (MLogMobile) ctx.get(Constant.REQUEST);
        ResponseWebServiceContainer responseWebServiceContainer = (ResponseWebServiceContainer) ctx.get(Constant.RESPONSE);
        Space space = SpaceFactory.getSpace();

        log.info("Request Id " + activity.getId());
        log.info("Response " + responseWebServiceContainer.jsonToString());

        space.out(activity.getId(), ctx, Constant.SETTING.SETTING_VALUE.TIME_OUT);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        MLogMobile activity = (MLogMobile) ctx.get(Constant.REQUEST);
        ResponseWebServiceContainer responseWebServiceContainer = (ResponseWebServiceContainer) ctx.get(Constant.RESPONSE);
        Space space = SpaceFactory.getSpace();

        log.info("Request Id " + activity.getId());
        log.info("Response " + responseWebServiceContainer.jsonToString());

        space.out(activity.getId(), ctx, Constant.SETTING.SETTING_VALUE.TIME_OUT);    }
}