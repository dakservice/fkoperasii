/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class ViewLogSimulasi implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    @Override
    public int prepare(long id, Serializable context) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
    
    Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MPengajuan mp = (MPengajuan) ctx.get(Constant.DATA_PENGAJUAN.toString());
        log.info("=============================== ISI DATA PENGAJUAL "+ctx.get(Constant.DATA_PENGAJUAN));
        
        try{
        MPengajuan mpj = new MPengajuan();
            mpj.setNo_ajuan(mp.getNo_ajuan());
            mpj.setAjuan_id(mp.getAjuan_id());
            mpj.setAnggota_id(mp.getAnggota_id());
            log.info("=============================== TANGGAL INPUT "+mp.getTgl_input());
            mpj.setTgl_input(mp.getTgl_input().toString());
            mpj.setJenis(mp.getJenis());
            mpj.setNominal(mp.getNominal().toString());
            mpj.setLama_ags(mp.getLama_ags().toString());
            mpj.setKeterangan(mp.getKeterangan());
            mpj.setStatus(mp.getStatus());
            mpj.setAlasan(mp.getAlasan());
            mpj.setTgl_cair(mp.getTgl_cair().toString());
            mpj.setTgl_update(mp.getTgl_update().toString());
            
            
            log.info("=============================== MASUK LOG DATA");
            JSONObject logData = new JSONObject();
            logData.put("no_ajuan", mp.getNo_ajuan().toString());
            logData.put("ajuan_id", mp.getAjuan_id());
            logData.put("anggota_id", mp.getAnggota_id().toString());
            log.info("=============================== TANGGAL INPUT "+mp.getTgl_input());
            logData.put("tanggal_input", mp.getTgl_input().toString());
            logData.put("jenis", mp.getJenis());
            logData.put("nominal", mp.getNominal().toString());
            logData.put("lama_angsuran", mp.getLama_ags().toString());
            logData.put("keterangan", mp.getKeterangan());
            logData.put("status", mp.getStatus());
            logData.put("alasan", mp.getAlasan());
            logData.put("tanggal_cair", mp.getTgl_cair().toString());
            logData.put("tanggal_update",mp.getTgl_update().toString());
            
        }catch(Exception e){
            
        }
    
    }

    @Override
    public void abort(long id, Serializable context) {
    }
    
}
