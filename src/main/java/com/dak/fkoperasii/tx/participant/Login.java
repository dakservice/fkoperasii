/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MDeviceAnggota;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MLogTrx;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MOtp;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MTransSP;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.Function;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import com.dak.fkoperasii.utility.Tripledes;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;
import java.util.Random;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.jpos.transaction.Context;
import static org.jpos.transaction.TransactionConstants.PREPARED;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class Login implements TransactionParticipant {

//    private Configuration cfg;
    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);
        try {
            String device_id = jsonReq.getString("device_id");
            String device_name = jsonReq.getString("device_name");
//            Tripledes tdes = new Tripledes(Constant.SETTING.TRIDES_MOB_ENCRYPT);
            String notelp = jsonReq.getString("notelp");
            String pass = "nsi" + jsonReq.getString("password");
            String password = SpringInit.getmNasabahDao().encryptPasswordSHA1(pass);
            System.out.println("No Telepon " + notelp);
            log.info("========================= NO TELEPON " + notelp);
            log.info("========================= PASSWORD " + password);

            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            log.info("====================== DATA NSBH " + nsbh);

            if (nsbh == null) {
                log.info("====================== User Tidak Ditemukan");
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_FAILED);
                dr = new ResponseWebServiceContainer(rc.getRc(), "User Tidak Ditemukan");
                ctx.put(Constant.LOGIN.STATUS_LOGIN, "User Tidak Ditemukan");
//                log.info("====================== STATUS LOGIN" +ctx.get(Constant.LOGIN.STATUS_LOGIN, rc));

                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return ABORTED | NO_JOIN;

            } else if (nsbh.getAktif().equals("N")) {
                
                log.info("====================== USER NOT ACTIVE");

                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
//                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return ABORTED | NO_JOIN;

            } else {

                String enPass = encryptPasswordSHA1(pass);

//                log.info("enpas :" + enPass + "\n" + "pass " + nsbh.getPass_word());
                if (!enPass.equals(nsbh.getPass_word())) {

                    log.info("====================== Passwword Salah");
                    log.info("id user : " + nsbh.getId());
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), "Passwword Anda Salah");
                      //                            mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return ABORTED | NO_JOIN;

                }
                MDeviceAnggota devicecek = SpringInit.getMDeviceAnggotaDao().finddeviceIdByIdaAnggota(nsbh.getId());

                if (devicecek == null) {

                    log.info("====================== Login Pertama Kali");
                    log.info("id user : " + nsbh.getId());
                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer("2803", "Login Pertama Kali");
                      //                            mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    ctx.put(Constant.LOGIN.STATUS_LOGIN, 2803);
//                    ctx.put("nasabah", nsbh);
                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return ABORTED | NO_JOIN;

                }
                MDeviceAnggota device = SpringInit.getMDeviceAnggotaDao().finddeviceIdBydeviceId(device_id);

//                log.info("Device ID: " + device.getDevice_id());
//                log.info("Device ID: " + Boolean.parseBoolean(device.getStatus()));
                if (device == null) {

                    log.info("====================== User Anda Login Di Device Lain");
//                    log.info("id user : " + nsbh.getId());
//                    log.info("id device : " + device.getDevice_id());
//                    log.info("id device dari user: " + device_id);
//                    MDeviceAnggota newDevice = new MDeviceAnggota();
//                    newDevice.setDevice_id(device_id);
//                    newDevice.setDevices_name(device_name);
//                    newDevice.setIdAnggota(nsbh.getId());
//                    newDevice.setStatus("false");
//                    SpringInit.getMDeviceAnggotaDao().saveOrUpdate(newDevice);
                    Random rand = new Random();
                    int rand_int1 = rand.nextInt(1000000);
                    String otp = Integer.toString(rand_int1);
                    System.out.println("otp : " + otp);

                    MOtp mOtp = new MOtp();
                    mOtp.setOtp(otp);
                    mOtp.setType("otp_new_device");
                    mOtp.setStatus("aktif");
                    mOtp.setNoTelp(notelp);
                    SpringInit.getmOtpDao().saveOrUpdate(mOtp);
                    final String message = SpringInit.getmSettingDao().getSettingByParam("otp_wa_template").getValue().replace("#otp",otp);
                    Function.sendWA(notelp,message);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer("2304", "User Anda Login Di Device Lain");
                    ctx.put(Constant.LOGIN.STATUS_LOGIN, 2304);
//                    ctx.put("nasabah", nsbh);
                      //                            mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return ABORTED | NO_JOIN;

                }
                if (!Boolean.parseBoolean(device.getStatus())) {

                    log.info("====================== Device Anda Belum Aktif");
                    log.info("id user : " + nsbh.getId());
                    log.info("id device : " + device.getDevice_id());
                    log.info("id device dari user: " + device_id);

                    rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer("2910", "Device Anda Belum Aktif");
                      //                            mlogmobile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    ctx.put(Constant.LOGIN.STATUS_LOGIN, 2910);
//                    ctx.put("nasabah", nsbh);
                    ctx.put(Constant.WS.RESPONSE, dr);
                    ctx.put(Constant.TRX.RC, rc);
                    ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                    return ABORTED | NO_JOIN;

                }
                log.info("====================== LOGIN SUKSES");
                JSONObject json = new JSONObject();
                json.put("username", nsbh.getNotelp());
                rc = SpringInit.getmRcDao().findRm(Constant.WS.RC.LOGIN_SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());

                JSONObject json2 = new JSONObject();
                json2.put("data", json);
//                mlogmobile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);
                ctx.put(Constant.USER_DETAIL, nsbh);
                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

                return PREPARED | NO_JOIN;

            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
//            mACtivity.setIdUser(nsbh.getId());
//            ctx.put(Constant.WS.REQUEST, mACtivity);

            ctx.put(Constant.WS.RESPONSE, dr);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
            return ABORTED | NO_JOIN;
        }

    }

    @Override
    public void commit(long id, Serializable srlzbl) {

    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

    public String encryptPasswordSHA1(String pass_word) {

        String encPass = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA1");
            digest.update(pass_word.getBytes(), 0, pass_word.length());
            encPass = new BigInteger(1, digest.digest()).toString(16);
            String vsha1 = encPass.toLowerCase();
            return vsha1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encPass;
    }
}
