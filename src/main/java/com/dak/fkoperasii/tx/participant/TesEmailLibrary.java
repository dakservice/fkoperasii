package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.Function;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONObject;

import java.io.Serializable;

public class TesEmailLibrary implements TransactionParticipant {
    @Override
    public int prepare(long l, Serializable serializable) {
        return PREPARED;
    }

    @Override
    public void commit(long l, Serializable serializable) {
        MRc rc;
        Context ctx = (Context) serializable;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        Function.sendEmailLib(jsonReq.getString("email"),"<h3 align=\"Center\">RESET PASSWORD</h3>\n" +
                "<br>\n" +
                "<p align='center'>Berikut OTP change password anda, masa aktif OTP <b>30 menit</b>, silahkan masukan OTP berikut pada aplikasi mobile Koperasi\n" +
                "</p>\n" +
                "<br>\n" +
                "  <p><h5 align='center'>OTP : #otp<h5> \n","Testing");
        rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
        ResponseWebServiceContainer dr = new ResponseWebServiceContainer("00","Success");
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long l, Serializable serializable) {

    }
}
