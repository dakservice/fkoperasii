/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.participant;

import com.dak.fkoperasii.entity.MCustomCoa;
import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MRateAsuransi;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MStatusPengajuan;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class AddPengajuan implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc = new MRc();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;

    }

    @Override
    public void commit(long id, Serializable srlzbl) {

        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(new Date());
        Calendar cal = Calendar.getInstance();
        DecimalFormat df = new DecimalFormat("#");

        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {

            log.info("================= MASUK TRY ");
            int total = 0;
            String ajuan_id;
            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);
            log.info("================= MASUK NASABAH " + nsbh.getAktif());
            int jenis = jsonReq.getInt("jenis");
            MJenisPinjaman mjp = SpringInit.getMJenisPinjamanDao().jenisPinjamanByIdStatus(jenis, nsbh.getStatus_pegawai());
            Integer nominal = jsonReq.getInt("nominal");
            Integer lama_angsuran = jsonReq.getInt("lamaangsuran");
            String ket = jsonReq.getString("keterangan");

            if (nsbh.getAktif().equals("Y")) {

                log.info("================= MASUK IF ");
                MPengajuan mj = SpringInit.getMPengajuanDao().pengajuanByAjuanId();
                MPengajuan mp = new MPengajuan();
//                    mp.setId(Long.parseLong(nsbh.getId().toString()));
                int no_aj = Integer.valueOf(mj.getNo_ajuan()) + 1;

                mp.setNo_ajuan("" + no_aj);
                log.info("================= NO AJUAN " + mp.getNo_ajuan());

                if (mj != null) {
                    String id_trx = mj.getAjuan_id().substring(12, 13);
                    log.info("======================= GET ID TRANSAKSI " + mj.getAjuan_id());
                    String AN = "" + (Integer.parseInt(id_trx) + 1);
                    log.info("======================= ISI AN " + AN);
                    String Nol = "";
                    if (AN.length() == 1) {
                        Nol = "00";
                    } else if (AN.length() == 2) {
                        Nol = "0";
                    } else if (AN.length() == 3) {
                        Nol = "";
                    }
                    ajuan_id = "PRM.19.08." + Nol + AN;
                } else {
                    ajuan_id = "PRM.19.08.001";
                }

                log.info("======================= ISI AJUAN ID " + ajuan_id);
                mp.setAjuan_id(ajuan_id);
                mp.setAnggota_id(Integer.parseInt(nsbh.getId().toString()));
                mp.setTgl_input(datetime.format(new Date()));
                mp.setJenis("" + jenis);
                mp.setNominal(nominal.toString());
                mp.setLama_ags(lama_angsuran.toString());
                mp.setKeterangan(ket);
                mp.setStatus("0");
                mp.setAlasan("");
                mp.setTgl_cair(date);
                mp.setTgl_update(datetime.format(new Date()));
                mp.setId_barang(4);

                log.info("=============================== MASUK LOG DATA");
                JSONObject logData = new JSONObject();
                logData.put("No Ajuan", mp.getNo_ajuan().toString());
                logData.put("Ajuan ID", mp.getAjuan_id());
                logData.put("Anggota ID", mp.getAnggota_id().toString());
                log.info("=============================== TANGGAL INPUT " + mp.getTgl_input());
                logData.put("Tanggal Input", mp.getTgl_input().toString());
                MJenisPinjaman mjpinjam = SpringInit.getMJenisPinjamanDao().jenisPinjamanById(jenis);
                logData.put("Jenis", mjpinjam.getNama_pinjaman());
                log.info("=============================== JENIS PINJAMAN " + mjpinjam.getNama_pinjaman());
                Integer nom = Integer.parseInt(mp.getNominal());
                logData.put("Nominal", kursIndonesia.format(nom));
                logData.put("Lama Angsuran", mp.getLama_ags().toString());
                logData.put("Keterangan", mp.getKeterangan());
                MStatusPengajuan msp = SpringInit.getMStatusPengajuanDao().statusById(Integer.parseInt(mp.getStatus()));
                logData.put("Status", msp.getStatus());
                logData.put("Alasan", mp.getAlasan());
                logData.put("Tanggal Cair", mp.getTgl_cair().toString());
                logData.put("Tanggal Update", mp.getTgl_update().toString());
                logData.put("ID Barang", mp.getId_barang().toString());

                logData.put("Plafon", kursIndonesia.format(nominal).toString());
                logData.put("Jangka Waktu", mp.getLama_ags().toString());

                Double waktu = Double.valueOf(lama_angsuran) / 12;
                Double wkt = Math.ceil(waktu);
                log.info("====== HASIL WKT " + wkt);
                Integer k = Integer.parseInt(df.format(wkt.intValue()));

                MRateAsuransi mra = SpringInit.getMPresentasePlafonDao().getRateAsuransi(k);
                logData.put("Rate Asuransi", mra.getPersentase_rate().toString() + "%");

                Double premi = nominal * mra.getPersentase_rate() / 100;
                Integer prem = Integer.parseInt(df.format(premi));
                logData.put("Premi Asuransi", kursIndonesia.format(prem));
                log.info("====== PREMI ASURANSI " + premi);
                logData.put("Biaya Polis", kursIndonesia.format(mjpinjam.getBiaya_polis())).toString();

                MCustomCoa b_materai = SpringInit.getMPresentasePlafonDao().getBiayaMaterai();
                logData.put("Biaya Materai", kursIndonesia.format(b_materai.getValue_awal()).toString());

                Integer jlh_biaya = Integer.parseInt(df.format(premi)) + mjpinjam.getBiaya_polis() + b_materai.getValue_awal();
                logData.put("Jumlah Biaya", kursIndonesia.format(jlh_biaya).toString());
                log.info("===== JUMLAH BIAYA " + jlh_biaya);

                Integer b_plafon = mjpinjam.getPlafon() * 45 / 100;
                Integer b_adm = nominal * 5 / 1000;
                log.info("===== BIAYA ADM " + b_adm);
                Integer biaya_adm = 0;
                if (b_adm < 25000) {
                    biaya_adm = 25000;
                } else {
                    biaya_adm = b_adm;
                }

                logData.put("Biaya Administrasi", kursIndonesia.format(biaya_adm).toString());
                logData.put("Pinjaman Diterima", kursIndonesia.format(nominal - (jlh_biaya + biaya_adm)).toString());
                log.info("===== PINJAMAN DITERIMA " + (nominal - (jlh_biaya + biaya_adm)));

//==========================================================================================================
                String status = nsbh.getStatus_pegawai();

                total = nominal;
                double pokok_awal = total;
                double jasas = mjp.getSuku_bunga();
                double jasa = jasas / 100;
                log.info("============= AMBIL BUNGA " + mjp.getSuku_bunga());
                double month = 12;
                double hasil = 0;
                double a, b, c, d, e, angsuran_jasa, pokok;

//                DecimalFormat df = new DecimalFormat("#");
                a = pokok_awal * (jasa / month);
                b = 1 + jasa / month;
                c = Math.pow(b, lama_angsuran);
                d = 1 / c;
                e = 1 - d;

                JSONArray logSimulasis = new JSONArray();
                for (int i = 0; i < lama_angsuran; i++) {
                    hasil = a / e;
                    angsuran_jasa = pokok_awal * (jasa / month);
                    pokok = hasil - angsuran_jasa;
                    pokok_awal = pokok_awal - pokok;
                    int x = i + 1;

                    JSONObject logSimulasi = new JSONObject();
                    logSimulasi.put("angsuran", x);
                    logSimulasi.put("total", df.format(hasil));
                    log.info("============= HASIL " + df.format(hasil));
                    logSimulasi.put("pokok", df.format(pokok));
                    log.info("============= POKOK " + df.format(pokok));
                    logSimulasi.put("jasa", df.format(angsuran_jasa));
                    log.info("============= BUNGA " + df.format(angsuran_jasa));

                    if (pokok_awal > 0) {
                        logSimulasi.put("sisa_angsuran", df.format(Math.round(pokok_awal)));
                    } else {
                        logSimulasi.put("sisa_angsuran", "0");
                    }
                    logSimulasis.put(logSimulasi);
                }

// =============================================================================  
//                    MJenisPinjaman mjp = SpringInit.getMJenisPinjamanDao().jenisPinjamanByStatus(mp.getJenis(), nsbh.getStatus_pegawai());
//                    JSONArray logSimulasis = new JSONArray();
//                    log.info("=============================== MASUK SIMULASI");
//                    int angsuran_pokok, biaya_jasa, biaya_admin, jml_tagihan;
////                    int suku_jasa = mjp.getSuku_jasa();
//                    Date tgl_tempo;
//                    
//                    
//                    
//                    for(int i = 0; i < lama_angsuran; i++){
//                        log.info("=============================== MASUK FOR SIMULASI");
//                        log.info("=============================== SUKU BUNGA "+mjp.getSuku_jasa());
//                        angsuran_pokok = nominal / lama_angsuran;
//                        biaya_jasa= nominal * mjp.getSuku_jasa();
//                        biaya_admin = 1500;
//                        jml_tagihan = angsuran_pokok + biaya_jasa + biaya_admin;
//                        cal.add(Calendar.DAY_OF_MONTH, 30);
//                        tgl_tempo = cal.getTime();
//                        log.info("=========================== TANGGAL TEMPO "+format.format(tgl_tempo));
//                        
//                        JSONObject logSimulasi = new JSONObject();
//                        logSimulasi.put("angsuran", i);
//                        logSimulasi.put("tgl_tempo", format.format(tgl_tempo));
//                        logSimulasi.put("angsuran_pokok", angsuran_pokok);
//                        logSimulasi.put("biaya_jasa", biaya_jasa);
//                        logSimulasi.put("biaya_admin", biaya_admin);
//                        logSimulasi.put("jml_tagihan", jml_tagihan);
//                        
//                        logSimulasis.put(logSimulasi);
//                    }
                JSONArray array = new JSONArray();
                JSONArray arrays = new JSONArray();

                array.put(logData);
//                    JSONObject json = new JSONObject();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsons = array.getJSONObject(i);
                    JSONArray array2 = jsons.names();
                    for (int j = i; j < array2.length(); j++) {
                        JSONObject logs = new JSONObject();
                        logs.put("item", array2.getString(j));
                        logs.put("value", jsons.get(array2.getString(j)));
                        arrays.put(logs);

                    }
                }

                JSONObject json2 = new JSONObject();
                json2.put("detail_simulasi", arrays);
                json2.put("log_simulasi", logSimulasis);
                log.info("=============================== LOG DATA :" + json2);

//                    log.info("================= MASUK MAU SAVE ");
//                    mp = SpringInit.getMPengajuanDao().saveOrUpdate(mp);
//                    ctx.put(Constant.DATA_PENGAJUAN, mp);
//                    ctx.put(Constant.LOG_SIMULASI, logSimulasis);
//                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);
                rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json2);

                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

            } else {

//                mlogmobile id user
                ctx.put(Constant.LOGIN.STATUS_LOGIN, rc);
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);
                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_AUTH_FAILED);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                ctx.put(Constant.WS.RESPONSE, dr);
                ctx.put(Constant.TRX.RC, rc);
                ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);

            }
        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());

            ctx.put(Constant.WS.RESPONSE, dr);
            ctx.put(Constant.TRX.RC, rc);
            ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
        }
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }

}
