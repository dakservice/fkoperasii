/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.tx.pinjaman;

import com.dak.fkoperasii.entity.MAddendum;
import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class addAddendum implements TransactionParticipant{

    Log log = Log.getLog("Q2", getClass().getName());
    

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;
    
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat datemonth = new SimpleDateFormat("dd.MM");
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(new Date());
        Calendar cal = Calendar.getInstance();
        String angsuranke = "";
        
        try {
            String kode_addendum;
            String pinjam_id = jsonReq.getString("pinjamid");
            JSONArray logAngsuran = jsonReq.getJSONArray("angsuran");
            int lengthS = logAngsuran.length();
            log.info("===== LOG ANGSURAN SIZE "+lengthS);
            for (int a = 0; a < lengthS; a++) {
                angsuranke += logAngsuran.getJSONObject(a).getString("angsuranke");
                if (a != lengthS-1) {
                    angsuranke += ", ";
                }
            }
            
            String ket = jsonReq.getString("keterangan");
            
            MAddendum ma = new MAddendum();
            
                MAddendum madd = SpringInit.getMAddendumDao().getAddendumById();
                    
                    if(madd != null){
                        kode_addendum = madd.getKode_addendum().substring(11, 12);
                        log.info("======================= GET KODE ADDENDUM "+madd.getKode_addendum());
                        
                        if(Integer.parseInt(kode_addendum) == 0){
                            kode_addendum = madd.getKode_addendum().substring(10, 12);
                            log.info("============== SUBSTRING KODE ADDENDUM "+kode_addendum);
                            
                        }else{
                                kode_addendum = madd.getKode_addendum().substring(10, 12);
                                log.info("============== SUBSTRING KODE ADDENDUM "+kode_addendum);
                            
                        }
                        String AN = "" + (Integer.parseInt(kode_addendum) + 1);
                        log.info("======================= ISI AN "+AN);
                        String Nol = "";
                        if(AN.length()==1){
                            Nol = "00";
                        }
                        else if(AN.length()==2){
                            Nol = "0";
                        }
                        else if(AN.length()==3){
                            Nol = "";
                        }
                        
                        kode_addendum = "AD."+datemonth.format(new Date())+"."+ Nol + AN;           
                    }else{
                        kode_addendum = "AD."+datemonth.format(new Date())+".001";
                    }
            
            log.info("================== KODE ADDENDUM FIX "+kode_addendum);

            
            ma.setKode_addendum(kode_addendum);
            ma.setPinjam_id(Integer.parseInt(pinjam_id));
            ma.setAngsuran_ke(angsuranke);
            ma.setKeterangan(ket);
            ma.setAlasan("");
            ma.setStatus(0);
            ma.setTgl_input(datetime.format(new Date()));
            
            SpringInit.getMAddendumDao().saveOrUpdate(ma);
            
            JSONObject logData = new JSONObject();
            logData.put("kodeaddendum", kode_addendum);
            logData.put("pinjamid", ""+Integer.parseInt(pinjam_id));
            logData.put("angsuranke", angsuranke);
            logData.put("keterangan", ket);
            logData.put("alasan", "");
            logData.put("status", 0);
            logData.put("tglinput", datetime.format(new Date()));
            
            JSONObject json = new JSONObject();
            json.put("data", logData);
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json);
        } catch (Exception e) {
            
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++"+stringWriter.toString());
            
            
            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), jsonReq);
        }
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbl) {
    }
    
}
