/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MBuktiJaminan;
import com.dak.fkoperasii.entity.MJurnal;
import com.dak.fkoperasii.entity.MNonRutin;
import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MPersyaratanPengajuan;
import com.dak.fkoperasii.entity.MTransSP;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MPengajuanDao")
@Transactional
public class MPengajuanDao extends Dao{
    
    public MPengajuan saveOrUpdate(MPengajuan mPengajuan) {
        if (mPengajuan.getId() == null) {
            em.persist(mPengajuan);
        } else {
            em.merge(mPengajuan);
        }
        return mPengajuan;
    }
    
    public MNonRutin saveOrUpdateNRutin(MNonRutin mNonRutin) {
        if (mNonRutin.getId() == null) {
            em.persist(mNonRutin);
        } else {
            em.merge(mNonRutin);
        }
        return mNonRutin;
    }
    
    
    public MPersyaratanPengajuan saveOrUpdateSyarat(MPersyaratanPengajuan mPersyaratan) {
        if (mPersyaratan.getId() == null) {
            em.persist(mPersyaratan);
        } else {
            em.merge(mPersyaratan);
        }
        return mPersyaratan;
    }
    
     public MPengajuan pengajuanById(Integer id) {
        try {
            MPengajuan mp = (MPengajuan) em.createQuery("SELECT mp FROM MPengajuan mp WHERE mp.id = :id")
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .getSingleResult();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
     
     public MPengajuan getIdPengajuan() {
        try {
            MPengajuan mp = (MPengajuan) em.createQuery("SELECT mp FROM MPengajuan mp ORDER BY id DESC")
                    .setMaxResults(1)
                    .getSingleResult();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
     
     public MPengajuan pengajuanByAjuanId() {
        try {
            MPengajuan mj = (MPengajuan) em.createQuery("SELECT mj FROM MPengajuan mj WHERE mj.ajuan_id LIKE 'PRM.19.08.%' ORDER BY mj.ajuan_id DESC")
                    .setMaxResults(1)
                    .getSingleResult();
            return mj;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
     public List<MPengajuan> getAllbyIdAnggota(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MPengajuan mt WHERE mt.anggota_id = :anggota_id ORDER BY mt.id DESC")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
