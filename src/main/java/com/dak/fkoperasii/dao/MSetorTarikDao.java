/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MSetoranNonTunai;
import com.dak.fkoperasii.entity.MTokenSetorTarik;
import com.dak.fkoperasii.entity.MTokenSetorTarikDetail;
import com.dak.fkoperasii.entity.TmpDetailBrg;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MSetorTarikDao")
@Transactional
public class MSetorTarikDao extends Dao{
    
    public MTokenSetorTarik saveOrUpdate(MTokenSetorTarik mTokenSetorTarik) {
        if (mTokenSetorTarik.getId() == null) {
            em.persist(mTokenSetorTarik);
        } else {
            em.merge(mTokenSetorTarik);
        }
        return mTokenSetorTarik;
    }
    
    public MSetoranNonTunai saveOrUpdate(MSetoranNonTunai mSetorNonTunai) {
        if (mSetorNonTunai.getId() == null) {
            em.persist(mSetorNonTunai);
        } else {
            em.merge(mSetorNonTunai);
        }
        return mSetorNonTunai;
    }
    
    public MTokenSetorTarikDetail saveOrUpdate(MTokenSetorTarikDetail mTokenSetorTarikDetail) {
        if (mTokenSetorTarikDetail.getId() == null) {
            em.persist(mTokenSetorTarikDetail);
        } else {
            em.merge(mTokenSetorTarikDetail);
        }
        return mTokenSetorTarikDetail;
    }
    
    public List<MTokenSetorTarik> getAllChart() {
        try {
           return em.createQuery("SELECT mts FROM MTokenSetorTarik mts ORDER BY mts.id DESC")
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
}
