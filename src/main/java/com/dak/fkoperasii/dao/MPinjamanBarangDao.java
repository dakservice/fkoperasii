/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MPinjamanBarang;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author arfandiusemahu
 */
@Repository(value = "MPinjamanBarangDao")
@Transactional
public class MPinjamanBarangDao extends Dao{
    public List<MPinjamanBarang> viewPinjamanBarang() {
        try {
            List mpb = null;
            mpb = em.createQuery("SELECT mpb from MPinjamanBarang mpb")
                    .getResultList();
            return mpb;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public Object pinjamanBarangByBulanTahun(int bulan, Integer tahun) {
        try {
          return em.createNativeQuery(" select jasa from m_conf_pinjaman_barang m where m.tahum=:tahun and :bulan between m.periode_dari and m.periode_Sampai")
                    .setParameter("bulan", bulan)
                    .setParameter("tahun", tahun)
         .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
