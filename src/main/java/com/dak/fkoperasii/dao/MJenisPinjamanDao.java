/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MJenisPinjaman;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MJenisPinjamanDao")
@Transactional
public class MJenisPinjamanDao extends Dao{
 
    public List<MJenisPinjaman> viewJenisPinjaman() {
        try {
            List mjp = null;
            mjp = em.createQuery("SELECT mjp from MJenisPinjaman mjp")
                    .getResultList();
            return mjp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MJenisPinjaman> jenisPinjamanByStatus(String status) {
        try {
            List mjp;
            mjp = em.createQuery("SELECT mjp from MJenisPinjaman mjp WHERE mjp.status_pegawai= :status and mjp.nama_pinjaman != 'Pinjaman Motor' and mjp.nama_pinjaman != 'Pinjaman Barang'")
                    .setParameter("status", status)
                    .getResultList();
            return mjp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public MJenisPinjaman jenisPinjamanByIdStatus(Integer id, String status) {
        try {
          MJenisPinjaman mjp = (MJenisPinjaman) em.createQuery("SELECT mjp from MJenisPinjaman mjp WHERE mjp.id= :id AND mjp.status_pegawai= :status")
                    .setParameter("id", id)
                    .setParameter("status", status)
                    .setMaxResults(1)
                    .getSingleResult();
            return mjp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public MJenisPinjaman jenisPinjamanById(Integer id) {
        try {
          MJenisPinjaman mjp = (MJenisPinjaman) em.createQuery("SELECT mjp from MJenisPinjaman mjp WHERE mjp.id= :id")
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .getSingleResult();
            return mjp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public MJenisPinjaman jenisPinjamanByNama(String nama,String status) {
        try {
          MJenisPinjaman mjp = (MJenisPinjaman) em.createQuery("SELECT mjp from MJenisPinjaman mjp WHERE mjp.nama_pinjaman= :nama AND mjp.status_pegawai= :status")
                    .setParameter("nama", nama)
                    .setParameter("status", status)
                    .setMaxResults(1)
                    .getSingleResult();
            return mjp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
}
