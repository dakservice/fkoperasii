/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MStatusPengajuan;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MStatusPengajuanDao")
@Transactional
public class MStatusPengajuanDao extends Dao{
    
    public MStatusPengajuan statusById(Integer id) {
        try {
          MStatusPengajuan mjp = (MStatusPengajuan) em.createQuery("SELECT mjp from MStatusPengajuan mjp WHERE mjp.id_status = :id")
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .getSingleResult();
            return mjp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
}
