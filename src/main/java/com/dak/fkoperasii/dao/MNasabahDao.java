/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MLogTrx;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MPartnerCore;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MSaldoAnggota;
import com.dak.fkoperasii.entity.MTanggunganPlafon;
import com.dak.fkoperasii.entity.MTransSP;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "mNasabahDao")
@Transactional
public class MNasabahDao extends Dao {

    public MNasabah saveOrUpdate(MNasabah mNasabah) {
        if (mNasabah.getId() == null) {
            em.persist(mNasabah);
        } else {
            em.merge(mNasabah);
        }
        return mNasabah;
    }

    public MTanggunganPlafon saveTanggungan(MTanggunganPlafon mTanggunganPlafon) {
        if (mTanggunganPlafon.getId_plafon() == null) {
            em.persist(mTanggunganPlafon);
        } else {
            em.merge(mTanggunganPlafon);
        }
        return mTanggunganPlafon;
    }

    public void updateNasabah(MNasabah mNasabah) {
        em.merge(mNasabah);
    }

    public void updateFoto(Integer id_anggota, String filename) {
        Query query = em.createNativeQuery("update tbl_anggota set file_pic= :filename where id= :id_anggota");
        query.setParameter("id_anggota", id_anggota);
        query.setParameter("filename", filename);
        query.executeUpdate();
    }

    public void updateTanggungan(MTanggunganPlafon mTanggunganPlafon) {
        em.merge(mTanggunganPlafon);
    }

    public void deleteTanggungan(Integer id_anggota, Integer id_plafon) {
        Query query = em.createQuery("Delete from MTanggunganPlafon s where s.id = :id_anggota and s.id_plafon = :id_plafon");
        query.setParameter("id_anggota", id_anggota);
        query.setParameter("id_plafon", id_plafon);
        query.executeUpdate();
    }

    public MNasabah userByNotelp(String notelp) {
        try {
            MNasabah mnd = (MNasabah) em.createQuery("SELECT mnd FROM MNasabah mnd WHERE mnd.notelp = :notelp ORDER BY mnd.id DESC")
                    .setParameter("notelp", notelp)
                    .setMaxResults(1)
                    .getSingleResult();
            return mnd;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public MNasabah userByEmail(String email) {
        try {
            MNasabah mnd = (MNasabah) em.createQuery("SELECT mnd FROM MNasabah mnd WHERE mnd.email = :email ORDER BY mnd.id DESC")
                    .setParameter("email", email)
                    .setMaxResults(1)
                    .getSingleResult();
            return mnd;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public MNasabah userById(Long id_anggota) {
        try {
            MNasabah mnd = (MNasabah) em.createQuery("SELECT mnd FROM MNasabah mnd WHERE mnd.id =:id_anggota ORDER BY mnd.id DESC")
                    .setParameter("id_anggota", id_anggota)
                    .setMaxResults(1)
                    .getSingleResult();
            return mnd;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public MNasabah loginUser(String notelp, String password) {
        try {
            return (MNasabah) em.createQuery("SELECT mn FROM MNasabah mn WHERE (mn.notelp = :notelp OR mn.nip = :notelp) AND mn.pass_word = :password")
                    .setParameter("notelp", notelp)
                    .setParameter("password", password)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public String encryptPasswordSHA1(String pass_word) {

        String encPass = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA1");
            digest.update(pass_word.getBytes(), 0, pass_word.length());
            encPass = new BigInteger(1, digest.digest()).toString(16);
            String vsha1 = encPass.toLowerCase();
            return vsha1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encPass;
    }

    public MTanggunganPlafon getTanggunganByAnggota(Integer id_anggota) {
        try {
            MTanggunganPlafon mnd = (MTanggunganPlafon) em.createQuery("SELECT mtp FROM MTanggunganPlafon mtp WHERE mtp.id = :id_anggota")
                    .setParameter("id_anggota", id_anggota)
                    .setMaxResults(1)
                    .getSingleResult();
            return mnd;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public MTanggunganPlafon getTanggunganByIdandAnggota(Integer id_anggota, Integer id_plafon) {
        try {
            MTanggunganPlafon mnd = (MTanggunganPlafon) em.createQuery("SELECT mtp FROM MTanggunganPlafon mtp WHERE mtp.id = :id_anggota AND mtp.id_plafon = :id_plafon")
                    .setParameter("id_anggota", id_anggota)
                    .setParameter("id_plafon", id_plafon)
                    .setMaxResults(1)
                    .getSingleResult();
            return mnd;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public Double getSaldoPokok(String id_anggota) {
        try {
            return (Double) em.createNativeQuery("SELECT(SELECT IFNULL(SUM(jumlah),0) FROM tbl_trans_sp WHERE anggota_id= :id_anggota and jenis_id='40' and akun ='Setoran') -\n"
                    + "(SELECT IFNULL(SUM(jumlah),0) FROM tbl_trans_sp WHERE anggota_id= :id_anggota AND jenis_id ='40' and akun='Penarikan')- \n"
                    + "(SELECT value_awal FROM custom_coa WHERE nm_label='saldo_minimum' AND status_aktif ='Y')")
                    .setParameter("id_anggota", id_anggota)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public Integer getSaldoPokoks(String id_anggota) {
        try {
            return (Integer) em.createNativeQuery("SELECT saldo FROM tbl_saldo_anggota WHERE id_anggota= :id_anggota AND id_jenis='40'")
                    .setParameter("id_anggota", id_anggota)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public Double getSaldoWajib(String id_anggota) {
        try {
            return (Double) em.createNativeQuery("SELECT(SELECT IFNULL(SUM(jumlah),0) FROM tbl_trans_sp WHERE anggota_id= :id_anggota and jenis_id='41' and akun ='Setoran') -\n"
                    + "(SELECT IFNULL(SUM(jumlah),0) FROM tbl_trans_sp WHERE anggota_id= :id_anggota AND jenis_id ='41' and akun='Penarikan')- \n"
                    + "(SELECT value_awal FROM custom_coa WHERE nm_label='saldo_minimum' AND status_aktif ='Y')")
                    .setParameter("id_anggota", id_anggota)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public Integer getSaldoWajibs(String id_anggota) {
        try {
            return (Integer) em.createNativeQuery("SELECT saldo FROM tbl_saldo_anggota WHERE id_anggota= :id_anggota AND id_jenis='41'")
                    .setParameter("id_anggota", id_anggota)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public Double getSaldoSukarela(String id_anggota) {
        try {
            return (Double) em.createNativeQuery("SELECT(SELECT IFNULL(SUM(jumlah),0) FROM tbl_trans_sp WHERE anggota_id= :id_anggota and jenis_id='32' and akun ='Setoran') -\n"
                    + "(SELECT IFNULL(SUM(jumlah),0) FROM tbl_trans_sp WHERE anggota_id= :id_anggota AND jenis_id ='32' and akun='Penarikan')- \n"
                    + "(SELECT value_awal FROM custom_coa WHERE nm_label='saldo_minimum' AND status_aktif ='Y') -"
                    + "(SELECT IFNULL(SUM(amount),0) FROM MLogTrx WHERE anggota_id= :id_anggota)")
                    .setParameter("id_anggota", id_anggota)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public Integer getSaldoSukarelas(String id_anggota) {
        try {
            return (Integer) em.createNativeQuery("SELECT saldo FROM tbl_saldo_anggota WHERE id_anggota= :id_anggota AND id_jenis='32'")
                    .setParameter("id_anggota", id_anggota)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public void updateSaldoSukarela(Integer id_anggota, Integer saldo) {
        Query query = em.createNativeQuery("update tbl_saldo_anggota set saldo = :saldo where id_anggota =:id_anggota and id_jenis='32'");
        query.setParameter("id_anggota", id_anggota);
        query.setParameter("saldo", saldo);
        query.executeUpdate();
    }

    public Double getJumlahLogTrx(String id_anggota) {
        try {
            return (Double) em.createNativeQuery("SELECT(SELECT IFNULL(SUM(amount),0) FROM MLogTrx WHERE anggota_id= :id_anggota)")
                    .setParameter("id_anggota", id_anggota)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

//    public Double getSaldoByIdSimpanan(String id_anggota, String id_simpanan) {
//        try {
//            return (Double) em.createNativeQuery("SELECT(SELECT IFNULL(SUM(jumlah),0) FROM tbl_trans_sp WHERE anggota_id= :id_anggota and jenis_id= :id_simpanan and akun ='Setoran') -\n" +
//                                                   "(SELECT IFNULL(SUM(jumlah),0) FROM tbl_trans_sp WHERE anggota_id= :id_anggota AND jenis_id = :id_simpanan and akun='Penarikan') - \n" +
//                                                   "(SELECT value_awal FROM custom_coa WHERE nm_label='saldo_minimum' AND status_aktif ='Y')")
//                    .setParameter("id_anggota", id_anggota)
//                    .setParameter("id_simpanan", id_simpanan)
//                    .getSingleResult();
//        } catch (NoResultException nre) {
//            nre.printStackTrace();
//            return null;
//        }
//    }
    public Integer getSaldoByIdSimpanan(String id_anggota, String id_simpanan) {
        try {
            return (Integer) em.createNativeQuery("SELECT saldo FROM tbl_saldo_anggota WHERE id_anggota=:id_anggota AND id_jenis=:id_simpanan")
                    .setParameter("id_anggota", id_anggota)
                    .setParameter("id_simpanan", id_simpanan)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public BigDecimal getTotalTanggungan(String id_anggota) {
        try {
            return (BigDecimal) em.createNativeQuery("SELECT(SELECT IFNULL(SUM(nominal_tanggungan),0) FROM tbl_tanggungan_plafon WHERE id= :id_anggota)")
                    .setParameter("id_anggota", id_anggota)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public List<MTanggunganPlafon> tanggunganByIdAnggota(Integer id_anggota) {
        try {
            List mtp;
            mtp = em.createQuery("SELECT mtp from MTanggunganPlafon mtp WHERE mtp.id= :id_anggota")
                    .setParameter("id_anggota", id_anggota)
                    .getResultList();
            return mtp;
        } catch (NoResultException nre) {
            return null;
        }
    }

}
