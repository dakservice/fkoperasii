/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MSetting;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "mSettingDao")
@Transactional(transactionManager = "transactionManagerInternal")
public class MSettingDao extends Dao{
    
    public MSetting getSetting(String incom) {
        try {
            return (MSetting) em.createQuery("SELECT mrc FROM MSetting mrc WHERE mrc.incoming = :incoming")
                    .setParameter("incoming", incom)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public MSetting getSettingByParam(String param){
        try{
            return (MSetting)em.createQuery("select s from MSetting s where s.parameter=:param")
                    .setParameter("param",param)
                    .getSingleResult();
        }catch (Exception e){
            return null;
        }
    }

    public Object getTerm(){
        try{
            return em.createNativeQuery("select content from tbl_terms").getSingleResult();
        }catch (Exception e){
            return null;
        }
    }

    public Object getJasaPinjamanRutin(String tahun){
    try{
        return em.createNativeQuery("select jasa from m_conf_pinjaman_rutin where tahun =:p_tahun\n")
                .setParameter("p_tahun",tahun)
                .getSingleResult();
    }catch (Exception e){
        return null;
    }
    }
}
