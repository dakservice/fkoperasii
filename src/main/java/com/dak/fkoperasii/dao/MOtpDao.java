/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MOtp;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author 1star
 */
@Repository(value = "MOtpDao")
@Transactional
public class MOtpDao extends Dao{
    
       
        public MOtp saveOrUpdate(MOtp mOtp) {
        if (mOtp.getId() == null) {
            em.persist(mOtp);
        } else {
            em.merge(mOtp);
        }
        return mOtp;
    }
        
         public MOtp getOtpBynoTlpDate(String noTelp) {
        try {
            MOtp mp = (MOtp) em.createQuery("SELECT m FROM MOtp m WHERE m.noTelp =:noTelp ORDER BY m.createAt DESC")
                    .setParameter("noTelp", noTelp)
                    .setMaxResults(1)
                    .getSingleResult();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
          public MOtp getOtpByDate(String otp) {
        try {
            MOtp mp = (MOtp) em.createQuery("SELECT m FROM MOtp m WHERE m.otp =:otp ORDER BY m.createAt DESC")
                    .setParameter("otp", otp)
                    .setMaxResults(1)
                    .getSingleResult();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
}
