/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MTransSP;
import com.dak.fkoperasii.entity.TmpDetailChart;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MTransSPDao")
@Transactional
public class MTransSPDao extends Dao{

    public List<Object[]> getHistorySpByDate(String anggota_id, String strdate, String enddate) {
        try {
            return em.createNativeQuery("SELECT id,tgl_transaksi,jenis_id,jumlah,keterangan FROM tbl_trans_sp WHERE anggota_id = :anggota_id and tgl_transaksi >= DATE(:strdate) and tgl_transaksi <= DATE(:enddate) order by id asc")
                    .setParameter("anggota_id", anggota_id)
                    .setParameter("strdate", strdate)
                    .setParameter("enddate", enddate)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MTransSP> getAll(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id ORDER BY mt.id DESC")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<Object[]> getAllByDate() {
        try {
               return em.createNativeQuery("SELECT * FROM tbl_trans_sp WHERE anggota_id = '3' and tgl_transaksi >= DATE('2019-07-03') and tgl_transaksi <= DATE('2019-07-05')")
//                    .setParameter("anggota_id", anggota_id)
//                    .setParameter("strdate", strdate)
//                    .setParameter("enddate", enddate)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MTransSP> getAllSetoran(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND akun='Setoran'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MTransSP> getAllPenarikan(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND akun='Penarikan'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MTransSP> getSetorSimpananWajib(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='41' AND akun='Setoran'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getTarikSimpananWajib(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='41' AND akun='Penarikan'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getSetorSimpananPokok(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='40' AND akun='Setoran'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getTarikSimpananPokok(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='40' AND akun='Penarikan'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getSetorSimpananSukarela(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='32' AND akun='Setoran'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MTransSP> getTarikSimpananSukarela(Integer anggota_id) {
        try {
           return em.createQuery("SELECT mt FROM MTransSP mt WHERE mt.anggota_id = :anggota_id AND mt.jenis_id='32' AND akun='Penarikan'")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

}
