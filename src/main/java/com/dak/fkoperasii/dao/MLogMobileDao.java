/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MLogMobile;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MLogMobileDao")
@Transactional
public class MLogMobileDao extends Dao {

    public MLogMobile saveOrUpdate(MLogMobile logMobileTrx) {
        if (logMobileTrx.getId() == null) {
            em.persist(logMobileTrx);
        } else {
            em.merge(logMobileTrx);
        }
        return logMobileTrx;

    }
    
    public MLogMobile getLogByActivity(MLogMobile mobileIdActivity) {
        try {
            MLogMobile trx = (MLogMobile) em.createQuery("SELECT trx FROM MLogMobile trx WHERE trx.id = :mobileIdActivity")
                    .setParameter("mobileIdActivity", mobileIdActivity)
                    .getSingleResult();
            return trx;
        } catch (NoResultException nre) {
            return null;
        }
    }

}
