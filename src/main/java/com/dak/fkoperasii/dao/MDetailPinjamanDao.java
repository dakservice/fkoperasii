/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MDetailPinjaman;
import com.dak.fkoperasii.entity.MHeadPinjaman;
import com.dak.fkoperasii.entity.MJurnal;
import com.dak.fkoperasii.entity.MPengajuan;
import com.dak.fkoperasii.entity.MTransSP;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MDetailPinjamanDao")
@Transactional
public class MDetailPinjamanDao extends Dao{
    
//    public MPengajuan saveOrUpdate(MPengajuan mPengajuan) {
//        if (mPengajuan.getId() == null) {
//            em.persist(mPengajuan);
//        } else {
//            em.merge(mPengajuan);
//        }
//        return mPengajuan;
//    }
    
//     public MPengajuan pengajuanById(Integer id) {
//        try {
//            MPengajuan mp = (MPengajuan) em.createQuery("SELECT mp FROM MPengajuan mp WHERE mp.id = :id")
//                    .setParameter("id", id)
//                    .setMaxResults(1)
//                    .getSingleResult();
//            return mp;
//        } catch (NoResultException nre) {
//            return null;
//        }
//    }
//    
//     
//     public MHeadPinjaman pinjamanByAjuanId(Integer id_pinjam) {
//        try {
//            MHeadPinjaman mj = (MHeadPinjaman) em.createQuery("SELECT mj FROM MHeadPinjaman mj WHERE mj.id= :id_pinjam ORDER BY mj.id DESC")
//                    .setParameter("id_pinjam", id_pinjam)
//                    .setMaxResults(1)
//                    .getSingleResult();
//            return mj;
//        } catch (NoResultException nre) {
//            return null;
//        }
//    }
    
     public List<MDetailPinjaman> getAllById(Integer id_pinjam) {
        try {
           return em.createQuery("SELECT mt FROM MDetailPinjaman mt WHERE mt.pinjam_id = :id_pinjam")
                    .setParameter("id_pinjam", id_pinjam)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
