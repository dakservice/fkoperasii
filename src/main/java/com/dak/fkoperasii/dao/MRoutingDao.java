/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MSetting;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value="MRoutingDao")
@Transactional
public class MRoutingDao extends Dao{
    
    public MSetting getConstantDB(String param) {
        try {
            MSetting query = (MSetting) em.createQuery("SELECT m FROM MSetting m WHERE m.parameter =:parameter")
                    .setParameter("parameter", param)
                    .setMaxResults(1)
                    .getSingleResult();
            return query;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
}
