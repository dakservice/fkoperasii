/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MCoa;
import com.dak.fkoperasii.entity.MJurnal;
import com.dak.fkoperasii.entity.MKas;
import com.dak.fkoperasii.entity.MNasabah;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MJurnalDao")
@Transactional
public class MJurnalDao extends Dao{
    
    public MJurnal saveOrUpdate(MJurnal mJurnal) {
        if (mJurnal.getId_jurnal()== null) {
            em.persist(mJurnal);
        } else {
            em.merge(mJurnal);
        }
        return mJurnal;
    }
    
    public MJurnal jurnalByIdtrx() {
        try {
            MJurnal mj = (MJurnal) em.createQuery("SELECT mj FROM MJurnal mj WHERE mj.id_transaksi LIKE 'TRM%' ORDER BY mj.id_transaksi DESC")
                    .setMaxResults(1)
                    .getSingleResult();
            return mj;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public MKas cekKas() {
        try {
            MKas mk = (MKas) em.createQuery("SELECT mk FROM MKas mk WHERE mk.trans_pulsa='Ya' OR mk.trans_pulsa='Y'")
                    .setMaxResults(1)
                    .getSingleResult();
            return mk;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public Integer cekCoa() {
        try {
            return (Integer) em.createQuery("SELECT mo.id_coa FROM MCoa mo WHERE mo.keterangan ='Penjualan Pulsa'")
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public Integer cekCoaSukarela() {
        try {
            return (Integer) em.createQuery("SELECT mo.id_coa FROM MCoa mo WHERE mo.keterangan ='Simpanan Sukarela'")
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
}
