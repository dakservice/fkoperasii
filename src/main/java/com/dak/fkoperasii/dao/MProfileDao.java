package com.dak.fkoperasii.dao;



import com.dak.fkoperasii.entity.MAgama;
import com.dak.fkoperasii.entity.MDepartement;
import com.dak.fkoperasii.entity.MJenisAngsuran;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MPekerjaan;
import com.dak.fkoperasii.entity.MRc;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author risyamaulana
 */
@Repository(value = "MProfileDao")
@Transactional
public class MProfileDao extends Dao {
    
    
    public List<MAgama> getAgama() {
        try {
            List agama;
            agama = em.createQuery("SELECT ag from MAgama ag ORDER BY ag.id_agama ASC")
                    .getResultList();
            return agama;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public MAgama agamaById(Integer idagama) {
        try {
            MAgama ma = (MAgama) em.createQuery("SELECT ma FROM MAgama ma WHERE ma.id_agama = :idagama ORDER BY ma.id_agama DESC")
                    .setParameter("idagama", idagama)
                    .setMaxResults(1)
                    .getSingleResult();
            return ma;
        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<Object[]> getPekerjaan() {
        try {
            return em.createNativeQuery("SELECT id_kerja,jenis_kerja from pekerjaan k where k.aktif='y' ORDER BY k.id_kerja ASC")
                    .getResultList();

        } catch (NoResultException nre) {
            return null;
        }
    }

    public List<MDepartement> getDepartement() {
        try {
            List dep;
            dep = em.createQuery("SELECT d from MDepartement d ORDER BY d.id_departement ASC")
                    .getResultList();
            return dep;
        } catch (NoResultException nre) {
            return null;
        }
    }

    
}
