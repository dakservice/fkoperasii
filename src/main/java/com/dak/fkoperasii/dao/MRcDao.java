package com.dak.fkoperasii.dao;



import com.dak.fkoperasii.entity.MRc;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author risyamaulana
 */
@Repository(value = "mRcDao")
@Transactional
public class MRcDao extends Dao {
    
    public MRc getRc(String rc) {
        try {
            MRc mrc = (MRc) em.createQuery("SELECT mrc FROM MRc mrc WHERE mrc.rc = :rc")
                    .setParameter("rc", rc)
                    .getSingleResult();
            return mrc;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public MRc findRm(String rc) {
        try {
            System.out.println("Rc Incoming "+rc);
            return (MRc) em.createQuery("SELECT r FROM MRc AS r WHERE r.rc =:rc")
                    .setParameter("rc", rc)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    
}
