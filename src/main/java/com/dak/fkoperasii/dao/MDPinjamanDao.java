/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MAddendum;
import com.dak.fkoperasii.entity.MDPinjaman;
import com.dak.fkoperasii.entity.MDetailPinjaman;
import com.dak.fkoperasii.entity.MNasabah;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MDPinjamanDao")
@Transactional
public class MDPinjamanDao extends Dao{
    
    public MDPinjaman save(MDPinjaman mDPinjaman) {
        if (mDPinjaman.getId() == null) {
            em.persist(mDPinjaman);
        } 
        return mDPinjaman;
    }
    
    public MDPinjaman update(MDPinjaman mDPinjaman) {
        if (mDPinjaman.getId() != null) {
            em.merge(mDPinjaman);
        } 
        return mDPinjaman;
    }
    
    public List<MDPinjaman> getDetailPinjamanById(Integer id_pinjam) {
        try {
           return em.createQuery("SELECT mt FROM MDPinjaman mt WHERE mt.pinjam_id = :id_pinjam ORDER BY mt.id ASC")
                    .setParameter("id_pinjam", id_pinjam)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MDetailPinjaman> getAllById(Integer id_pinjam) {
        try {
           return em.createQuery("SELECT mt FROM MDetailPinjaman mt WHERE mt.pinjam_id = :id_pinjam")
                    .setParameter("id_pinjam", id_pinjam)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MAddendum> getAddendumById(Integer id_pinjam) {
        try {
           return em.createQuery("SELECT mt FROM MAddendum mt WHERE mt.pinjam_id = :id_pinjam")
                    .setParameter("id_pinjam", id_pinjam)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public MDPinjaman getTotalJasa(String pinjam_id) {
        try {
            return (MDPinjaman) em.createNativeQuery("SELECT(SELECT IFNULL(SUM(angsuran_bunga)) FROM MDPinjaman WHERE pinjam_id= :pinjam_id)")
                    .setParameter("pinjam_id", pinjam_id)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }
}
