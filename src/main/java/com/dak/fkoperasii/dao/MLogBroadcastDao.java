package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MJurnal;
import com.dak.fkoperasii.entity.MLogBroadcast;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository(value = "MlogBroadcastDao")
@Transactional
public class MLogBroadcastDao extends Dao{
    public MLogBroadcast saveOrUpdate(MLogBroadcast mLogBroadcast) {
        if (mLogBroadcast.getId()== null) {
            em.persist(mLogBroadcast);
        } else {
            em.merge(mLogBroadcast);
        }
        return mLogBroadcast;
    }
}
