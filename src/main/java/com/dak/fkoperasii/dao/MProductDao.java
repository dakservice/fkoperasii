/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MProduct;
import com.dak.fkoperasii.entity.MNasabah;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MProductDao")
@Transactional
public class MProductDao extends Dao{
     
    public MProduct barangById(long id) {
        try {
            MProduct mp = (MProduct) em.createQuery("SELECT mp FROM MProduct mp WHERE mp.id = :id ORDER BY mp.id DESC")
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .getSingleResult();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MProduct> viewAllProduct() {
        try {
            List mp = null;
            mp = em.createQuery("SELECT mp from MProduct mp")
                    .getResultList();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MProduct> productByKategori(String kategori) {
        try {
            List mp = null;
            mp = em.createQuery("SELECT mp from MProduct mp WHERE mp.kategori =:kategori AND mp.jml_brg != 0")
                    .setParameter("kategori", kategori)
                    .getResultList();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MProduct> productByMerk(String provider) {
        try {
            List mp = null;
            mp = em.createQuery("SELECT mp from MProduct mp WHERE mp.merk =:provider AND mp.jml_brg != 0")
                    .setParameter("provider", provider)
                    .getResultList();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
}
