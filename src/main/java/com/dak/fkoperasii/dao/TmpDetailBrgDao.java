/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MPresentasePlafon;
import com.dak.fkoperasii.entity.TmpDetailBrg;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "TmpDetailBrgDao")
@Transactional
public class TmpDetailBrgDao extends Dao{
    
    public TmpDetailBrg saveOrUpdate(TmpDetailBrg tmpDetailBrg) {
//        if (tmpDetailBrg.getId() == null && tmpDetailBrg.getId_barang() == null) {
            em.persist(tmpDetailBrg);
//        } else {
//            em.merge(tmpDetailBrg);
//        }
        return tmpDetailBrg;
    }
    
    public TmpDetailBrg remove(TmpDetailBrg tmpDetailBrg) {
        em.remove(tmpDetailBrg);
        return tmpDetailBrg;
    }
    
//    public TmpDetailChart getAllChart(Long anggota_id) {
//        try {
//            List mlt = null;
//            mlt = em.createQuery("SELECT mlt FROM TmpDetailChart mlt WHERE mlt.anggota_id = :anggota_id")
//                    .setParameter("anggota_id", anggota_id)
//                    .getResultList();
//            return mlt;
//        } catch (NoResultException nre) {
//            return null;
//        }
//    }
    
    public List<TmpDetailBrg> getAllChart(Long anggota_id) {
        try {
           return em.createQuery("SELECT tmp FROM TmpDetailBrg tmp WHERE tmp.id = :anggota_id ORDER BY tmp.id")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<Object[]> getAllChartNative(Long anggota_id) {
        try {
           return em.createNativeQuery("SELECT id, id_barang, jml_brg FROM tbl_tmp_dtl_brg WHERE id = :anggota_id")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public TmpDetailBrg getDetailByIdBarang(Integer id, Integer id_barang) {
        try {
          TmpDetailBrg mp = (TmpDetailBrg) em.createQuery("SELECT mp from TmpDetailBrg mp WHERE mp.id= :id AND mp.id_barang= :id_barang")
                    .setParameter("id", id)
                    .setParameter("id_barang", id_barang)
                    .setMaxResults(1)
                    .getSingleResult();
            return mp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public void deleteCartBrg(int id, Integer id_barang) {
        Query query = em.createQuery("Delete from TmpDetailBrg s where s.id = :id and s.id_barang = :id_barang");
        query.setParameter("id", id);
        query.setParameter("id_barang", id_barang);
        query.executeUpdate();
    }
    
//    public List <TmpDetailBrg> getIdandIdBarang(Integer id, Integer id_barang) {
//        try {
//          TmpDetailBrg mp = (TmpDetailBrg) em.createQuery("SELECT mp.id, mp.id_barang from TmpDetailBrg mp WHERE mp.id= :id AND mp.id_barang= :id_barang ORDER BY mp.id DESC")
//                    .setParameter("id", id)
//                    .setParameter("id_barang", id_barang)
//                    .setMaxResults(1)
//                    .getSingleResult();
//            return mp;
//        } catch (NoResultException nre) {
//            return null;
//        }
//    }
    
    public List<TmpDetailBrg> getIdandIdBarang(Integer id, Integer id_barang) {
        try {
           return em.createQuery("SELECT mp.id, mp.id_barang from TmpDetailBrg mp WHERE mp.id= :id AND mp.id_barang= :id_barang")
                    .setParameter("id", id)
                    .setParameter("id_barang", id_barang)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
//    public List<TmpDetailChart> getAllChart(Long anggota_id) {
//        try {
//            List tmp = null;
//            tmp = (TmpDetailChart) em.createQuery("SELECT tmp FROM TmpDetailChart tmp WHERE tmp.id = :anggota_id ORDER BY tmp.id DESC")
//                    .setParameter("anggota_id", anggota_id)
//                    .getResultList();
//            return tmp;
//        } catch (NoResultException nre) {
//            return null;
//        }
//    }
    
    
}
