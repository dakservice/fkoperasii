/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MDeviceAnggota;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author 1star
 */
@Repository(value = "MDeviceAnggotaDao")
@Transactional
public class MDeviceAnggotaDao extends Dao {

    public MDeviceAnggota saveOrUpdate(MDeviceAnggota mAddendum) {
        if (mAddendum.getDevice_id() == null) {
            em.persist(mAddendum);
        } else {
            em.merge(mAddendum);
        }
        return mAddendum;
    }
    
        public MDeviceAnggota finddeviceIdByIdaAnggotaAndDeviceID(String deviceId, Long id_anggota) {
        try {
            MDeviceAnggota mnd = (MDeviceAnggota) em.createQuery("SELECT mnd FROM MDeviceAnggota mnd"
                    + " WHERE mnd.idAnggota = :id_anggota AND mnd.device_id= :deviceId")
                    .setParameter("id_anggota", id_anggota)
                    .setParameter("deviceId", deviceId)
                    .setMaxResults(1)
                    .getSingleResult();
            return mnd;
        } catch (NoResultException nre) {
            return null;
        }
    }
        public MDeviceAnggota finddeviceIdByIdaAnggota(Long id_anggota) {
        try {
            MDeviceAnggota mnd = (MDeviceAnggota) em.createQuery("SELECT mnd FROM MDeviceAnggota mnd WHERE mnd.idAnggota = :id_anggota")
                    .setParameter("id_anggota", id_anggota)
                    .setMaxResults(1)
                    .getSingleResult();
            return mnd;
        } catch (NoResultException nre) {
            return null;
        }
    }
        public MDeviceAnggota finddeviceIdBydeviceId(String deviceId) {
        try {
            MDeviceAnggota mnd = (MDeviceAnggota) em.createQuery("SELECT mnd FROM MDeviceAnggota mnd WHERE mnd.device_id = :deviceId")
                    .setParameter("deviceId", deviceId)
                    .setMaxResults(1)
                    .getSingleResult();
            return mnd;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
}
