/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MLogTrx;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.TmpDetailChart;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "TmpDetailChartDao")
@Transactional
public class TmpDetailChartDao extends Dao{
    
    public TmpDetailChart saveOrUpdate(TmpDetailChart tmpDetailChart) {
//        if (tmpDetailChart.getId() == null && tmpDetailChart.getId_barang() == null) {
            em.persist(tmpDetailChart);
//        } else {
//            em.merge(tmpDetailChart);
//        }
        return tmpDetailChart;

    }
    
//    public TmpDetailChart getAllChart(Long anggota_id) {
//        try {
//            List mlt = null;
//            mlt = em.createQuery("SELECT mlt FROM TmpDetailChart mlt WHERE mlt.anggota_id = :anggota_id")
//                    .setParameter("anggota_id", anggota_id)
//                    .getResultList();
//            return mlt;
//        } catch (NoResultException nre) {
//            return null;
//        }
//    }
    
    public List<TmpDetailChart> getAllChart(Long anggota_id) {
        try {
           return em.createQuery("SELECT tmp FROM TmpDetailChart tmp WHERE tmp.id = :anggota_id ORDER BY tmp.id")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<Object[]> getAllChartNative(Long anggota_id) {
        try {
           return em.createNativeQuery("SELECT id, id_barang, jml FROM tbl_tmp_dtl_chart WHERE id = :anggota_id")
                    .setParameter("anggota_id", anggota_id)
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public void deleteCartMtr(int id, Long id_barang) {
        Query query = em.createQuery("Delete from TmpDetailChart s where s.id = :id and s.id_barang = :id_barang");
        query.setParameter("id", id);
        query.setParameter("id_barang", id_barang);
        query.executeUpdate();
    }
    
//    public List<TmpDetailChart> getAllChart(Long anggota_id) {
//        try {
//            List tmp = null;
//            tmp = (TmpDetailChart) em.createQuery("SELECT tmp FROM TmpDetailChart tmp WHERE tmp.id = :anggota_id ORDER BY tmp.id DESC")
//                    .setParameter("anggota_id", anggota_id)
//                    .getResultList();
//            return tmp;
//        } catch (NoResultException nre) {
//            return null;
//        }
//    }
    
    
}
