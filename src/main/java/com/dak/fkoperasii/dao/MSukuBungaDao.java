/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MJenisPinjaman;
import com.dak.fkoperasii.entity.MSukuBunga;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MSukuBungaDao")
@Transactional
public class MSukuBungaDao extends Dao{
 
    public MSukuBunga getSukuBunga() {
        try {
          MSukuBunga msb = (MSukuBunga) em.createQuery("SELECT msb from MSukuBunga msb WHERE msb.opsi_key='biaya_adm'")
                    .setMaxResults(1)
                    .getSingleResult();
            return msb;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
}
