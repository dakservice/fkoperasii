/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MJenisSimpan;
import com.dak.fkoperasii.entity.MJurnal;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Morten Jonathan
 */
@Repository(value = "MJenisSimpanDao")
@Transactional
public class MJenisSimpanDao extends Dao{
    
    public MJenisSimpan jenisSimpanById(Integer id) {
        try {
            return (MJenisSimpan) em.createQuery("SELECT mo FROM MJenisSimpan mo WHERE mo.id=:id")
                    .setParameter("id", id)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MJenisSimpan> getAllJenisSimpanan() {
        try {
           return em.createQuery("SELECT mo FROM MJenisSimpan mo ORDER BY mo.id ASC ")
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public String getJenisSimpnanById(int id) {
        try {
            return em.createQuery("select j.jns_simpan from MJenisSimpan j  where j.id=:pid")
                    .setParameter("pid",id)
                    .getSingleResult().toString();
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public List<MJenisSimpan> getJenisSimpanan() {
        try {
           return em.createQuery("SELECT mo FROM MJenisSimpan mo where mo.tarik_tunai !='T'")
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public Object getMinSaldoSukarela(String year,int month){
        try{
            return em.createNativeQuery("select mcss.min_ngendap from dbkop_kop3ras1_bck.m_conf_sim_sukarela mcss where mcss.tahun =:pyear and :pmonth BETWEEN periode_dari and periode_sampai ")
                    .setParameter("pyear",year)
                    .setParameter("pmonth",month)
                    .getSingleResult();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    
}
