/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.dao;

import com.dak.fkoperasii.entity.MPinjamanRutinPromo;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author arfandiusemahu
 */
@Repository(value = "MPinjamanRutinPromoDao")
@Transactional
public class MPinjamanRutinPromoDao extends Dao{
    public List<MPinjamanRutinPromo> viewPinjamanRutin() {
        try {
            List mprp = null;
            mprp = em.createQuery("SELECT mpr from MPinjamanRutin mpr")
                    .getResultList();
            return mprp;
        } catch (NoResultException nre) {
            return null;
        }
    }
    
    public Object pinjamanRutinPromoByBulanTahunNominal(int bulan, Integer tahun, Integer nominal) {
        System.out.println("asd ============="+nominal);
        System.out.println("asd ============="+bulan);
        System.out.println("asd ============="+tahun);
        try {
          return  em.createNativeQuery("select jasa from m_conf_pinjaman_rutin_promo mcprp where tahun =:tahun and (:bulan BETWEEN periode_dari and periode_sampai) and nominal_pinjaman >=:nominal order by nominal_pinjaman asc limit 1")
                    .setParameter("bulan", bulan)
                    .setParameter("tahun", tahun)
                    .setParameter("nominal", nominal)
                    .getSingleResult();
        } catch (NoResultException nre) {
            System.out.println("asdNRE  ======="+nre.getMessage());
            return null;
        }
    }
}
