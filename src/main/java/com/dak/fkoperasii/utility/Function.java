/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import com.dak.fkoperasii.entity.MLogBroadcast;
import com.dak.fkoperasii.entity.MSetting;
import com.dak.fkoperasii.spring.SpringInit;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.transactional.SendContact;
import com.mailjet.client.transactional.SendEmailsRequest;
import com.mailjet.client.transactional.TrackOpens;
import com.mailjet.client.transactional.TransactionalEmail;
import com.mailjet.client.transactional.response.SendEmailsResponse;
import okhttp3.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jpos.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.*;

/**
 * @author Morten Jonathan
 */
public class Function {

    public static String getCurrentTime() {
        final DateTime date = new DateTime();
        return (date.toString(DateTimeFormat.forPattern("MM-dd-yyyy")));
    }

    public static JSONObject sendEmailLib(String email,String receiverName,String html) {
        final MLogBroadcast log = new MLogBroadcast();
        try {
            ClientOptions options = ClientOptions.builder()
                    .apiKey("ee5cb04f1c66e70ad4d265f9bf2d2992")
                    .apiSecretKey("e32af3572e6b5c0a8d24ef579a0d978f")
                    .build();
            MailjetClient client = new MailjetClient(options);
            TransactionalEmail message1 = TransactionalEmail
                    .builder()
                    .to(new SendContact(email, receiverName))
                    .from(new SendContact(Constant.EMAIL_SENDER, "KOPERASI DIY"))
                    .htmlPart(html)
                    .subject("FORGOT PASSWORD KOPERASI APP")
                    .trackOpens(TrackOpens.ENABLED)
                    //.attachment(Attachment.fromFile(attachmentPath))
                    .header("test-header-key", "test-value")
                    .customID("custom-id-value")
                    .build();
            SendEmailsRequest request = SendEmailsRequest
                    .builder()
                    .message(message1) // you can add up to 50 messages per request
                    .build();
            SendEmailsResponse response = request.sendWith(client);
            //save log
            log.setDate(getCurrentTime());
            log.setType("EMAIL");
            log.setBroadcastType("");
            log.setMessage(html);
            log.setReceiver(email);
            log.setSender(Constant.EMAIL_SENDER);
            final Log log2 = Log.getLog("Q2", "FUNCTIONS EMAIL");
            log2.info(response.toString());
            log.setResponse("00");
            log.setSystemResponse(response.toString());
            SpringInit.getmLogBroadcastDao().saveOrUpdate(log);
            if (response.toString().contains("status=SUCCESS"))
                return new JSONObject().put("message","Success");
            else
                return new JSONObject().put("message","Bad Request");

        } catch (Exception e) {
            log.setResponse("IE");
            log.setSystemResponse(e.getMessage());
            SpringInit.getmLogBroadcastDao().saveOrUpdate(log);
            return new JSONObject().put("message","Bad Request");
        }
    }

    public static JSONObject merge(JSONObject... jsonObjects) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (JSONObject temp : jsonObjects) {
            Iterator<String> keys = temp.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                jsonObject.put(key, temp.get(key));
            }
        }
        return jsonObject;
    }

    public static String toRupiah(double nominal){
        final DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        final DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        dfs.setMonetaryDecimalSeparator(',');
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        return "Rp." + df.format(nominal).replace(",00","");
    }

    public static Date yyyyMMddHHmmssToDate(Date tanggal) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(tanggal);
        Date date = formatter.parse(dateString);
        return date;
    }

    public static void sendWA(String phone, String message) {
        final MLogBroadcast log = new MLogBroadcast();
        try {
            MSetting mSetting = SpringInit.getmSettingDao().getSettingByParam("wa_api_setting");
            String[] settingWA = mSetting.getValue().split(",");

            phone = phone.replace("+", "");
            if (String.valueOf(phone.charAt(0)).equals("0")) {
                phone = "62" + phone.substring(1);
            }
            final OkHttpClient client = new OkHttpClient();
            final MediaType mediaType = MediaType.parse("application/json");
            final JSONObject bodyBroadcast = new JSONObject();
            bodyBroadcast.put("device_number", settingWA[1].trim());
            bodyBroadcast.put("message", message);
            bodyBroadcast.put("to", phone);
            final RequestBody body = RequestBody.create(mediaType, bodyBroadcast.toString());
            final Request request = new Request.Builder()
//                    .url("https://wapi.srv.co.id:31012/sendMessage")
                    .url(settingWA[0].trim())
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("access-token", settingWA[2].trim())
                    .build();


            //save log
            log.setDate(getCurrentTime());
            log.setType("WA");
            log.setBroadcastType("");
            log.setMessage(message);
            log.setReceiver(phone);
            log.setSender(Constant.WA_PHONE_NUMBER);
            Response response = client.newCall(request).execute();
            final String responseString = response.body().string();
            final Log log2 = Log.getLog("Q2", "FUNCTIONS WA");
            log2.info(responseString);
            final JSONObject jsonResponse = new JSONObject(responseString);
            if (jsonResponse.has("status") && jsonResponse.getBoolean("status"))
                log.setResponse("00");
            else log.setResponse("02");
            log.setSystemResponse(jsonResponse.toString());
            SpringInit.getmLogBroadcastDao().saveOrUpdate(log);
        } catch (Exception e) {
            e.printStackTrace();
            log.setResponse("IE");
            log.setSystemResponse(e.getMessage());
            SpringInit.getmLogBroadcastDao().saveOrUpdate(log);
        }
    }

    public static JSONObject sendEmail(String email, String name, String message) throws Exception {
        final JSONObject from = new JSONObject();
        from.put("email", "diykoperasi1@gmail.com");
        from.put("name", "KOPERASI");
        JSONObject to = new JSONObject();
        to.put("email", email);
        to.put("name", name);
        JSONObject bodyEmail = new JSONObject();
        bodyEmail.put("subject", "FORGOT PASSWORD KOPERASI APP");
        bodyEmail.put("textpart", "");
        bodyEmail.put("html", message);
        bodyEmail.put("customid", "sendMail");
        bodyEmail.put("from", from);
        bodyEmail.put("to", to);
        final String bodyemailString = bodyEmail.toString();
        final OkHttpClient client = new OkHttpClient();

        final MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, bodyemailString);
        Request request = new Request.Builder()
                .url("https://yunna-service.herokuapp.com/v1/send_email")
                .post(body)
                .addHeader("Content-Type", "application/json")
                .addHeader("api-key", "YUNNA-2019-ARGAN")
                .addHeader("mailjet_user", " ee5cb04f1c66e70ad4d265f9bf2d2992")
                .addHeader("mailjet_pass", "e32af3572e6b5c0a8d24ef579a0d978f")
                .build();
        final Response response = client.newCall(request).execute();
        return new JSONObject(response.body().string());
    }

    public static String getDetailPinjaman(String idKaryawan) throws Exception{
        final String theUrl = Constant.URL_GET_DETAIL_PINJAMAN+idKaryawan;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(theUrl)
                .get()
                .build();

        final Response response = client.newCall(request).execute();
//        System.out.println("asd respo");
        return response.body().string();
    }


}
