package com.dak.fkoperasii.utility;

import org.jpos.util.Log;

/**
 *
 * @author Morten Jonathan
 */
public class Constant {

//    Database
    public static final String DATABASE_SCHEMA = "koperasidakco_dkoperasi";

//    HTTP SERVLET
    public static final String HTTP_SERVLET_ENTITY_AUTH = "x-api-key";
    public static final String HTTP_SERVLET_ENTITY_PARTNER_IDENTIFIER = "user-agent";
    public static final String HTTP_SERVLET_ENTITY_PARTNER_AUTH = "authorization";
    public static final String HTTP_SERVLET_ENTITY_INCOM = "xincomingx";
    public static final String WA_PHONE_NUMBER="6287772211666";
    public static final String EMAIL_SENDER="diykoperasi1@gmail.com";
    public static final String URL_GET_DETAIL_PINJAMAN="https://koperasiutama.co.id/api/pinjaman/tanggungan?id_anggota=";
    
    public static final String REQUEST_BODY = "REQUEST_BODY";
    public static final String REQUEST = "REQUEST";
    public static final String RESPONSE = "RESPONSE";
    public static final String PATH = "PATH";
    public static final String MLOG = "MLOG";
    public static final String ABORTED_RESPONSE = "ABORTED_RESPONSE";
    public static final String IMAGE = "IMAGE";
    public static final String PDF = "PDF";
    public static final String CTX_ID_USER ="is_user";
    
    public static final String USER_DETAIL = "USER_DETAIL";
    public static final String DATA_PENGAJUAN = "DATA_PENGAJUAN";
    public static final String LOG_SIMULASI = "LOG_SIMULASI";

    public class STATUS_ANGGOTA{
        public static final String PEGAWAI_TETAP = "1";
        public static final String PEGAWAI_TIDAK_TETAP = "2";
        public static final String UNIT_BAGIAN = "3";
    }
    
    public class WS {

        public static final String REQUEST_BODY = "REQUEST_BODY";
        public static final String PATH = "PATH";
        public static final String PARTNER_CORE = "PARTNER_CORE";
        public static final String TRXMGR = "TRXMGR";
        public static final String TIMEOUT = "TIMEOUT";
        public static final String REQUEST = "REQUEST";
        public static final String RESPONSE = "RESPONSE";
        public static final String OTP = "OTP";
        public static final String OTP_EN = "OTP_EN";
        public static final String MSISDN = "MSISDN";
        public static final String DESTINATION = "DESTINATION";
        public static final String TOKEN = "TOKEN";
        public static final String LOG_TX = "LOG_TX";

        public class MRC_EXT {

            public static final String STATUS = "STATUS";
            public static final String TRUE = "TRUE";
            public static final String FALSE = "FALSE";
        }

        public class STATUS {

            public static final String SUCCESS = "00";
            public static final String FAILED = "1";
        }

        public class RC {

            public static final String LOGIN_SUCCESS = "200";
            public static final String LOGIN_FAILED = "201";
            public static final String USER_AUTH_FAILED = "10";
            public static final String USER_NOT_FOUND = "11";
            public static final String PASSWORD_AUTH_FAILED = "12";
            public static final String PASSWORD_MISMATCH = "101";
            public static final String USERNAME_MISMATCH = "111";
            public static final String USER_NOT_ACTIVE = "15";
//            public static final String USER_MSISDN_ALREADY_EXIST = "16";
//            public static final String USER_CIF_ALREADY_EXIST = "17";
            public static final String DATA_NOT_FOUND = "20";
//            public static final String UNREGISTERED_ACCOUNT_NUMBER = "21";
            public static final String BALANCE_NOT_SUFFICIENT = "30";
            public static final String TIMEOUT = "50";
//            public static final String PARTNER_NOT_FOUND = "60";
//            public static final String FAILED_FROM_CORE = "70";
//            public static final String OTP_FAILED = "97";
            public static final String AUTH_FAILED = "98";
            public static final String SERVICE_PATH_NOT_FOUND = "99";
            public static final String INTERNAL_ERROR = "IE";
            public static final String FAILED = "FA";
            public static final String ALREADY_IN_THE_CART = "51";
                public static final String DATA_SUDAH_TERSIMPAN = "301";
        }
    }

    public class RM {}
    public static final String USER_NOT_FOUND="User not found";

    public class WA {

        public static final String REQUEST_BODY = "REQUEST_BODY";
        public static final String PATH = "PATH";
        public static final String PARTNER_CORE = "PARTNER_CORE";
        public static final String TRXMGR = "TRXMGR";
        public static final String TIMEOUT = "TIMEOUT";
        public static final String REQUEST = "REQUEST";
        public static final String RESPONSE = "RESPONSE";
        public static final String USER_DATA = "USER_DATA";
        public static final String OTP = "OTP";
        public static final String MSISDN = "MSISDN";

        public class STATUS {

            public static final String SUCCESS = "0";
            public static final String FAILED = "1";
            public static final String ACTIVE = "ACTIVE";
            public static final String NON_ACTIVE = "NON_ACTIVE";
        }

        public class RC {

            public static final String SUCCESS = "00";
            public static final String USER_AUTH_FAILED = "10";
            public static final String USER_NOT_FOUND = "11";
            public static final String PIN_AUTH_FAILED = "12";
            public static final String PIN_MISMATCH = "13";
            public static final String MSISDN_ALREADY_EXIST = "14";
            public static final String USER_NOT_ACTIVE = "15";
            public static final String BALANCE = "15";
            public static final String TIMEOUT = "50";
            public static final String PARTNER_NOT_FOUND = "60";
            public static final String FAILED_FROM_CORE = "70";
            public static final String OTP_FAILED = "97";
            public static final String AUTH_FAILED = "98";
            public static final String SERVICE_PATH_NOT_FOUND = "99";
            public static final String INTERNAL_ERROR = "IE";
            public static final String FAILED = "FA";
        }

        public class ROLE {

            public static final String SUPER_USER = "1";
            public static final String ADMIN = "2";
            public static final String USER_DEFAULT = "3";
        }

    }

    public class TRX {

        public static final String STATUS = "STATUS";
        public static final String RC = "RC";
        public static final String HARGA = "HARGA";
        public static final String USER_DETAIL = "USER_DETAIL";
        public static final String INQ_DATA = "INQ_DATA";
        public static final String DEBET_REF = "DEBET_REF";
        public static final String DEBET = "debet";
        public static final String KREDIT = "kredit";
        
    }

//    public class SETTINGS {
//
//        public static final String TIMEOUT = "TIMEOUT";
//        public static final String TRXMGR_MOB = "TRXMGR_MOB";
//
//        public static final String TRXMGR_WEB_ADMIN = "TRXMGR_WEB_ADMIN";
//
//        public static final String CORE_URL = "CORE_URL";
//
//        public static final String TRIDES_PIN_ENCRYPT = "MBDIGITALAMOREKRIYANESIA";
//        public static final String TRIDES_OTP_ENCRYPT = "MBDIGITALAMOREKRIYANESIA";
//        public static final String TRIDES_WEB_PASS_ENCRYPT = "CRYMOBWEBADMINKRIYANESIA";
//    }
    
    public class SETTING {
        
        public static final String TRIDES_MOB_ENCRYPT = "MBDIGITALAMOREKRIYANESIA";
        public static final String HTTP_SERVLET_ENTITY_AUTH = "x-api-key";
        
        public class SETTING_VALUE {
        
            public static final int TIME_OUT = 100000;
            public static final String TRXMGR  = "fkoperasii-mob-trx";
            public static final String TRXMGR_UPLOAD  = "upload-trx";
        }
        
        public class SETTING_DESC {
        
            public static final String TIME_OUT = "TIME_OUT";
            public static final String TRXMGR  = "TRXMGR_MOB";
            public static final String TRXMGR_UPLOAD  = "TRXMGR_UPLOAD";
        
        }
        
    }

    public class CONTROLLER {

        public static final String GROUP = "GROUP";
    }

    public class LOGIN {
        public static final String STATUS_LOGIN = "STATUS_LOGIN";
        
        public class STATUS_USER {

            public static final String INACTIVE = "N";
            public static final String ACTIVE = "Y";
            public static final String BLOCKED = "2";
            public static final String DELETED = "3";
        }

        public class USER_DESC_STATUS {

            public static final String INACTIVE = "INACTIVE";
            public static final String ACTIVE = "ACTIVE";
            public static final String BLOCKED = "BLOCKED";
            public static final String DELETED = "DELETED";
        }
    }

    public static class STATUS_PRODUCT {

        public final boolean ACTIVE = true;
        public final boolean INACTIVE = false;

    }

    public class ISO {

        public class PROCESSING_CODE {

            public static final String INQUIRY = "100100";
            public static final String POSTING = "100200";
        }

        public class MERCHANT_TYPE {

            public static final String TELLER = "6010";
            public static final String ATM = "6011";
            public static final String EDC_POS = "6012";
            public static final String PHONE_BANKING = "6013";
            public static final String INTERNET_BANKING = "6014";
            public static final String KIOSK = "6015";
            public static final String AUTO_DEBET = "6016";
            public static final String MOBILE_BANKING = "6017";
            public static final String ADM = "6018";

        }

        public class CURRENCY {

            public static final String RUPIAH = "360";
        }

        public class CARD_ACC {

            public static final String S_BANK = "S-BANK";
        }

        public static final String ISO_RESPONSE = "ISO_RESPONSE";
    }

    public class MERCHANT {

        public static final long XW = 1;
    }

    public class ANSI {

        public static final String ANSI_RESET = "\u001B[0m";
        public static final String ANSI_RED = "\u001B[32m";
    }
    
    public class TELCO {
        public static final String PROVIDER = "PROVIDER";
        public static final String TOKEN_TELCO_SERVICE = "TOKEN_TELCO_SERVICE";
        public static final String TOKEN_TELCO_BILLER = "TOKEN_TELCO_BILLER";
        public static final String INQ_RES = "INQ_RES";
        public static final String RC = "RC";
        
        public class INQ_JSON_INDEX {
            public static final int MSISDN = 0;
            public static final int DESTINATION = 1;
            public static final int DENOM = 2;
            public static final int AMOUNT_SETTLEMENT = 3;
            public static final int PROVIDER = 4;
            public static final int AMOUNT_TRANSACTION = 5;
        }
    }
    
    public class OBJ{
        public static final String RESP = "resp";
    }

    public static void check(String mess) {
        System.out.println(Constant.ANSI.ANSI_RED + mess + Constant.ANSI.ANSI_RESET);
    }
    
    public static String togrn(String mes) {
        mes = Constant.ANSI.ANSI_RED + mes + Constant.ANSI.ANSI_RESET ;
        return mes;
    }

}
