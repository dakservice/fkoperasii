/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.utility;

import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class ResponseWebServiceContainer {
     public final String RC = "rc";
    public final String RM = "rm";
    private JSONObject json;

    public ResponseWebServiceContainer() {

    }

    public ResponseWebServiceContainer(JSONObject jObjectResponse) {
        json = jObjectResponse;
    }

    public ResponseWebServiceContainer(String rc, String desc) {
        json = new JSONObject().put(RC, rc).put(RM, desc);
    }

    public ResponseWebServiceContainer(String rc, String desc, JSONObject another) {
        json = Utility.merge(new JSONObject().put(RC, rc).put(RM, desc), another);
    }

    public JSONObject toJSONObject() {
        return json;
    }

    public String jsonToString() {
        return json.toString();
    }
    
}
