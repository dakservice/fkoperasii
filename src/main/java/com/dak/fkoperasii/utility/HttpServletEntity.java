/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.utility;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Morten Jonathan
 */
public class HttpServletEntity {
    
    public HttpServletEntity(HttpServletRequest httpReq) {
        this.requestUrl = httpReq.getRequestURL().toString();
        this.remoteAddr = httpReq.getRemoteAddr();
        this.remoteHost = httpReq.getRemoteHost();
        this.headers = getHeadersInfo(httpReq).toString();
        this.authorize = httpReq.getHeader(Constant.HTTP_SERVLET_ENTITY_AUTH);
        this.incom = httpReq.getHeader(Constant.HTTP_SERVLET_ENTITY_INCOM);
        this.uri = httpReq.getRequestURI();
    }

    private final String requestUrl;
    private final String headers;
    private final String remoteAddr;
    private final String remoteHost;
    private final String authorize;
    private final String incom;
    private final String uri;

    public String getRequestUrl() {
        return requestUrl;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public String getHeaders() {
        return headers;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public String getAuthorize() {
        return authorize;
    }

    public String getUri() {
        return uri;
    }

    public String getIncom() {
        return incom;
    }
    
    private Map<String, String> getHeadersInfo(HttpServletRequest request) {
        Map<String, String> map = new HashMap<>();

        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }
        return map;
    }
    
}
