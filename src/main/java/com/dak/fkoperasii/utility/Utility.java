/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.utility;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Iterator;
import java.util.Random;
import org.jpos.iso.ISOUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class Utility {
    
    public static JSONObject merge(JSONObject... jsonObjects) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (JSONObject temp : jsonObjects) {
            Iterator<String> keys = temp.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                jsonObject.put(key, temp.get(key));
            }
        }
        return jsonObject;
    }

    public static String getStan() {
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return s.substring(s.length() - 6);
    }

    public static String getTrxId() {
        String s = ISOUtil.getRandomDigits(new Random(), 30, 9);
        return s;
    }

    public static String getReffNumber() {
        String s = ISOUtil.getRandomDigits(new Random(), 30, 9);
        return s;
    }

    public static String getRrn() {
        String s = ISOUtil.getRandomDigits(new Random(), 12, 9);
        return s;
    }

    public static String getToken() {
        String s = String.valueOf(Math.abs(new Random().nextLong()));
        return s.substring(s.length() - 6);
    }

    public static String replaceMsisdn(String msisdn) {
        StringBuilder sb = new StringBuilder();
        if (msisdn.startsWith("0")) {
            sb.append("62");
            sb.append(msisdn.substring(1));
        } else if (msisdn.startsWith("620")) {
            sb.append("62");
            sb.append(msisdn.substring(3));
        } else if (msisdn.startsWith("+62")) {
            sb.append(msisdn.substring(1));
        } else {
            sb.append(msisdn);
        }
        return sb.toString();
    }

    public static String replaceMsisdnToCore(String msisdn) {
        StringBuilder sb = new StringBuilder();

        sb.append("0");
        sb.append(msisdn.substring(2));

        return sb.toString();
    }

    public static String formatAmount(double val) {
        String ret = "";
        DecimalFormat decFrm = new DecimalFormat();
        decFrm.applyPattern("###,###,###,###,###,###");
        ret = decFrm.format(val).replace(',', '.');
        return ret;
    }

//    public static String generateDigitPin() {
//        int digit1 = getRandomIntegerBetweenRange(1, 6);
//        int digit2 = getRandomIntegerBetweenRange(digit1, 6);
//        for (int i = 0; i < 20; i++) {
//            if (digit1 == digit2) {
//                digit2 = getRandomIntegerBetweenRange(digit1, 6);
//            } else {
//                break;
//            }
//        }
//        String random = "" + digit1 + "" + digit2;
//
//        return random;
//    }
    
    public static String generateDigitPin() {
        int digit1 = getRandomIntegerBetweenRange(1, 9);
        int digit2 = getRandomIntegerBetweenRange(digit1, 9);
        int digit3 = getRandomIntegerBetweenRange(digit2, 9);
        int digit4 = getRandomIntegerBetweenRange(1, 9);
        int digit5 = getRandomIntegerBetweenRange(1, 9);
        int digit6 = getRandomIntegerBetweenRange(1, 9);
        for (int i = 0; i < 6; i++) {
            if (digit1 == digit2) {
                digit2 = getRandomIntegerBetweenRange(digit1, 9);
            } else if (digit3 == digit4) {
                digit3 = getRandomIntegerBetweenRange(1, 6);
            } else if (digit5 == digit6) {
                digit4 = getRandomIntegerBetweenRange(digit3, 9);
            } else if (digit4 == digit5) {
                digit6 = getRandomIntegerBetweenRange(1, 8);
            }else {
                break;
            }
        }
        String random = "" + digit1 + "" + digit2+ "" + digit3+ "" + digit4+ "" + digit5+ "" + digit6;

        return random;
    }

    public static int getRandomIntegerBetweenRange(int min, int max) {
        int x = (int) ((int) (Math.random() * ((max - min) + 1)) + min);
        return x;
    }
    
    public static void setJsonToArray(JSONArray jArr, String header, String value) {
        JSONObject jObj = new JSONObject();
        jObj.put("header", header);
        jObj.put("value", value);
        jArr.put(jObj);
    }
    
    

    public static String generateToken(int length) {
        Random random = new Random();
        String code = new String("");
        for (int i = 0; i < length; i++) {
            code += (char) (random.nextInt(10) + '0');
        }
        return code;
    }
    
    public static void getFormatCurrency(int currency) {


        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);
    }
}
