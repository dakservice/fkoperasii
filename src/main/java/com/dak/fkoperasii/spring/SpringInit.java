/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.spring;

import com.dak.fkoperasii.dao.*;

import javax.naming.ConfigurationException;
import org.jpos.util.Log;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Frika Da Cintia
 */
public class SpringInit {
    Log log = Log.getLog("Q2", getClass().getName());

    private static MNasabahDao mNasabahDao;
    private static MRcDao mRcDao;
    private static MSettingDao mSettingDao;
    private static MLogMobileDao mLogMobileDao;
    private static MPengajuanDao MPengajuanDao;
    private static MProductDao MProductDao;
    private static MLogTrxDao MLogTrxDao;
    private static MRoutingDao MRoutingDao;
    private static TmpDetailChartDao TmpDetailChartDao;
    private static MTransSPDao MTransSPDao;
    private static MJurnalDao MJurnalDao;
    private static MJenisSimpanDao MJenisSimpanDao;
    private static MJenisAngsuranDao MJenisAngsuranDao;
    private static MJenisPinjamanDao MJenisPinjamanDao;
    private static MPresentasePlafonDao MPresentasePlafonDao;
    private static MSukuBungaDao MSukuBungaDao;
    private static MStatusPengajuanDao MStatusPengajuanDao;
    private static MNilaiJaminanDao MNilaiJaminanDao;
    private static MHeadPinjamanDao MHeadPinjamanDao;
    private static MDetailPinjamanDao MDetailPinjamanDao;
    private static MBuktiJaminanDao MBuktiJaminanDao;
    private static TmpDetailBrgDao TmpDetailBrgDao;
    private static MAddendumDao MAddendumDao;
    private static MDPinjamanDao MDPinjamanDao;
    private static MSetorTarikDao MSetorTarikDao;
    private static MProfileDao MProfileDao;

    private static MPinjamanRutinDao MPinjamanRutinDao;
    private static MPinjamanNonRutinDao MPinjamanNonRutinDao;
    private static MPinjamanBarangDao MPinjamanBarangDao;
    private static MPinjamanMotorDao MPinjamanMotorDao;
    private static MPinjamanRutinPromoDao MPinjamanRutinPromoDao;
    private static MPinjamanBarangPromoDao MPinjamanBarangPromoDao;
    private static MOtpDao mOtpDao;
    private static MDeviceAnggotaDao mDeviceAnggotaDao;
    private static MLogBroadcastDao mLogBroadcastDao;

    public static MLogBroadcastDao getmLogBroadcastDao() {
        return mLogBroadcastDao;
    }

    public static void setmLogBroadcastDao(MLogBroadcastDao mLogBroadcastDao) {
        SpringInit.mLogBroadcastDao = mLogBroadcastDao;
    }

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public static MDeviceAnggotaDao getMDeviceAnggotaDao() {
        return mDeviceAnggotaDao;
    }

    public static void setMDeviceAnggotaDao(MDeviceAnggotaDao mDeviceAnggotaDao) {
        SpringInit.mDeviceAnggotaDao = mDeviceAnggotaDao;
    }
    public static MOtpDao getmOtpDao() {
        return mOtpDao;
    }

    public static void setMotpDao(MOtpDao mOtpDao) {
        SpringInit.mOtpDao = mOtpDao;
    }
    public static MNasabahDao getmNasabahDao() {
        return mNasabahDao;
    }

    public static void setmNasabahDao(MNasabahDao mNasabahDao) {
        SpringInit.mNasabahDao = mNasabahDao;
    }

    public static MRcDao getmRcDao() {
        return mRcDao;
    }

    public static void setmRcDao(MRcDao mRcDao) {
        SpringInit.mRcDao = mRcDao;
    }

    public static MSettingDao getmSettingDao() {
        return mSettingDao;
    }

    public static void setmSettingDao(MSettingDao mSettingDao) {
        SpringInit.mSettingDao = mSettingDao;
    }

    public static MLogMobileDao getmLogMobileDao() {
        return mLogMobileDao;
    }

    public static void setmLogMobileDao(MLogMobileDao mLogMobileDao) {
        SpringInit.mLogMobileDao = mLogMobileDao;
    }

    public static MPengajuanDao getMPengajuanDao() {
        return MPengajuanDao;
    }

    public static void setMPengajuanDao(MPengajuanDao MPengajuanDao) {
        SpringInit.MPengajuanDao = MPengajuanDao;
    }

    public static MProductDao getMProductDao() {
        return MProductDao;
    }

    public static void setMProductDao(MProductDao MProductDao) {
        SpringInit.MProductDao = MProductDao;
    }

    public static MLogTrxDao getMLogTrxDao() {
        return MLogTrxDao;
    }

    public static void setMLogTrxDao(MLogTrxDao MLogTrxDao) {
        SpringInit.MLogTrxDao = MLogTrxDao;
    }

    public static MRoutingDao getMRoutingDao() {
        return MRoutingDao;
    }

    public static void setMRoutingDao(MRoutingDao MRoutingDao) {
        SpringInit.MRoutingDao = MRoutingDao;
    }

    public static TmpDetailChartDao getTmpDetailChartDao() {
        return TmpDetailChartDao;
    }

    public static void setTmpDetailChartDao(TmpDetailChartDao TmpDetailChartDao) {
        SpringInit.TmpDetailChartDao = TmpDetailChartDao;
    }

    public static MTransSPDao getMTransSPDao() {
        return MTransSPDao;
    }

    public static void setMTransSPDao(MTransSPDao MTransSPDao) {
        SpringInit.MTransSPDao = MTransSPDao;
    }

    public static MJurnalDao getMJurnalDao() {
        return MJurnalDao;
    }

    public static void setMJurnalDao(MJurnalDao MJurnalDao) {
        SpringInit.MJurnalDao = MJurnalDao;
    }

    public static MJenisSimpanDao getMJenisSimpanDao() {
        return MJenisSimpanDao;
    }

    public static void setMJenisSimpanDao(MJenisSimpanDao MJenisSimpanDao) {
        SpringInit.MJenisSimpanDao = MJenisSimpanDao;
    }

    public static MJenisAngsuranDao getMJenisAngsuranDao() {
        return MJenisAngsuranDao;
    }

    public static void setMJenisAngsuranDao(MJenisAngsuranDao MJenisAngsuranDao) {
        SpringInit.MJenisAngsuranDao = MJenisAngsuranDao;
    }

    public static MJenisPinjamanDao getMJenisPinjamanDao() {
        return MJenisPinjamanDao;
    }

    public static void setMJenisPinjamanDao(MJenisPinjamanDao MJenisPinjamanDao) {
        SpringInit.MJenisPinjamanDao = MJenisPinjamanDao;
    }

    public static MPresentasePlafonDao getMPresentasePlafonDao() {
        return MPresentasePlafonDao;
    }

    public static void setMPresentasePlafonDao(MPresentasePlafonDao MPresentasePlafonDao) {
        SpringInit.MPresentasePlafonDao = MPresentasePlafonDao;
    }

    public static MSukuBungaDao getMSukuBungaDao() {
        return MSukuBungaDao;
    }

    public static void setMSukuBungaDao(MSukuBungaDao MSukuBungaDao) {
        SpringInit.MSukuBungaDao = MSukuBungaDao;
    }

    public static MStatusPengajuanDao getMStatusPengajuanDao() {
        return MStatusPengajuanDao;
    }

    public static void setMStatusPengajuanDao(MStatusPengajuanDao MStatusPengajuanDao) {
        SpringInit.MStatusPengajuanDao = MStatusPengajuanDao;
    }

    public static TmpDetailBrgDao getTmpDetailBrgDao() {
        return TmpDetailBrgDao;
    }

    public static void setTmpDetailBrgDao(TmpDetailBrgDao TmpDetailBrgDao) {
        SpringInit.TmpDetailBrgDao = TmpDetailBrgDao;
    }

    public static MNilaiJaminanDao getMNilaiJaminanDao() {
        return MNilaiJaminanDao;
    }

    public static void setMNilaiJaminanDao(MNilaiJaminanDao MNilaiJaminanDao) {
        SpringInit.MNilaiJaminanDao = MNilaiJaminanDao;
    }

    public static MHeadPinjamanDao getMHeadPinjamanDao() {
        return MHeadPinjamanDao;
    }

    public static void setMHeadPinjamanDao(MHeadPinjamanDao MHeadPinjamanDao) {
        SpringInit.MHeadPinjamanDao = MHeadPinjamanDao;
    }

    public static MBuktiJaminanDao getMBuktiJaminanDao() {
        return MBuktiJaminanDao;
    }

    public static void setMBuktiJaminanDao(MBuktiJaminanDao MBuktiJaminanDao) {
        SpringInit.MBuktiJaminanDao = MBuktiJaminanDao;
    }

    public static MDetailPinjamanDao getMDetailPinjamanDao() {
        return MDetailPinjamanDao;
    }

    public static void setMDetailPinjamanDao(MDetailPinjamanDao MDetailPinjamanDao) {
        SpringInit.MDetailPinjamanDao = MDetailPinjamanDao;
    }

    public static MAddendumDao getMAddendumDao() {
        return MAddendumDao;
    }

    public static void setMAddendumDao(MAddendumDao MAddendumDao) {
        SpringInit.MAddendumDao = MAddendumDao;
    }

    public static MDPinjamanDao getMDPinjamanDao() {
        return MDPinjamanDao;
    }

    public static void setMDPinjamanDao(MDPinjamanDao MDPinjamanDao) {
        SpringInit.MDPinjamanDao = MDPinjamanDao;
    }

    public static MSetorTarikDao getMSetorTarikDao() {
        return MSetorTarikDao;
    }

    public static void setMSetorTarikDao(MSetorTarikDao MSetorTarikDao) {
        SpringInit.MSetorTarikDao = MSetorTarikDao;
    }

    public static MProfileDao getMProfileDao() {
        return MProfileDao;
    }

    public static void setMProfileDao(MProfileDao MProfileDao) {
        SpringInit.MProfileDao = MProfileDao;
    }

    public static MPinjamanRutinDao getMPinjamanRutinDao() {
        return MPinjamanRutinDao;
    }

    public static void setMPinjamanRutinDao(MPinjamanRutinDao MPinjamanRutinDao) {
        SpringInit.MPinjamanRutinDao = MPinjamanRutinDao;
    }

    public static MPinjamanNonRutinDao getMPinjamanNonRutinDao() {
        return MPinjamanNonRutinDao;
    }

    public static void setMPinjamanNonRutinDao(MPinjamanNonRutinDao MPinjamanNonRutinDao) {
        SpringInit.MPinjamanNonRutinDao = MPinjamanNonRutinDao;
    }

    public static MPinjamanBarangDao getMPinjamanBarangDao() {
        return MPinjamanBarangDao;
    }

    public static void setMPinjamanBarangDao(MPinjamanBarangDao MPinjamanBarangDao) {
        SpringInit.MPinjamanBarangDao = MPinjamanBarangDao;
    }

    public static MPinjamanMotorDao getMPinjamanMotorDao() {
        return MPinjamanMotorDao;
    }

    public static void setMPinjamanMotorDao(MPinjamanMotorDao MPinjamanMotorDao) {
        SpringInit.MPinjamanMotorDao = MPinjamanMotorDao;
    }

    public static MPinjamanRutinPromoDao getMPinjamanRutinPromoDao() {
        return MPinjamanRutinPromoDao;
    }

    public static void setMPinjamanRutinPromoDao(MPinjamanRutinPromoDao MPinjamanRutinPromoDao) {
        SpringInit.MPinjamanRutinPromoDao = MPinjamanRutinPromoDao;
    }

    public static MPinjamanBarangPromoDao getMPinjamanBarangPromoDao() {
        return MPinjamanBarangPromoDao;
    }

    public static void setMPinjamanBarangPromoDao(MPinjamanBarangPromoDao MPinjamanBarangPromoDao) {
        SpringInit.MPinjamanBarangPromoDao = MPinjamanBarangPromoDao;
    }





    public void initService() throws ConfigurationException {
        ApplicationContext context = new FileSystemXmlApplicationContext("/src/main/resources/ApplicationContext.xml");
//        ApplicationContext context = new FileSystemXmlApplicationContext("ApplicationContext.xml");
        setmNasabahDao(context.getBean("mNasabahDao", MNasabahDao.class));
        setmRcDao(context.getBean("mRcDao", MRcDao.class));
        setmSettingDao(context.getBean("mSettingDao", MSettingDao.class));
        setmLogMobileDao(context.getBean("MLogMobileDao", MLogMobileDao.class));
        setMPengajuanDao(context.getBean("MPengajuanDao", MPengajuanDao.class));
        setMProductDao(context.getBean("MProductDao", MProductDao.class));
        setMLogTrxDao(context.getBean("MLogTrxDao", MLogTrxDao.class));
        setMRoutingDao(context.getBean("MRoutingDao", MRoutingDao.class));
        setTmpDetailChartDao(context.getBean("TmpDetailChartDao", TmpDetailChartDao.class));
        setMTransSPDao(context.getBean("MTransSPDao", MTransSPDao.class));
        setMJurnalDao(context.getBean("MJurnalDao", MJurnalDao.class));
        setMJenisSimpanDao(context.getBean("MJenisSimpanDao", MJenisSimpanDao.class));
        setMJenisAngsuranDao(context.getBean("MJenisAngsuranDao", MJenisAngsuranDao.class));
        setMJenisPinjamanDao(context.getBean("MJenisPinjamanDao", MJenisPinjamanDao.class));
        setMPresentasePlafonDao(context.getBean("MPresentasePlafonDao", MPresentasePlafonDao.class));
        setMSukuBungaDao(context.getBean("MSukuBungaDao", MSukuBungaDao.class));
        setMStatusPengajuanDao(context.getBean("MStatusPengajuanDao", MStatusPengajuanDao.class));
        setTmpDetailBrgDao(context.getBean("TmpDetailBrgDao", TmpDetailBrgDao.class));
        setMNilaiJaminanDao(context.getBean("MNilaiJaminanDao", MNilaiJaminanDao.class));
        setMHeadPinjamanDao(context.getBean("MHeadPinjamanDao", MHeadPinjamanDao.class));
        setMBuktiJaminanDao(context.getBean("MBuktiJaminanDao", MBuktiJaminanDao.class));
        setMDetailPinjamanDao(context.getBean("MDetailPinjamanDao", MDetailPinjamanDao.class));
        setMAddendumDao(context.getBean("MAddendumDao", MAddendumDao.class));
        setMDPinjamanDao(context.getBean("MDPinjamanDao", MDPinjamanDao.class));
        setMSetorTarikDao(context.getBean("MSetorTarikDao", MSetorTarikDao.class));
        setMProfileDao(context.getBean("MProfileDao", MProfileDao.class));
        setMPinjamanRutinDao(context.getBean("MPinjamanRutinDao", MPinjamanRutinDao.class));
        setMPinjamanNonRutinDao(context.getBean("MPinjamanNonRutinDao", MPinjamanNonRutinDao.class));
        setMPinjamanBarangDao(context.getBean("MPinjamanBarangDao", MPinjamanBarangDao.class));
        setMPinjamanMotorDao(context.getBean("MPinjamanMotorDao", MPinjamanMotorDao.class));
        setMPinjamanRutinPromoDao(context.getBean("MPinjamanRutinPromoDao", MPinjamanRutinPromoDao.class));
        setMPinjamanBarangPromoDao(context.getBean("MPinjamanBarangPromoDao", MPinjamanBarangPromoDao.class));
        setMotpDao(context.getBean("MOtpDao", MOtpDao.class));
        setMDeviceAnggotaDao(context.getBean("MDeviceAnggotaDao", MDeviceAnggotaDao.class));
        setmLogBroadcastDao(context.getBean("MlogBroadcastDao",MLogBroadcastDao.class));
        log.info("Init DB has successfull");
        log.info("Service Started");

    }
}
