/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.laporan;

import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MNasabah;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.entity.MTransSP;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Morten Jonathan
 */
public class LapPembayaran implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer dr = new ResponseWebServiceContainer();
    MRc rc;

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        JSONObject jsonReq = (JSONObject) ctx.get(Constant.WS.REQUEST_BODY);
        DecimalFormat df = new DecimalFormat("#");
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        MLogMobile mACtivity = (MLogMobile) ctx.get(Constant.WS.REQUEST);

        try {

            String notelp = jsonReq.getString("notelp");
            MNasabah nsbh = SpringInit.getmNasabahDao().userByNotelp(notelp);

            if (nsbh != null) {
                if (nsbh.getAktif().equals("Y")) {
                    List<Object[]> lpembayaran = SpringInit.getMHeadPinjamanDao().getPembayaranByAnggota(Integer.parseInt(nsbh.getId().toString()));
                    JSONArray data = new JSONArray();
                    if (lpembayaran.size() > 0 && lpembayaran.get(0)[3] != null) {
                        for (int i = 0; i < lpembayaran.size(); i++) {
                            JSONObject logData = new JSONObject();
                            logData.put("pinjamid", lpembayaran.get(i)[0].toString());

                            //String tanggel = lpembayaran.get(i)[3].toString();
                            if (lpembayaran.get(i)[3] == null) {
                                logData.put("tanggal", "null");
                            } else {
                                logData.put("tanggal", datetime.format(lpembayaran.get(i)[3]));
                                log.info("======= TANGGAL " + datetime.format(lpembayaran.get(i)[3]));
                            }

                            if (lpembayaran.get(i)[9] == null) {
                                logData.put("jenis", "null");
                            } else {
                                logData.put("jenis", lpembayaran.get(i)[9]);
                            }

                            if (lpembayaran.get(i)[5] == null) {
                                logData.put("angsuranke", "null");
                            } else {
                                logData.put("angsuranke", lpembayaran.get(i)[5].toString());
                            }

                            if (lpembayaran.get(i)[7] == null) {
                                logData.put("denda", "null");
                            } else {
                                logData.put("denda", lpembayaran.get(i)[7].toString());
                            }

                            if (lpembayaran.get(i)[6] == null) {
                                logData.put("jlhbayar", "null");
                            } else {
                                logData.put("jlhbayar", lpembayaran.get(i)[6].toString());
                            }

                            if (lpembayaran.get(i)[15] == null) {
                                logData.put("keterangan", "null");
                            } else {
                                logData.put("keterangan", lpembayaran.get(i)[15]);
                            }

                            data.put(logData);
                            JSONObject json = new JSONObject();
                            json.put("data", data);
                            rc = SpringInit.getmRcDao().getRc(Constant.WS.STATUS.SUCCESS);
                            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm(), json);
//                            mlogmonile id user
                            mACtivity.setIdUser(nsbh.getId());
                            ctx.put(Constant.WS.REQUEST, mACtivity);

                        }
                    } else {
                        //                            mlogmonile id user
                        mACtivity.setIdUser(nsbh.getId());
                        ctx.put(Constant.WS.REQUEST, mACtivity);

                        rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.DATA_NOT_FOUND);
                        dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                    }
                } else {

                    //                            mlogmonile id user
                    mACtivity.setIdUser(nsbh.getId());
                    ctx.put(Constant.WS.REQUEST, mACtivity);

                    rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_ACTIVE);
                    dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
                }
            } else {
                //                            mlogmonile id user
                mACtivity.setIdUser(nsbh.getId());
                ctx.put(Constant.WS.REQUEST, mACtivity);

                rc = SpringInit.getmRcDao().getRc(Constant.WA.RC.USER_NOT_FOUND);
                dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
            }

        } catch (Exception e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            log.error("+++++++++++++++++++++++" + stringWriter.toString());

            rc = SpringInit.getmRcDao().getRc(Constant.WS.RC.INTERNAL_ERROR);
            dr = new ResponseWebServiceContainer(rc.getRc(), rc.getRm());
        }
        
        ctx.put(Constant.WS.RESPONSE, dr);
        ctx.put(Constant.TRX.RC, rc);
        ctx.put(Constant.TRX.STATUS, Constant.WS.STATUS.SUCCESS);
    }

    @Override
    public void abort(long id, Serializable srlzbls) {
    }

}
