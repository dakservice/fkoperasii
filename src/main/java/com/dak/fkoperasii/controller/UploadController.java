/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.fkoperasii.controller;


import com.dak.fkoperasii.entity.MLogMobile;
import com.dak.fkoperasii.entity.MRc;
import com.dak.fkoperasii.spring.SpringInit;
import com.dak.fkoperasii.utility.Constant;
import com.dak.fkoperasii.utility.HttpServletEntity;
import com.dak.fkoperasii.utility.ResponseWebServiceContainer;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Morten Jonathan
 */
@CrossOrigin(origins = "*")
@RestController
public class UploadController {
    
    Log log = Log.getLog("Q2", getClass().getName());

    private ResponseWebServiceContainer wsResponse = new ResponseWebServiceContainer();
    private MRc mapRc = new MRc();
    Context ctx = new Context();
    
    @RequestMapping(value = "/fkoperasi/upload/{service_type}", method = RequestMethod.POST)
    public String getRequestMultipartAds(
            @RequestPart(name = "file_images", required = true) MultipartFile images[],
            @RequestPart(name = "body_request") String body,
            @RequestPart(name = "file_pdf", required = true) MultipartFile files[],
            @PathVariable("service_type") String path,
            HttpServletRequest request) {
    
        HttpServletEntity httpServletEntity = new HttpServletEntity(request);

            MLogMobile mActivity= new MLogMobile();
            mActivity.setRemoteAddr(httpServletEntity.getRemoteAddr());
            mActivity.setRequestUrl(httpServletEntity.getRequestUrl());
            mActivity.setRequestBody(body);
            mActivity.setRouterPath(path);
            mActivity.setRequestTime(new Date());

            mActivity = SpringInit.getmLogMobileDao().saveOrUpdate(mActivity);
            log.info("Incoming from : " + mActivity.getRemoteAddr());
            log.info("Incoming Request Url : " + mActivity.getRequestUrl());
            log.info("Incoming Body: " + mActivity.getRequestBody());


            JSONObject reqBody = new JSONObject(mActivity.getRequestBody());
            log.info("================ MASUK TRY CONTROLLER");
            try {
                Space sp = SpaceFactory.getSpace();
                ctx.put(Constant.WA.PATH, mActivity.getRouterPath());
                ctx.put(Constant.WA.REQUEST_BODY, reqBody);
                ctx.put(Constant.WA.REQUEST, mActivity);
                ctx.put(Constant.IMAGE, images);
                log.info("================ IMAGES "+images);
                ctx.put(Constant.PDF, files);
                log.info("================ FILES "+files);
                ctx.put(Constant.SETTING.SETTING_DESC.TRXMGR_UPLOAD, Constant.SETTING.SETTING_VALUE.TRXMGR_UPLOAD);
                log.info("================ MASUK SETTING TRXMGR UPLOAD");
                ctx.put(Constant.SETTING.SETTING_DESC.TIME_OUT, Constant.SETTING.SETTING_VALUE.TIME_OUT);
                log.info("================ MASUK RESPON TIMEOUT");
                sp.out(Constant.SETTING.SETTING_VALUE.TRXMGR_UPLOAD, ctx, Constant.SETTING.SETTING_VALUE.TIME_OUT);
                log.info("================ MASUK RESPON TRXMGR UPLOAD");

    //                Send request and get response from space
                Context response = (Context) sp.in(mActivity.getId(), Constant.SETTING.SETTING_VALUE.TIME_OUT);
                log.info("Insert request " + mActivity.getId() + " to space[" + Constant.SETTING.SETTING_VALUE.TRXMGR + "], timeout trx : " + Constant.SETTING.SETTING_VALUE.TIME_OUT);
                log.info("Waiting " + mActivity.getId() + " from space");
                log.info("Get " + mActivity.getId() + " from space");
    //                Response action handle
                if (response != null) {
                    log.info("================ MASUK RESPON TIDAK NULL");
                    mActivity.setStatus(response.getString(Constant.TRX.STATUS));
                    mapRc = (MRc) response.get(Constant.TRX.RC);
                    wsResponse = (ResponseWebServiceContainer) response.get(Constant.WA.RESPONSE);
                } else {
                    log.info("================ MASUK RESPON NULL");
                    mActivity.setStatus(Constant.WA.STATUS.FAILED);
                    mapRc = SpringInit.getmRcDao().getRc(Constant.WA.RC.TIMEOUT);
                    wsResponse = new ResponseWebServiceContainer(mapRc.getRc(), mapRc.getRm(), reqBody);

                }

            } catch (Exception ex) {
                log.error(ExceptionUtils.getStackTrace(ex));
                mapRc = SpringInit.getmRcDao().getRc(Constant.WA.RC.SERVICE_PATH_NOT_FOUND);
                wsResponse = new ResponseWebServiceContainer(mapRc.getRc(), mapRc.getRm(), reqBody);
            }

            mActivity.setResponseTime(new Date());
            mActivity.setResponse(wsResponse.jsonToString());
            mActivity.setResponseCode(mapRc.getRc());
            mActivity.setResponseMessage(mapRc.getRm());

            log.info("Outgoing to : " + request.getRemoteAddr());
            log.info("Outgoing Body: " + wsResponse.jsonToString());

            SpringInit.getmLogMobileDao().saveOrUpdate(mActivity);
            return mActivity.getResponse();

        }
    
}
